//
//  NewcustomerViewController.h
//  Lead Now
//
//  Created by Rakesh Jain on 03/10/13.
//  Copyright (c) 2013 Infocrats. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomerTable.h"
#import "CacheManager.h"
@interface NewcustomerViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate>
{

    
    IBOutlet UISegmentedControl *segment;
     
    
    IBOutlet UITextField *txtFname,*txtLname,*txtPhone,*txtEmail;
    CustomerTable *cTable;
    CacheManager *cMgr;
    
    CGFloat   animatedDistance;
    IBOutlet UILabel *lblComment;
    IBOutlet UITextView *txtComment;
    IBOutlet UIImageView *imgBackGroundComment;
    IBOutlet UITextField *txtZipcode;
    IBOutlet UILabel *lblZipCode;
    IBOutlet UIImageView *imgCommentbackground;
    __weak IBOutlet UILabel *lblCompanyName;
}
@property (strong, nonatomic) IBOutlet UIButton *helpBtnnNewcustomer;
- (IBAction)goToiPadHelp:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblNewCustomer;
@property (strong, nonatomic) IBOutlet UIButton *btnnnBackkk;
@property (strong, nonatomic) IBOutlet UIButton *addleadBtnnn;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollviews;
@property (strong, nonatomic) IBOutlet UIImageView *imgEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblEmail;
@property (strong, nonatomic) IBOutlet UIImageView *imgState;
@property (strong, nonatomic) IBOutlet UIImageView *imgCity;
@property (strong, nonatomic) IBOutlet UIImageView *imgAddress2;
@property (strong, nonatomic) IBOutlet UIImageView *imgAddress1;
@property (strong, nonatomic) IBOutlet UITextField *Address1Txt;
@property (strong, nonatomic) IBOutlet UITextField *Address2Txt;
@property (strong, nonatomic) IBOutlet UITextField *cityTxt;
@property (strong, nonatomic) IBOutlet UITextField *stateTxt;
@property (strong, nonatomic) IBOutlet UILabel *lblState;
@property (strong, nonatomic) IBOutlet UILabel *lblCity;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress2;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress1;

@property (nonatomic, retain) NSManagedObjectContext        *managedObjectContext;
@property (nonatomic, retain) NSFetchedResultsController    *fetchedResultsController;
-(IBAction)saveServiceId;

-(IBAction)back;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height;


@end

