//
//  AppDelegate.h
//  Green Lead
//
//  Created by Rakesh Jain on 17/09/13.
//  Copyright (c) 2013 Infocrats. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CacheManager.h"
#import "GetService.h"
#import "Checkversion.h"
#import "MyObjects.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <UserNotifications/UserNotifications.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,NSURLConnectionDelegate,NSURLConnectionDataDelegate, UNUserNotificationCenterDelegate, NSFetchedResultsControllerDelegate>
{
    CacheManager *cMgr;
    Checkversion *chk;
    NSUserDefaults *standardUserDefaults;
    NSString *uuidCustom;
    NSString *strU;
    NSString *activeCompanyKey;
    MyObjects *objClass;
    NSManagedObjectContext *context;
    NSString *strBadgeNo;
    int valueBadge;
}
@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property(nonatomic,retain)NSString *activeCompanyKey;
@property(nonatomic,retain) NSString *uuidCustom;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
@end
