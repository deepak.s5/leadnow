//
//  Checkversion.h
//  Lead Now
//
//  Created by Rakesh Jain on 15/02/14.
//  Copyright (c) 2014 Infocrats. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Checkversion : NSManagedObject

@property (nonatomic, retain) NSString * sversion;

@end
