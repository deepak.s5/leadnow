//
//  HelpViewController.h
//  Lead Now
//
//  Created by Rakesh Jain on 25/09/13.
//  Copyright (c) 2013 Infocrats. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController
{

    IBOutlet UITextView *txtView;
}

- (IBAction)backiPadNewCustomer:(id)sender;
- (IBAction)backiPadExistingCustomer:(id)sender;
- (IBAction)backAddLeadView:(id)sender;
- (IBAction)backSelectedView:(id)sender;
-(void)loadHelp:(NSString *)loadAll;
-(IBAction)back;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height;
@property (weak, nonatomic) IBOutlet UILabel *lblHelp;


@end
