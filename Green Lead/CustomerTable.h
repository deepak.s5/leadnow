//
//  CustomerTable.h
//  Lead Now
//
//  Created by Rakesh Jain on 18/02/16.
//  Copyright © 2016 Infocrats. All rights reserved.
////

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomerTable : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "CustomerTable+CoreDataProperties.h"
