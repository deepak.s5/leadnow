//
//  MyObjects.m
//  Lead Now
//
//  Created by Rakesh jain on 01/05/15.
//  Copyright (c) 2015 Infocrats. All rights reserved.
//

#import "MyObjects.h"

@implementation MyObjects
@synthesize isonSelectedServices,LeadCountOutBox,ArrOutBox;

+(MyObjects *)shareManager{
    static MyObjects *sharedInstance=nil;
    static dispatch_once_t  oncePredecate;
    dispatch_once(&oncePredecate,^{
        sharedInstance=[[MyObjects alloc] init];
    });
    return sharedInstance;
}
@end
