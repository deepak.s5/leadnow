
//
//  SelectedServiceViewController.m
//  Green Lead
//
//  Created by Rakesh Jain on 24/09/13.
//  Copyright (c) 2013 Infocrats. All rights reserved.
//
#import "SelectedServiceViewController.h"
#import "AddleadViewController.h"
#import "AppDelegate.h"
#import "MainViewController.h"
#import "OutboxViewController.h"
#import "MyObjects.h"
#import "DeveloperViewController.h"
#import "IncidenceTableViewCell.h"
#import "DejalActivityView.h"
#import "HelpViewController.h"

@interface SelectedServiceViewController ()
{
    MyObjects *objClass;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSSortDescriptor *sortDescriptor1;
    NSArray *sortDescriptors1;
    NSArray *arrAllObj;
    NSArray *arrtotalCount;
}
@end

@implementation SelectedServiceViewController
@synthesize mutableArray;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    objClass = [MyObjects shareManager];
    cMgr=[CacheManager getInstance];
    objectsMutablesw=[[NSMutableArray alloc]init];
    lblAccount.text=cMgr.accountName;
    objClass.isonSelectedServices=1;
    tblView.delegate=self;
    tblView.dataSource=self;

    //hide extra separators
    tblView.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];

    getReferal =[[NSMutableArray alloc]initWithObjects:@"land",@"lawn" ,@"tree",@"machanic",nil];
    
    getDecription=[[NSMutableArray alloc]initWithObjects:@"Signature service provides weekly mowing edging cleaning out planting bedsand trimming shrubs",@"Provides for preventative grub control in Spring and additional spot applications of insecticide as needed." ,@"Provides for preventative grub control in Spring and additional spot applications of insecticide as needed.",@"The home must qualify for this option in regards to roof type, garage door seal, and the construction of the crawl space.",nil];
    
    urgency=[[NSMutableArray alloc]initWithObjects:@"Hot",@"Cold" ,@"Mild",@"Hot",nil];
      AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _managedObjectContext= [appDelegate managedObjectContext];
    
    entityDesc1=[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext];
    request=[[NSFetchRequest alloc
              ]init];
    [request setEntity:entityDesc1];
    NSPredicate    *pred = [NSPredicate predicateWithFormat:@"(username= %@)",cMgr.strUsreName];
    [request setPredicate:pred];
    objectsMutable=[[NSMutableArray alloc]init];
    
    pImg=[[NSMutableArray alloc]init];
    pImg1=[[NSMutableArray alloc]init];
    
    
    entityDesc2=[NSEntityDescription entityForName:@"Imgpath"inManagedObjectContext:_managedObjectContext];
    request1=[[NSFetchRequest alloc
              ]init];
    [request1 setEntity:entityDesc2];
    NSPredicate    *pred1 = [NSPredicate predicateWithFormat:@"(username= %@)",cMgr.strUsreName];
    [request1 setPredicate:pred1];
    NSLog(@"%@",cMgr.selectedService);
    
    //===============================================================================
    
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"username" ascending:NO];
    
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];

    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    [self.fetchedResultsController setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    
    [self.fetchedResultsController performFetch:&error];
     arrtotalCount = [self.fetchedResultsController fetchedObjects];
    
    if ([[UIScreen mainScreen] bounds].size.height <= 736){
        self.height.constant = 60;
    }else{
        self.height.constant = 135;
    }
    
}
-(void)viewWillDisappear:(BOOL)animated
{
   objClass.isonSelectedServices=0;
}
-(void)viewWillAppear:(BOOL)animated
{
    isAlreadySend=NO;
    NSUserDefaults *de=[NSUserDefaults standardUserDefaults];
   strU=[de stringForKey:@"url"];
    NSError *error; NSPredicate   *pred = [NSPredicate predicateWithFormat:@"(autoId= %@)",cMgr.selectedService];
    
    [request setPredicate:pred];
    
    objects = [_managedObjectContext executeFetchRequest:request error:&error];
    //[[NSUserDefaults standardUserDefaults]setObject:objects forKey:@"objects"];
    objectsMutable=  [NSMutableArray  arrayWithArray:objects];
    
    
    NSPredicate   *pred1 = [NSPredicate predicateWithFormat:@"(autoidimg= %@)",cMgr.selectedService];
    
    [request1 setPredicate:pred1];
    
    objects1 = [_managedObjectContext executeFetchRequest:request1 error:&error];
    
    pImg=  [NSMutableArray  arrayWithArray:objects1];
    
   // NSLog(@"%ld",[objects1 count]);
    if ([objects count] == 0) {
    }
    else {
        
        lblAccountNum.text =cMgr.accountName;
        NSLog(@"%@",lblAccountNum.text);

        
    }
   
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [tblView beginUpdates];
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    //[tblView endUpdates];
}
- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    switch (type) {
        case NSFetchedResultsChangeInsert: {
            [tblView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeDelete: {
            [tblView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeUpdate: {
           [self configureCell:(IncidenceTableViewCell *)[tblView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        }
        case NSFetchedResultsChangeMove: {
            [tblView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tblView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *sections = [self.fetchedResultsController sections];
    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
   // totalCount=[sectionInfo numberOfObjects];
    return [sectionInfo numberOfObjects];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IncidenceTableViewCell *cell = (IncidenceTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure Table View Cell
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}
- (void)configureCell:(IncidenceTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Fetch Record
    NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [cell.ServiceType setText:[record valueForKey:@"serviceName"]];
    [cell.Description setText:[record valueForKey:@"comment"]];
    [cell.LevelOfUrgency setText:[record valueForKey:@"urgencyLevel"]];
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Deleting..."];
        if (record) {
            [self.fetchedResultsController.managedObjectContext deleteObject:record];
        }
        NSError *error;
        [_managedObjectContext save:&error];
        [self viewWillAppear:YES];
         arrtotalCount=[self.fetchedResultsController fetchedObjects];
        [self performSelector:@selector(StopActivityDezal) withObject:nil afterDelay:.60];
    }
}

-(void)StopActivityDezal{
    [DejalBezelActivityView removeView];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    cMgr.cacheIndex=(int)(long)indexPath.row;
    int k = cMgr.cacheIndex;
//    NSString *strTes=[NSString stringWithFormat:@"%d",k];
//    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"indexxxx" message:strTes delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
    NSString *strIndexPath = [NSString stringWithFormat:@"%d",k];
    NSUserDefaults *boolUserDefaults = [NSUserDefaults standardUserDefaults];
    [boolUserDefaults setValue:strIndexPath forKey:@"strIndex"];
    [boolUserDefaults setValue:strIndexPath forKey:@"strIndexNew"];
    [boolUserDefaults synchronize];

    cMgr.isUpdate=YES;
    UIStoryboard *sb;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        sb = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    }
    else
    {
        sb = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
    }

    NSUserDefaults *boolUserDefaults1 = [NSUserDefaults standardUserDefaults];
    [boolUserDefaults1 setBool:YES forKey:@"isFromUpdate"];
    [boolUserDefaults1 synchronize];

    AddleadViewController *vc = [sb instantiateViewControllerWithIdentifier:@"addLead"];
     vc.indexPathCoreData = strIndexPath;
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)goToiPadHelp:(id)sender {
    UIStoryboard *storyboard;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    }
    HelpViewController* lgn = (HelpViewController*)[storyboard instantiateViewControllerWithIdentifier:@"HelpViewControlleriPadSelected"];
    [self.navigationController presentViewController:lgn animated:NO completion:nil];
}

-(IBAction)back
{
//    DeveloperViewController *wc = [[UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"dev"];
//    [self.navigationController pushViewController:wc animated:YES];
//    cMgr.accountName=nil;
//    NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
//    [def setObject:nil forKey:@"zipcode"];
//    [def synchronize];
//    [self deleteCustomerTable];
//    [self deleteServiceDetailTable];
//    [self deleteImagePathTable];
//    [self deleteAudioPathTable];
    
    if (arrtotalCount.count==0) {
            DeveloperViewController *wc = [[UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"dev"];
            [self.navigationController pushViewController:wc animated:YES];
            cMgr.accountName=nil;
            NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
            [def setObject:nil forKey:@"zipcode"];
            [def synchronize];
            [self deleteCustomerTable];
            [self deleteServiceDetailTable];
            [self deleteImagePathTable];
            [self deleteAudioPathTable];
    } else {
    NSUserDefaults *boolUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *strIndexxx=[boolUserDefaults valueForKey:@"strIndexNew"];
    NSString *strIndexPath;
    if (strIndexxx.length==0) {
        strIndexPath= [NSString stringWithFormat:@"%lu",arrtotalCount.count-1];
    }
    else
    {
    strIndexPath = strIndexxx;
    }
    NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
    [defsss setValue:strIndexPath forKey:@"strIndex"];
    [defsss synchronize];
    
    cMgr.isUpdate=YES;
    UIStoryboard *sb;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        sb = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    }
    else
    {
        sb = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
    }
    
    NSUserDefaults *boolUserDefaults1 = [NSUserDefaults standardUserDefaults];
    [boolUserDefaults1 setBool:YES forKey:@"isFromUpdate"];
    [boolUserDefaults1 synchronize];
    
    AddleadViewController *vc = [sb instantiateViewControllerWithIdentifier:@"addLead"];
    vc.indexPathCoreData = strIndexPath;
    [self.navigationController popViewControllerAnimated:NO];
    }
}
-(void)deleteCustomerTable
{
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [_managedObjectContext executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [_managedObjectContext deleteObject:data];
    }
    NSError *saveError = nil;
    [_managedObjectContext save:&saveError];
}

-(void)deleteServiceDetailTable
{
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [_managedObjectContext executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [_managedObjectContext deleteObject:data];
    }
    NSError *saveError = nil;
    [_managedObjectContext save:&saveError];
}

-(void)deleteImagePathTable
{
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"Imgpath" inManagedObjectContext:_managedObjectContext]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [_managedObjectContext executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [_managedObjectContext deleteObject:data];
    }
    NSError *saveError = nil;
    [_managedObjectContext save:&saveError];
}

-(void)deleteAudioPathTable
{
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"Audiopath" inManagedObjectContext:_managedObjectContext]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [_managedObjectContext executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [_managedObjectContext deleteObject:data];
    }
    NSError *saveError = nil;
    [_managedObjectContext save:&saveError];
}

-(IBAction)submitTooutbox
{
    
    entityDesc3=[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext];
    request2=[[NSFetchRequest alloc
              ]init];
    [request2 setEntity:entityDesc3];

    sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"username" ascending:NO];
    
    sortDescriptors1 = [NSArray arrayWithObject:sortDescriptor1];
    
    [request2 setSortDescriptors:sortDescriptors1];
    
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    [self.fetchedResultsController setDelegate:self];
    
    // Perform Fetch
    NSError *errorr = nil;
    
    [self.fetchedResultsController performFetch:&errorr];
    arrAllObj = [self.fetchedResultsController fetchedObjects];

    if (arrAllObj.count==0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No leads available to send" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
    NSEntityDescription *entDescriptio1=[NSEntityDescription entityForName:@"ServiceDetailTable"  inManagedObjectContext:_managedObjectContext];
    NSFetchRequest *request11=[[NSFetchRequest alloc] init];
    [request11 setEntity:entDescriptio1];
    
    
    NSManagedObject *match=nil;
    NSError *error;
    NSArray *object=[_managedObjectContext executeFetchRequest:request1 error:&error];
    if([object count]==0)
    {
        
    }
    else
    {
        for (int i=0; i<[objects count] ; i++) {
            match=[object objectAtIndex:i];
            [match setValue:@"true" forKey:@"statuso"];
            [_managedObjectContext save:&error];
          }
        
        NSEntityDescription *CustomerTable=[NSEntityDescription entityForName:@"CustomerTable"  inManagedObjectContext:_managedObjectContext];
        NSFetchRequest *request5=[[NSFetchRequest alloc] init];
        [request5 setEntity:CustomerTable];
        NSPredicate    *pred1 = [NSPredicate predicateWithFormat:@"(username= %@)",cMgr.strUsreName];
        [request5 setPredicate:pred1];
        
        NSManagedObject *match=nil;
        NSError *error;
        NSArray *object=[_managedObjectContext executeFetchRequest:request5 error:&error];
        NSLog(@"%@",object);
        if([object count]==0)
        {
            
            
        }
        else
        {
            for (int i=0; i<[object count] ; i++) {
                
                match=[object objectAtIndex:i];
                
                [match setValue:@"true" forKey:@"statuso"];
                
                [_managedObjectContext save:&error];

            }

    }
    }
    

    
    UIStoryboard *storyboard;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    }
    else
    {
        
        storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
    }

    
    OutboxViewController * lgn = (OutboxViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Outbox"];
    // lgn.objectsMutable=[objectsMutable mutableCopy];
    
    if ([objClass.ArrOutBox count]>0) {
       objClass.ArrOutBox=[objClass.ArrOutBox arrayByAddingObjectsFromArray:objectsMutable];
    }
    else{
    objClass.ArrOutBox= [objectsMutable mutableCopy];
    }
    
    
   NSLog(@"object mutable ---%@",objectsMutable);
    
    //[[NSUserDefaults standardUserDefaults] setObject:objectsMutable forKey:@"KeyArr"];
    [self.navigationController  pushViewController:lgn animated:YES];
   
    
   // [self.navigationController popToRootViewControllerAnimated:YES];
    
    }
}
-(IBAction)addMore
{
     // cMgr.isAutoid=YES;
    NSUserDefaults *boolUserDefaults = [NSUserDefaults standardUserDefaults];
    [boolUserDefaults setBool:YES forKey:@"isFromAddMoreView"];
    [boolUserDefaults setBool:YES forKey:@"isFromAddMoreViewNew"];
    [boolUserDefaults setValue:nil forKey:@"strIndexNew"];
    [boolUserDefaults synchronize];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationAddLead" object:nil];
    [self.navigationController popViewControllerAnimated:NO];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
    AddleadViewController* lgn = (AddleadViewController*)[storyboard instantiateViewControllerWithIdentifier:@"addLead"];
    [self.navigationController  pushViewController:lgn animated:YES];
  
    
}


-(void)postdatattoserver
{
    
    isAlreadySend=YES;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _managedObjectContext= [appDelegate managedObjectContext];
    
    NSEntityDescription    *entityDesc11=[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext];
    
    NSFetchRequest  *request121=[[NSFetchRequest alloc
                                 ]init];
    //
    
    // [request setEntity:entityDesc];
    
    [request121 setEntity:entityDesc11];
    
    NSError *error;
    
    //objects = [_managedObjectContext executeFetchRequest:request error:&error];
    NSPredicate  *pred = [NSPredicate predicateWithFormat:@"(statuso= %@)",@"true"];
    [request121 setPredicate:pred];
    NSArray    *objects12 = [_managedObjectContext executeFetchRequest:request121 error:&error];
    
    
   // NSLog(@"array to send");
    
    NSMutableArray  *objectsMutables=[NSMutableArray arrayWithArray:objects12];
    
   // NSLog(@"%d",[objectsMutables count]);
    
    
    if ([objectsMutables count]==0) {
        cMgr.issSending=NO;
    }
    
    
    //ttp://staging.quacito.com/austin/ServiceHandler.ashx?key=insertMaster&AutoID=9336&SendDateTime=6/8/113&uname=ankit.kasliwal&AccountNo=54323&status=old
    
    
    //
    
    for (int i=0 ;i<[objectsMutables count]; i++) {
        
        
        NSString *post;
         NSString *strK=[[NSUserDefaults standardUserDefaults]valueForKey:@"key"];
              post = [NSString stringWithFormat:@"&AutoID=%@&SendDateTime=%@&uname=%@&comKey=%@&key=insertMaster",[[objectsMutables objectAtIndex:i]autoId],[[objectsMutables objectAtIndex:i]dateTimec],cMgr.cacheName,strK];
        NSLog(@"%@",post);
        NSLog(@"%@",[[objectsMutables objectAtIndex:i]dateTimec]);
        NSString  *post1;
        if ([[[objectsMutables objectAtIndex:i]statusCustomer] isEqualToString:@"new"])
            
        {
            NSEntityDescription   *entityDesc22=[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext];
            
            request=[[NSFetchRequest alloc
                      ]init];
            // [request setEntity:entityDesc];
            
            [request setEntity:entityDesc22];
            NSPredicate    *pred1 = [NSPredicate predicateWithFormat:@"(username= %@)",cMgr.strUsreName];
            [request setPredicate:pred1];

            NSError *error;
            
            //    objects = [_managedObjectContext executeFetchRequest:request error:&error];
            //
            
            
            pred = [NSPredicate predicateWithFormat:@"(autoId = %@)",[[objectsMutables objectAtIndex:i]autoId]];
            [request setPredicate:pred];
            NSArray  *objects2 = [_managedObjectContext executeFetchRequest:request error:&error];
            if ([objects2 count]>0) {
                post1=[NSString stringWithFormat:@"&FName=%@&LName=%@&Phno=%@&Email=%@&status=new",[[objects2 objectAtIndex:0]fName],[[objects2 objectAtIndex:0]lName],[[objects2 objectAtIndex:0]phNo],[[objects2 objectAtIndex:0]emailId]];
                post=[post stringByAppendingString:post1];
                
            }
          }
        
        else
        {
            
            post1=[NSString stringWithFormat:@"&AccountNo=%@&status=old",[[objectsMutables objectAtIndex:i]accountNo]];
            post=[post stringByAppendingString:post1];
        }
        
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        //    You need to send the actual length of your data. Calculate the length of the post string.
        
        
        //    3. Create a Urlrequest with all the properties like HTTP method, http header field with length of the post string.
        //    Create URLRequest object and initialize it.
        
        NSMutableURLRequest *request34 = [[NSMutableURLRequest alloc] init];
        
        //    Set the Url for which your going to send the data to that request.
        
        
        NSLog(@"%@",strU);
        strU =[strU stringByAppendingString:@"?"];
        [request34 setURL:[NSURL URLWithString:strU]];
        
        //    Now, set HTTP method (POST or GET).
        //    Write this lines as it is in your code.
        
        [request34 setHTTPMethod:@"POST"];
        
        //    Set HTTP header field with length of the post data.
        
        
        //    Also set the Encoded value for HTTP header Field.
        
        //  [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];
        
        //    Set the HTTPBody of the urlrequest with postData.
        
        [request34 setHTTPBody:postData];
        //    4. Now, create URLConnection object. Initialize it with the URLRequest.
        //   dispatch_async(dispatch_get_main_queue(), ^{
        //update UI here
        //
        NSURLConnection *conn = [[NSURLConnection alloc]initWithRequest:request34 delegate:self];
        
        //            NSHTTPURLResponse  *response = nil;
        //            [NSURLConnection sendSynchronousRequest:request1
        //                                  returningResponse:&response
        //                                              error:nil];
        
        if (conn) {
            NSLog(@"poc");
            NSFetchRequest * allCars3 = [[NSFetchRequest alloc] init];
            [allCars3 setEntity:[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext ]];
            [allCars3 setIncludesPropertyValues:NO]; //only fetch the managedObjectID
            NSPredicate *p1=[NSPredicate predicateWithFormat:@"autoId=%@",[[objectsMutables objectAtIndex:i]autoId],cMgr.strUsreName];
            [allCars3 setPredicate:p1];
            NSError * error1 = nil;
            NSArray * cars1 = [_managedObjectContext   executeFetchRequest:allCars3 error:&error1];
            
            NSManagedObject *match=nil;
            
            if([cars1 count]==0)
            {
                
            }
            else
            {
                for (int i=0; i<[cars1 count] ; i++)
                {
                    
                    match=[cars1 objectAtIndex:i];
                    
                    [match setValue:@"send" forKey:@"statuso"];
                    
                    [_managedObjectContext save:&error];
                    
                }
                
                
            }
            
            
        }
        
        else
        {
            
        }
        
        }
    NSEntityDescription *entityDesc13=[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext];
    
    NSFetchRequest *request25=[[NSFetchRequest alloc
                                ]init];
    // [request setEntity:entityDesc];
    
    [request25 setEntity:entityDesc13];
    //    objects = [_managedObjectContext executeFetchRequest:request error:&error];
    NSPredicate  *pred1 = [NSPredicate predicateWithFormat:@"(statuso= %@)",@"true"];
    [request setPredicate:pred1];
    NSArray  *objects123 = [_managedObjectContext executeFetchRequest:request25  error:&error];
    
    
    objectsMutablesw=[NSMutableArray arrayWithArray:objects123];
    
    for (int i = 0; i<[objectsMutablesw count]; i++)
    {
        
        imagNamelocal=[[objectsMutablesw objectAtIndex:i]imgName];
        
        if ([imagNamelocal isEqualToString:@""]) {
            imagNamelocal=@"";
        }
        
        else if ([imagNamelocal isEqualToString:@"null"]) {
            imagNamelocal=@"";
        }
        
        else if (imagNamelocal==nil) {
            imagNamelocal=@"";
        }
        NSLog(@"%@",[[objectsMutablesw objectAtIndex:i]imgName]);
        localPath=[[objectsMutablesw objectAtIndex:i]savedImagePath];
        
        audioNamelocal=[[objectsMutablesw objectAtIndex:i]audioName];
        
        
        if ([audioNamelocal isEqualToString:@""]) {
            audioNamelocal=@"";
        }
        
        else if ([audioNamelocal isEqualToString:@"null"]) {
            audioNamelocal=@"";
        }
        else if (audioNamelocal==nil) {
            audioNamelocal=@"";
        }
        localaudioPath=[[objectsMutablesw objectAtIndex:i]audioPath];
        [RemoveIdv insertObject:[[objectsMutablesw objectAtIndex:i]autoId] atIndex:i];
        NSString *postn;
        
        postn = [NSString stringWithFormat:@"&autoid=%@&serviceid=%@&servicename=%@&images=%@&audio=%@&comment=%@&urgency=%@&acno=%@&key=detailinsert",[[objectsMutablesw objectAtIndex:i]autoId],[[objectsMutablesw objectAtIndex:i]serviceId],[[objectsMutablesw objectAtIndex:i]serviceName], imagNamelocal,audioNamelocal,[[objectsMutablesw objectAtIndex:i]comment],[[objectsMutablesw objectAtIndex:i]urgencyLevel],[[objectsMutablesw objectAtIndex:i]accountNo]];
        
        NSData *postData1 = [postn dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        //    You need to send the actual length of your data. Calculate the length of the post string.
        
        
        //    3. Create a Urlrequest with all the properties like HTTP method, http header field with length of the post string.
        //    Create URLRequest object and initialize it.
        
        NSMutableURLRequest *request2 = [[NSMutableURLRequest alloc] init];
        
        //    Set the Url for which your going to send the data to that request.
        
        [request2 setURL:[NSURL URLWithString:strU]];
        
        //    Now, set HTTP method (POST or GET).
        //    Write this lines as it is in your code.
        [request2 setHTTPMethod:@"POST"];
        
        //    Set HTTP header field with length of the post data.
        
        
        //    Also set the Encoded value for HTTP header Field.
        
        //  [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];
        
        //    Set the HTTPBody of the urlrequest with postData.
        
        [request2 setHTTPBody:postData1];
        
        
        //    4. Now, create URLConnection object. Initialize it with the URLRequest.
        //update UI here
        
        
        NSURLConnection *conn1 = [[NSURLConnection alloc]initWithRequest:request2 delegate:self];
        
        if(conn1)
        {
            
            //NSLog(@"Connection Successful”);
            
            NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
            [allCars setEntity:[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext ]];
            [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
            NSPredicate *p=[NSPredicate predicateWithFormat:@"iD=%@", [[objectsMutablesw objectAtIndex:i]iD]];
            [allCars setPredicate:p];
            NSError * error = nil;
            NSArray * cars = [_managedObjectContext    executeFetchRequest:allCars error:&error];
            //
            //        //error handling goes here
             NSManagedObject *match=nil;
            if([cars count]==0)
            {
            }
            else
            {
                for (int i=0; i<[cars count] ; i++)
                {
                     match=[cars objectAtIndex:i];
                    
                    [match setValue:@"send" forKey:@"statuso"];
                     [_managedObjectContext save:&error];
                    
                }
            }
        }
        else
        {
            //  NSLog(@"Connection could not be made”);
        }
        
        //    It returns the initialized url connection and begins to load the data for the url request. You can check that whether you URL connection is done properly or not using just if/else statement as below.
        cMgr.issSending=NO;
        //[self sendAudio];
        
    }
    
    
      
    cMgr.issSending=NO;
    [self sendimageToserver];

    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
    
    UIStoryboard *storyboard;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    }
    else
    {
        
        storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
    }

    
    OutboxViewController * lgn = (OutboxViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Outbox"];
    if (isAlreadySend==NO) {
        lgn.objectsMutable=[objectsMutable mutableCopy];
    }
    [self.navigationController  pushViewController:lgn animated:YES];
    
    cMgr.isAutoid=YES;
    }

-(void)sendimageToserver
{
    
    if (!cMgr.isImagesending)
    {
        
        
        cMgr.isImagesending=YES;
        NSEntityDescription  *entityDesc4=[NSEntityDescription entityForName:@"Imgpath" inManagedObjectContext:_managedObjectContext];
        
        NSFetchRequest *request2=[[NSFetchRequest alloc
                                   ]init];
        NSPredicate    *pred1 = [NSPredicate predicateWithFormat:@"(username= %@)",cMgr.strUsreName];
        [request2 setPredicate:pred1];
        
        [request2 setEntity:entityDesc4];
        
        
        
        //    objects = [_managedObjectContext executeFetchRequest:request error:&error];
        //
        
        //
        NSError *error=nil;
        NSArray    *objects2 = [_managedObjectContext executeFetchRequest:request2 error:&error];
        
        
        NSMutableArray  *objectsMutables1=[NSMutableArray arrayWithArray:objects2];
        
        NSLog(@"%d",[objectsMutables1 count]);
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            
            
            for (int i=0; i<[objectsMutables1 count]; i++)
            {
            
                
                NSData *imageData;
                
                UIImage *_img = [UIImage imageWithContentsOfFile:[[objectsMutables1 objectAtIndex:i]pathImg]];
                
                NSLog(@"%@",[[objectsMutables1 objectAtIndex:i]pathImg]);
                removalId=[[objectsMutables1 objectAtIndex:i]imgNameimg];
                UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,1.0,1)];
                imgView.image = _img;
                imageData =  UIImageJPEGRepresentation(imgView.image, 0.00000000000000000000000000001f);
                // NSLog(@"%@",imageData);
                // [self.view addSubview:imgView];
                
                // setting up the request object now
                //  	// setting up the URL to post to
                //http://myanteater.com/ImageUploader.ashx
                NSString *st=[strU stringByReplacingOccurrencesOfString:@"ServiceHandler.ashx" withString:@"ImageUploader.ashx"];
                NSString *urlString = [NSString stringWithFormat:@"%@",st];
                
                
                NSMutableURLRequest *request13 = [[NSMutableURLRequest alloc] init];
                [request13 setURL:[NSURL URLWithString:urlString]];
                [request13 setHTTPMethod:@"POST"];
                
                /*
                 add some header info now
                 we always need a boundary when we post a file
                 also we need to set the content type
                 
                 You might want to generate a random boundary.. this is just the same
                 as my output from wireshark on a valid html post
                 */
                NSString *boundary = @"---------------------------14737809831466499882746641449";
                NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
                [request13 addValue:contentType forHTTPHeaderField: @"Content-Type"];
                
                /*
                 now lets create the body of the post
                 */
                NSMutableData *body = [NSMutableData data];
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                
                
                NSString *combine =[[objectsMutables1 objectAtIndex:i]imgNameimg];
                
                
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@.png\"\r\n",combine] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[NSData dataWithData:imageData]];
                // NSLog(@"%@",imageData);
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                // setting the body of the post to the reqeust
                [request13 setHTTPBody:body];
                
                // now lets make the connection to the web
                
                
                //  dispatch_async(dispatch_get_main_queue(), ^{
                //update UI here
                
                NSData *returnData = [NSURLConnection sendSynchronousRequest:request13 returningResponse:nil error:nil];
                
                
                
                
                NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
                
                chkforResponse=returnString;
                NSLog(@"%@",returnString);
                //  });
                
                if ([ chkforResponse isEqualToString:@"OK"] ) {
                    
                  
                    
                                        //
                    //            combine =[combine stringByAppendingString:@".png"];
                    //            NSString *comma=@",";
                    //            combine=[combine stringByAppendingString:comma];
                    //            NSString *anthr=;
                    //            anthr=[anthr stringByAppendingString:@".png"];
                    //            NSString *yhi=[combine stringByAppendingString:anthr];
                    //            NSLog(@"%@",yhi);
                    NSData *imgd4;
                    
                    UIImage *img1 = [UIImage imageWithContentsOfFile:[[objectsMutables1 objectAtIndex:i]pathImg1]];
                    
                    NSLog(@"%@",[[objectsMutables1 objectAtIndex:i]pathImg1]);
                    removalId=[[objectsMutables1 objectAtIndex:i]imgNameimg];
                    UIImageView *imgView1 =  [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,1.0,1.0)];
                    imgView1.image = img1;
                    
                    
                    imgd4 =  UIImageJPEGRepresentation(imgView1.image, 0.00000000000000000000000000001f);
                    NSLog(@"%@",imgd4);
                    // [self.view addSubview:imgView];
                    
                    
                    //  	// setting up the URL to post to
                    
                    
                    // setting up the request object now
                    
                    NSString *st=[strU stringByReplacingOccurrencesOfString:@"ServiceHandler.ashx" withString:@"ImageUploader.ashx"];
                    NSString *urlString = [NSString stringWithFormat:@"%@",st];
                    
                    
                    NSMutableURLRequest *request14 = [[NSMutableURLRequest alloc] init];
                    [request14 setURL:[NSURL URLWithString:urlString]];
                    [request14 setHTTPMethod:@"POST"];
                    
                    /*
                     add some header info now
                     we always need a boundary when we post a file
                     also we need to set the content type
                     
                     You might want to generate a random boundary.. this is just the same
                     as my output from wireshark on a valid html post
                     */
                    NSString *boundary = @"---------------------------14737809831466499882746641449";
                    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
                    [request14 addValue:contentType forHTTPHeaderField: @"Content-Type"];
                    
                    /*
                     now lets create the body of the post
                     */
                    NSMutableData *body = [NSMutableData data];
                    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    
                    NSString *combine =[[objectsMutables1 objectAtIndex:i]imgNameimg1];
                    
                    
                    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@.png\"\r\n",combine] dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:[NSData dataWithData:imgd4]];
                    NSLog(@"%@",imgd4);
                    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    // setting the body of the post to the reqeust
                    [request14 setHTTPBody:body];
                    
                    // now lets make the connection to the web
                    
                    
                    
                    //   dispatch_async(dispatch_get_main_queue(), ^{
                    //update UI here
                    
                    NSData *returnData = [NSURLConnection sendSynchronousRequest:request14 returningResponse:nil error:nil];
                    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
                    
                    
                    
                    
                    NSLog(@"%@",returnString);
                    //   });
                    NSFetchRequest * allCars3 = [[NSFetchRequest alloc] init];
                    [allCars3 setEntity:[NSEntityDescription entityForName:@"Imgpath" inManagedObjectContext:_managedObjectContext ]];
                    [allCars3 setIncludesPropertyValues:NO]; //only fetch the managedObjectID
                    NSPredicate *p1=[NSPredicate predicateWithFormat:@"imgNameimg =%@ username =%@",removalId,cMgr.strUsreName];
                    [allCars3 setPredicate:p1];
                    NSError * error1 = nil;
                    NSArray * cars1 = [_managedObjectContext    executeFetchRequest:allCars3 error:&error1];
                    //
                    //        //error handling goes here
                    for (NSManagedObject * car in cars1) {
                        //
                        //
                      //  [_managedObjectContext  deleteObject:car];
                      //  NSError *saveError = nil;
                      //  [_managedObjectContext save:&saveError];
                    }
                    cMgr.isImagesending=NO;
                }
                
            }
            
            
        });
        
    }
    
}

-(void)ArrCountAgain{
    entityDesc1=[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext];
    request=[[NSFetchRequest alloc
              ]init];
    [request setEntity:entityDesc1];
    NSPredicate    *pred = [NSPredicate predicateWithFormat:@"(username= %@)",cMgr.strUsreName];
    [request setPredicate:pred];
    objectsMutable=[[NSMutableArray alloc]init];
    
    pImg=[[NSMutableArray alloc]init];
    pImg1=[[NSMutableArray alloc]init];
    
    
    entityDesc2=[NSEntityDescription entityForName:@"Imgpath"inManagedObjectContext:_managedObjectContext];
    request1=[[NSFetchRequest alloc
               ]init];
    [request1 setEntity:entityDesc2];
    NSPredicate    *pred1 = [NSPredicate predicateWithFormat:@"(username= %@)",cMgr.strUsreName];
    [request1 setPredicate:pred1];
    NSLog(@"%@",cMgr.selectedService);
    
    //===============================================================================
    
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"username" ascending:NO];
    
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    [self.fetchedResultsController setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    
    [self.fetchedResultsController performFetch:&error];
    arrtotalCount = [self.fetchedResultsController fetchedObjects];
}

@end
