//
//  Help1ViewController.h
//  Lead Now
//
//  Created by Saavan Patidar on 02/08/22.
//  Copyright © 2022 Infocrats. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Help1ViewController : UIViewController
- (IBAction)back:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height;

@end

NS_ASSUME_NONNULL_END
