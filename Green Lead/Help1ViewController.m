//
//  Help1ViewController.m
//  Lead Now
//
//  Created by Saavan Patidar on 02/08/22.
//  Copyright © 2022 Infocrats. All rights reserved.
//

#import "Help1ViewController.h"

@interface Help1ViewController ()

@end

@implementation Help1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([[UIScreen mainScreen] bounds].size.height <= 736){
        self.height.constant = 60;
    }else{
        self.height.constant = 135;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back:(id)sender {
    NSLog(@"hello");
    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
