//
//  AddleadViewController.h
//  Green Lead
//
//  Created by Rakesh Jain on 19/09/13.
//  Copyright (c) 2013 Infocrats. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMLParser.h"
#import "GetService.h"
#import "Imgpath.h"
#import "CustomerTable.h"
#import "CacheManager.h"
#import "SelectedServiceViewController.h"
#import "ServiceDetailTable.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "M13BadgeView.h"
#import <CoreData/CoreData.h>

@interface AddleadViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate,AVAudioRecorderDelegate>
{
    
    
    NSMutableArray *arrayForobjects;
    NSString *imgIdtoupdate;
    NSString *image11;
    NSString *image22;
    NSString *image33;
    NSString *image44;
    BOOL isRecording;
    //For recording
    NSMutableDictionary *recordSetting;
    NSString *recorderFilePath;
    AVAudioRecorder *recorder;

    ServiceDetailTable *serviceDetail;
    CacheManager *cMgr;
   // selectedServices *selectService;
    IBOutlet UILabel *lblAccount;
    
    IBOutlet   UITextField *txtaccountNumber;
    IBOutlet UIScrollView *scrollView;
    UIImage *checkmarkImage;

    NSString *lId,*leadcircle;

   IBOutlet  UIImageView *chkh,*chkc,*chkm,*chckCustomer;
    IBOutlet UITextView *txtviewComment;
   IBOutlet UITextField *tex1,*tex2,*tex3,*tex4,*tex5,*tex6;
    
    UIActionSheet *pickerSheet;
    UIPickerView *pickerView,*pickerViewforHotColdMIld;
    UIToolbar *pickerToolbar;
    
    IBOutlet UILabel *lblService;
    
    CustomerTable *customerTable;
    NSManagedObjectContext *context;
    
    NSEntityDescription *entityDesc,*entityDesc1,*entityDesc2,*entityDesc3;
    
    IBOutlet UIButton *BackStopAudio;
    Imgpath *imgpathsub;
  
    NSFetchedResultsController *fetchedResultsController;
    NSFetchRequest *request,*request1,*request2,*request3;
    
    NSString *lservice_id;
    NSArray *objects,*objectsarray,*objects1,*objects2;
    UIImagePickerController *imgCapture;
    UIAlertController *alertController;
    
    IBOutlet UIImageView *imgShow,*imgShow1,*imgShow2,*imgShow3;
    
    IBOutlet UILabel *hcmLabel;
    
    NSString *audioName;
    
    NSString *savedImagePath,*savedImagePath1,*savedImagePath2,*savedImagePath3;
    NSString *savedImagePathUpdate,*savedImagePathUpdate1,*savedImagePathUpdate2,*savedImagePathUpdate3;
    NSString *autoid;
    NSString *dateSetg,*datesetg1,*datesetg2,*datesetg3,*dateID;
    NSString *dateSetgUpdate,*datesetgUpdate1,*datesetgUpdate2,*datesetgUpdate3;
    NSString *FinalImageName;
    NSString *FinalImageNameUpdate;
    NSString *FinalImageAfterEdit;
    int position;
    NSString *iDToUpdate;
    IBOutlet  UILabel *lblTimer;
    IBOutlet UIImageView *changeImgbtn;
    
    int totalsecond;
    NSTimer *twoMinTimer;
  
   IBOutlet UIButton *btn1,*btn2;
    
    UILongPressGestureRecognizer *longPressGesture;
    
    
    NSArray *objVersion;
    NSMutableArray *myArrayOfNames;
    NSMutableArray *myArrayOfNames1;
    NSString *mainStringName;
    NSString *mainStringName1;
    
    //*********IPad************//
    IBOutlet UIButton *btnSelectService;
    IBOutlet UIButton *btnLevelOfUrgency;
    NSString *indexPathCoreData;
    UIPopoverController *popOverView;
    
    //***********************//
}
- (IBAction)goToAddHelp:(id)sender;
-(IBAction)removeImage;
-(IBAction)backforRecording;
-(IBAction)addMasterdata;
-(IBAction)showingToCustomer;
-(IBAction)showvalueondropdown;
-(IBAction)showvalueondropdownHot;
-(IBAction)back;
-(IBAction)chkHcm:(UIButton*)button;
-(IBAction)addleadValue;
-(IBAction)saveServiceId;
-(IBAction)startRecording;
-(IBAction)captureImage;
- (IBAction)stopRecording;
-(IBAction)imagefromgallery;
-(IBAction)actionForRecordingandTimer;
@property (nonatomic, retain) NSManagedObjectContext   *managedObjectContext;
@property(nonatomic,retain) UIImagePickerController *imgCapture;
@property(nonatomic,retain)UIImageView *imgShow,*imgShow1;
@property(nonatomic,retain) UIImageView *changeImgbtn;
@property(nonatomic,retain)  IBOutlet UITextView *txtviewComment;
@property(nonatomic) NSString *indexPathCoreData;
@property (nonatomic, retain) M13BadgeView *badgeView;
@property (strong, nonatomic) IBOutlet UIButton *hideNxtBtn;
@property (strong, nonatomic) IBOutlet UIButton *afterImageCaptureNxtBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height;


@end
