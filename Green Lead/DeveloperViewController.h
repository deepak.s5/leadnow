//
//  DeveloperViewController.h
//  Green Lead
//
//  Created by Rakesh Jain on 18/09/13.
//  Copyright (c) 2013 Infocrats. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CacheManager.h"
#import "GetService.h"
#import "CustomerTable.h"
#import "XMLParser.h"
#import "AppDelegate.h"
#import "ServiceDetailTable.h"
#import "GetService.h"
#import "Audiopath.h"
#import "XMLReader.h"
#import "M13BadgeView.h"
@interface DeveloperViewController : UIViewController<UITabBarControllerDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,NSFetchedResultsControllerDelegate>
{
    
    IBOutlet UISegmentedControl *segment;

   GetService *getService;
    IBOutlet UITextView *txtviewComment;
    Audiopath *audioPathData;
    NSManagedObjectContext *context;
    NSEntityDescription *entityDesc;
    NSFetchRequest *request;
    NSArray *objects;
    UILabel *lbl;
    UILabel *lblSetting;
    CacheManager *cMgr;
 IBOutlet   UITabBarController *tabbarController;
 IBOutlet   UITableView *tablView;

    UIImageView *developerTabImageView;
    UIImageView  *homeTabImageView;
    
     UIImageView  *outboxImageView;
     UIImageView *settingViewImg;
    
    
    UIImageView  *logoutboxImageView;
    IBOutlet   UITextField *tex1;

    NSString *autoid;
    NSMutableData *responseData;
    
    NSString *removalId;
    
    NSMutableArray *arrayvalus;
    NSString *strU;
    NSString *chkforResponse;
    NSString *imagNamelocal;
    NSMutableArray
    *objectsMutablesw;
    
    NSString *localPath;
    NSString *localaudioPath;
    UIButton *btnHistory;
    NSString *audioNamelocal;
     NSMutableArray *RemoveIdv;
    IBOutlet UITextField *txtZip;
    IBOutlet UIImageView *imgZipBackground;
    IBOutlet UILabel *lblZipTitle;
    NSMutableArray *arrayForsendinga;
    NSDictionary *dictRsponseFromUrl;
    IBOutlet UIActivityIndicatorView *activity;
    AppDelegate *appDelegate;
    CGFloat   animatedDistance;
    BOOL isGoToOutBoxCall;
}
@property (strong, nonatomic) IBOutlet UILabel *lblCompanyName;
@property (strong, nonatomic) IBOutlet UIButton *helpBtnNew;

- (IBAction)goToiPadHelp:(id)sender;
@property (nonatomic, retain) NSManagedObjectContext   *managedObjectContext;
@property(nonatomic,retain)IBOutlet   UITableView *tablView;
@property (nonatomic, retain) M13BadgeView *badgeView;
-(IBAction)addLead;
-(IBAction)loadAllDataForDrop;
-(IBAction)openHelp;
@property (strong, nonatomic) IBOutlet UILabel *lblExistingCustomer;
-(IBAction)newCustomer;
@property (strong, nonatomic) IBOutlet UILabel *lblFirstName;
@property (strong, nonatomic) IBOutlet UILabel *lblLastName;
@property (strong, nonatomic) IBOutlet UILabel *lblPhone;
@property (strong, nonatomic) IBOutlet UILabel *lblEmail;
@property (strong, nonatomic) IBOutlet UIImageView *imgfirst;
@property (strong, nonatomic) IBOutlet UIImageView *imgLast;
@property (strong, nonatomic) IBOutlet UIImageView *imgPhone;
@property (strong, nonatomic) IBOutlet UIImageView *imgEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtFirstName;
@property (strong, nonatomic) IBOutlet UITextField *txtLastName;
@property (strong, nonatomic) IBOutlet UITextField *txtPhoneNo;
@property (strong, nonatomic) IBOutlet UITextField *txtEmailId;
@property (strong, nonatomic) IBOutlet UIButton *addLeadBtnnn;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
- (IBAction)VersionInfoAction:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightImage;


@end
