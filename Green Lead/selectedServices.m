//
//  selectedServices.m
//  Green Lead
//
//  Created by Rakesh Jain on 19/09/13.
//  Copyright (c) 2013 Infocrats. All rights reserved.
//

#import "selectedServices.h"

@implementation selectedServices

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
