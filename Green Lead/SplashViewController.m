//
//  SplashViewController.m
//  Lead Now
//
//  Created by Saavan Patidar on 15/05/17.
//  Copyright © 2017 Infocrats. All rights reserved.
//

#import "SplashViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "XMLParser.h"
#import "DeveloperViewController.h"
#import "TabbbarViewController.h"
#import "Reachability.h"
#import "NewcustomerViewController.h"
#import "MainViewController.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
    
    BOOL isFromAppDelegate=[defss boolForKey:@"FromAppDelegate"];
    
    if (isFromAppDelegate) {
        
        [defss setBool:YES forKey:@"FromSplashView"];
        [defss setBool:NO forKey:@"FromAppDelegate"];
        [defss synchronize];
        
        [self performSelector:@selector(goToLoginView) withObject:nil afterDelay:0.3];

    } else {
        
        [self performSelector:@selector(goToNextView) withObject:nil afterDelay:0.3];
        
    }

    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSString *strImageName = [NSString stringWithFormat:@"%@",[defsLogindDetail valueForKey:@"Logo"]];
    NSString *strUrl = [[NSString alloc] initWithFormat:@"%@/C_Logo/%@",MainUrlLogo,strImageName];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",strImageName]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        
        [self loadImage:strImageName];
        
    } else {
        NSString *newStrUrlString =[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url_For_Ad_Image = [[NSURL alloc] initWithString:newStrUrlString];
        NSData *data_For_Ad_Image = [[NSData alloc] initWithContentsOfURL:url_For_Ad_Image];
        UIImage *temp_Ad_Image = [[UIImage alloc] initWithData:data_For_Ad_Image];
        [self saveImage:temp_Ad_Image :strImageName];
        
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)saveImage: (UIImage*)image :(NSString*)strImageName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",strImageName]];
    NSData* data = UIImagePNGRepresentation(image);
    [data writeToFile:path atomically:YES];
    
    [self loadImage:strImageName];
}

- (UIImage*)loadImage :(NSString*)name {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    
    _splashImage.image=image;
    
    return image;
}


-(void)goToNextView{
    
    NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
    BOOL isAD_Existing_Customer =[defsss boolForKey:@"AD_Existing_Customer"];
    BOOL isAD_New_Customer =[defsss boolForKey:@"AD_New_Customer"];
    BOOL isAD_Both =[defsss boolForKey:@"AD_Both"];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if (isAD_Both) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
            TabbbarViewController* verificationBadgesViewController = (TabbbarViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Developer"];
            [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
        } else if (isAD_Existing_Customer){
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
            TabbbarViewController* verificationBadgesViewController = (TabbbarViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Developer"];
            [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
        }else if (isAD_New_Customer){
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
            TabbbarViewController* verificationBadgesViewController = (TabbbarViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Developer"];
            [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
        }
    }
    else
    {
        //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
        
        UIStoryboard *storyboard;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
        }
        else
        {
            
            storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
        }
        if (isAD_Both) {
            TabbbarViewController* verificationBadgesViewController = (TabbbarViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Developer"];
            [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
        } else if (isAD_Existing_Customer){
            TabbbarViewController* verificationBadgesViewController = (TabbbarViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Developer"];
            [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
        }else if (isAD_New_Customer){
            //                NewcustomerViewController* lgn = (NewcustomerViewController*)[storyboard instantiateViewControllerWithIdentifier:@"new"];
            //                [self.navigationController  pushViewController:lgn animated:NO];
            TabbbarViewController* verificationBadgesViewController = (TabbbarViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Developer"];
            [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
        }
    }
}

-(void)goToLoginView{
    
    
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad"
                                                                 bundle: nil];
        MainViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"log"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
        
    }else{
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone"
                                                                 bundle: nil];
        MainViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"log"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }

    /*
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
        MainViewController *verificationBadgesViewController1 = (MainViewController *)[storyboard instantiateViewControllerWithIdentifier:@"log"];
        
        self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:verificationBadgesViewController1];
        
        [self.window makeKeyAndVisible];
        
        
    }else{
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
        MainViewController *verificationBadgesViewController1 = (MainViewController *)[storyboard instantiateViewControllerWithIdentifier:@"log"];
        
        self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:verificationBadgesViewController1];
        
        [self.window makeKeyAndVisible];
        
    }

     */
    
    
}

@end
