//
//  MyObjects.h
//  Lead Now
//
//  Created by Rakesh jain on 01/05/15.
//  Copyright (c) 2015 Infocrats. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyObjects : NSObject
{
    int isonSelectedServices;
    int LeadCountOutBox;
    NSMutableArray *ArrOutBox;
}
+(MyObjects *)shareManager;
@property(nonatomic,assign)int isonSelectedServices;
@property(nonatomic,assign)int LeadCountOutBox;
@property(nonatomic,retain)NSMutableArray *ArrOutBox;
@end
