//
//  CustomerTable+CoreDataProperties.h
//  Lead Now
//
//  Created by Rakesh Jain on 18/02/16.
//  Copyright © 2016 Infocrats. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CustomerTable.h"

NS_ASSUME_NONNULL_BEGIN

@interface CustomerTable (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *accountNo;
@property (nullable, nonatomic, retain) NSString *autoId;
@property (nullable, nonatomic, retain) NSString *dateTimec;
@property (nullable, nonatomic, retain) NSString *emailId;
@property (nullable, nonatomic, retain) NSString *fName;
@property (nullable, nonatomic, retain) NSString *lName;
@property (nullable, nonatomic, retain) NSString *phNo;
@property (nullable, nonatomic, retain) NSString *status1;
@property (nullable, nonatomic, retain) NSString *statusCustomer;
@property (nullable, nonatomic, retain) NSString *statuso;
@property (nullable, nonatomic, retain) NSString *userkey;
@property (nullable, nonatomic, retain) NSString *username;
@property (nullable, nonatomic, retain) NSString *address1;
@property (nullable, nonatomic, retain) NSString *address2;
@property (nullable, nonatomic, retain) NSString *city;
@property (nullable, nonatomic, retain) NSString *stateeee;

@end

NS_ASSUME_NONNULL_END
