//
//  SimpleTableCell.h
//  SimpleTable
//
//  Created by Simon Ng on 28/4/12.
//  Copyright (c) 2012 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryCell : UITableViewCell
{


}

@property (nonatomic, weak)IBOutlet UILabel *lblName;
@property (nonatomic, weak)IBOutlet UILabel *lblAcNo;
@property (nonatomic, weak)IBOutlet UILabel *lblDate;
@property (nonatomic,weak) IBOutlet UILabel *lblstatus;
@property (strong, nonatomic) IBOutlet UILabel *lblComment;

@end
