

//
//  AppDelegate.m
//  Green Lead
//
//  Created by Rakesh Jain on 17/09/13.
//  Copyright (c) 2013 Infocrats. All rights reserved.

#import "AppDelegate.h"
#import "XMLParser.h"
#import "MainViewController.h"
#import "DejalActivityView.h"
#import "MyObjects.h"
#import "SplashViewController.h"
#import "Reachability.h"
#import "Audiopath.h"

@implementation AppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize uuidCustom,activeCompanyKey;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType: completionHandler:)]) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            // Will get here on both iOS 7 & 8 even though camera permissions weren't required
            // until iOS 8. So for iOS 7 permission will always be granted.
            if (granted) {
                // Permission has been granted. Use dispatch_async for any UI updating
                // code because this block may be executed in a thread.
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    // Permission has been Granted.
                    
                });
            } else {
                // Permission has been denied.
            }
        }];
    } else {
        
        // Permission has been Granted.
        
    }
    
    if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType: completionHandler:)]) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {
            // Will get here on both iOS 7 & 8 even though camera permissions weren't required
            // until iOS 8. So for iOS 7 permission will always be granted.
            if (granted) {
                // Permission has been granted. Use dispatch_async for any UI updating
                // code because this block may be executed in a thread.
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    // Permission has been Granted.
                    
                });
            } else {
                // Permission has been denied.
            }
        }];
    } else {
    }
    ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
    [lib enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        NSLog(@"%zd", [group numberOfAssets]);
    } failureBlock:^(NSError *error) {
        if (error.code == ALAssetsLibraryAccessUserDeniedError) {
            NSLog(@"user denied access, code: %zd", error.code);
        } else {
            NSLog(@"Other error code: %zd", error.code);
        }
    }];
    BOOL downloaded = [[NSUserDefaults standardUserDefaults] boolForKey: @"dateInstall"];
    if (!downloaded) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd/MM/yyyy hh:mm:ss a"];
        NSDate *now = [[NSDate alloc] init];
        NSString  *theDate = [dateFormat stringFromDate:now];
        NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
        [defs setValue:theDate forKey:@"DateAppInstalled"];
        [defs synchronize];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey: @"dateInstall"];
    }
//NSInteger intRandom =   arc4random();
//NSUInteger r = arc4random_uniform(1000000000);
//    NSLog(@"%ld",(long)r);
    //Override point for customization after application launch.
     cMgr=[CacheManager getInstance];
 //   [self setBadge11];
   NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
    [def setObject:@"" forKey:@"zipcode"];
    
    [def setObject:@"" forKey:@"acomment"];
    [def synchronize];
     objClass = [MyObjects shareManager];
     objClass.isonSelectedServices=0;
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    
     if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
     [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
       [[UIApplication sharedApplication] registerForRemoteNotifications];
   }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
  //  [UIApplication sharedApplication].idleTimerDisabled = YES;
   
    UIApplicationState state = [application applicationState];
    UILocalNotification *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (notification) {
        //NSLog(@"app recieved notification from remote%@",notification);
        [self application:application didReceiveRemoteNotification:(NSDictionary*)notification];
    }else{
       // NSLog(@"app did not recieve notification");
    }
    
     [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    NSLog(@"%d",valueBadge);
     if (state == UIApplicationStateActive) {
     NSDictionary * pushDictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
   //
        if (pushDictionary) {
        NSString *cancelTitle = @"Close";
        
       // NSString *showTitle = [pushDictionary valueForKey:@"title"];
        NSString *message = [pushDictionary valueForKey:@"ci"];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"LeadNow"
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:cancelTitle
                                                  otherButtonTitles:nil, nil];
        [alertView show];
        // [UIApplication sharedApplication].applicationIconBadgeNumber =(unsigned long)strBadgeNo;
        }
       
    }
   // NSLog(@"chase manager %@",cMgr.cacheName);
  
    NSUserDefaults *df=[NSUserDefaults standardUserDefaults];
    NSString *myInt = [df objectForKey:@"Remember"];
    if ([myInt isEqualToString:@"YES"]) {
        
        NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];        
        [defss setBool:YES forKey:@"FromAppDelegate"];
        [defss synchronize];
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
            SplashViewController *verificationBadgesViewController1 = (SplashViewController *)[storyboard instantiateViewControllerWithIdentifier:@"SplashViewController"];
            
            self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:verificationBadgesViewController1];
            
            [self.window makeKeyAndVisible];

            
        }else{
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
            SplashViewController *verificationBadgesViewController1 = (SplashViewController *)[storyboard instantiateViewControllerWithIdentifier:@"SplashViewController"];
            
            self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:verificationBadgesViewController1];
            
            [self.window makeKeyAndVisible];
            
        }

    }else{
        
        NSLog(@"No Remember");
        MainViewController *controller = (MainViewController *)self.window.rootViewController.navigationController;
        controller.managedObjectContext = self.managedObjectContext;

        
    }
    
    // [self deleteDatabase];
    // [self deleteDatabase1];
    
    
    //NSURLRequest *requesting = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://myanteater.com/ServiceHandler.ashx?key=getMaxCount"]];
    //[[NSURLConnection alloc] initWithRequest:requesting delegate:self];
    //[self getService];
   // [self deleteDatabase];
    //[self deleteDatabase1];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(getEmployeeByroll)
//                                            name:@"checkversionData" object:nil];
    //
//    if (@available(iOS 13.0, *)) {
//        UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame] ;
//        statusBar.backgroundColor = [UIColor redColor];
//        [[UIApplication sharedApplication].keyWindow addSubview:statusBar];
//    } else {
//        
//        [[[UIApplication sharedApplication]valueForKey:@"statusBarWindow.statusBar"] backgroundColor] = UIColor.redColor ;
//        
//    }
    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    
   return YES;
}
-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    //NSLog(@"Failed to get token, error: %@", error);
}
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    for (id key in userInfo) {
      //  NSLog(@"key: %@, value: %@", key, [userInfo objectForKey:key]);
        
        [self addMessageFromRemoteNotification:userInfo updateUI:YES];
      UIApplicationState state = [application applicationState];
            if (state == UIApplicationStateActive) {
                NSString *cancelTitle = @"Close";
               // NSString *showTitle = [userInfo valueForKey:@"title"];
                NSString *message = [userInfo valueForKey:@"ci"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"LeadNow"
                                                                    message:message
                                                                   delegate:self
                                                          cancelButtonTitle:cancelTitle
                                                          otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                //Do stuff that you would do if the application was not active
    }
       
    //[PFPush handlePush:userInfo];
        //application.applicationIconBadgeNumber=application.applicationIconBadgeNumber+1;
  //       [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    }
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString   *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler{
    //handle the actions
        UIApplicationState state = [application applicationState];
        if (state == UIApplicationStateActive) {
            NSString *cancelTitle = @"Close";
            // NSString *showTitle = [userInfo valueForKey:@"title"];
            NSString *message = [userInfo valueForKey:@"ci"];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"LeadNow"
                                                                message:message
                                                               delegate:self
                                                      cancelButtonTitle:cancelTitle
                                                      otherButtonTitles:nil, nil];
            [alertView show];
            
      //      [UIApplication sharedApplication].applicationIconBadgeNumber = (unsigned long)strBadgeNo;
            
        } else {
            //Do stuff that you would do if the application was not active
        }
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))handler
{
    
    NSLog(@"didReceiveNotification");
  // [self configureLocalNotification:@"Request for silent push"];
    // Get application state for iOS4.x+ devices, otherwise assume active
    UIApplicationState appState = UIApplicationStateActive;
    if ([application respondsToSelector:@selector(applicationState)]) {
        appState = application.applicationState;
    }
    
    
    if (appState == UIApplicationStateInactive){
        
        
        //  [locationManager startUpdatingLocation];
        
    }
    //    else if (appState == UIApplicationStateActive)
    //    {
    //            [locationManager startUpdatingLocation];
    //    }
    
    
    // [self configureLocalNotification];
    
    
    
    handler(UIBackgroundFetchResultNewData);
    
}
-(void)configureLocalNotification: (NSString *)msg
{
    NSString *strMsg= [NSString stringWithFormat:@"%@",msg];
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
    localNotification.alertBody = strMsg;
    // localNotification.s
    localNotification.timeZone = [NSTimeZone localTimeZone];
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

-(void)getEmployeeByroll
{
    
    NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
    
   // [def setObject:[arr objectAtIndex:1] forKey:@"url"];

    strU=[def stringForKey:@"url"];
    
  [self deleteDatabase];
   [self deleteDatabase1];
    
   // NSString *si=[strU stringByAppendingString:@"key=getMaxCount"];
   // NSURLRequest *requesting = [NSURLRequest requestWithURL:[NSURL URLWithString:si]];
    
   // [[NSURLConnection alloc] initWithRequest:requesting delegate:self];

 //[self getService];
    
    [DejalKeyboardActivityView removeViewAnimated:YES];

}


-(void)getService
{
        
        NSUserDefaults *def=[NSUserDefaults standardUserDefaults];

        strU=[def stringForKey:@"name"];

        NSURL *url1 = [NSURL URLWithString:[NSString stringWithFormat:@"%@key=getServices&Uname=%@",KLOGINTOAPP,strU]];
        cMgr.serviceName=@"getServices";
        
        // NSLog(@"URL:%@",url1);
        NSXMLParser *xmlParser = [[NSXMLParser alloc]initWithContentsOfURL:url1];
        //    //Initialize the delegate.
        XMLParser   *parser = [[XMLParser alloc] init];
        //
        //    //Set delegate
        [xmlParser setDelegate:parser];
        
        BOOL success = [xmlParser parse];
        if (success) {
           // NSLog(@"SUCCESS:%d",[personalGoalarray count]);
            [DejalKeyboardActivityView removeViewAnimated:YES];
            if ([personalGoalarray count]>0) {
                //NSLog(@"%@",[[personalGoalarray objectAtIndex:0]goalName]);
            }
            //[[BusyAgent defaultAgent]makeBusy:NO showBusyIndicator:NO];
            // NSLog(@"%@",[subCategoryArray objectAtIndex:1])
        }

    }

-(void)deleteDatabase
{
//   GetService *getService= (GetService*)[NSEntityDescription insertNewObjectForEntityForName:@"GetService" inManagedObjectContext:_managedObjectContext];
    NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
    [allCars setEntity:[NSEntityDescription entityForName:@"GetService" inManagedObjectContext:_managedObjectContext ]];
    [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * cars = [_managedObjectContext    executeFetchRequest:allCars error:&error];
    
    //error handling goes here
    for (NSManagedObject * car in cars) {
        [_managedObjectContext  deleteObject:car];
    }
    NSError *saveError = nil;
    [_managedObjectContext save:&saveError];
    
}
-(void)deleteDatabase1
{
    //   GetService *getService= (GetService*)[NSEntityDescription insertNewObjectForEntityForName:@"GetService" inManagedObjectContext:_managedObjectContext];
    NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
    [allCars setEntity:[NSEntityDescription entityForName:@"Checkversion" inManagedObjectContext:_managedObjectContext ]];
    [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * cars = [_managedObjectContext    executeFetchRequest:allCars error:&error];
    
    //error handling goes here
    for (NSManagedObject * car in cars) {
        [_managedObjectContext  deleteObject:car];
    }
    NSError *saveError = nil;
    [_managedObjectContext save:&saveError];
    
}
- (void)applicationWillResignActive:(UIApplication *)application
{
  //  [self setBadge];
  //  [UIApplication sharedApplication].applicationIconBadgeNumber = valueBadge;
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:@"NO" forKey:@"isTerminated"];
    [prefs synchronize];

    [self checkLeadIfAvailable];

//    cMgr.isImagesending=NO;
//    cMgr.issendaudio=NO;
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
 //   [self setBadge];
 //   [UIApplication sharedApplication].applicationIconBadgeNumber = valueBadge;
  //  [[NSNotificationCenter defaultCenter] postNotificationName:@"pushNotification0" object:nil userInfo:nil];
    [self checkLeadIfAvailable];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:@"YES" forKey:@"isTerminated"];
    [prefs synchronize];
    [self checkLeadIfAvailable];

    // Saves changes in the application's managed object context before the application terminates.
  //  [self setBadge];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [self saveContext];
//    [self deleteCustomerTable];
//    [self deleteServiceDetailTable];
//    [self deleteImagePathTable];
//    [self deleteAudioPathTable];
//    objClass.LeadCountOutBox=nil;
}

//===================================================================
// Delete Database methods
//===================================================================

-(void)deleteCustomerTable
{
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [_managedObjectContext executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [_managedObjectContext deleteObject:data];
    }
    NSError *saveError = nil;
    [_managedObjectContext save:&saveError];
}

-(void)deleteServiceDetailTable
{
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [_managedObjectContext executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [_managedObjectContext deleteObject:data];
    }
    NSError *saveError = nil;
    [_managedObjectContext save:&saveError];
}

-(void)deleteImagePathTable
{
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"Imgpath" inManagedObjectContext:_managedObjectContext]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [_managedObjectContext executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [_managedObjectContext deleteObject:data];
    }
    NSError *saveError = nil;
    [_managedObjectContext save:&saveError];
}

-(void)deleteAudioPathTable
{
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"Audiopath" inManagedObjectContext:_managedObjectContext]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [_managedObjectContext executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [_managedObjectContext deleteObject:data];
    }
    NSError *saveError = nil;
    [_managedObjectContext save:&saveError];
}


-(void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
           // NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack
- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "Infocrats.TrackUserLocationDemo" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

//- (NSManagedObjectModel *)managedObjectModel {
//    if (_managedObjectModel != nil) {
//        return _managedObjectModel;
//    }
//    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil] ;
//    return _managedObjectModel;
//}
- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Green_Lead" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationStoresDirectory] URLByAppendingPathComponent:@"Green_Lead.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error]) {
        NSFileManager *fm = [NSFileManager defaultManager];
        
        // Move Incompatible Store
        if ([fm fileExistsAtPath:[storeURL path]]) {
            NSURL *corruptURL = [[self applicationIncompatibleStoresDirectory] URLByAppendingPathComponent:[self nameForIncompatibleStore]];
            
            // Move Corrupt Store
            NSError *errorMoveStore = nil;
            [fm moveItemAtURL:storeURL toURL:corruptURL error:&errorMoveStore];
            
            if (errorMoveStore) {
                NSLog(@"Unable to move corrupt store.");
            }
        }
    }
    //    NSError *errorAddingStore = nil;
    //    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&errorAddingStore]) {
    //        NSLog(@"Unable to create persistent store after recovery. %@, %@", errorAddingStore, errorAddingStore.localizedDescription);
    //    }
    
    //    NSString *title = @"Warning";
    //    NSString *applicationName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    //    NSString *message = [NSString stringWithFormat:@"A serious application error occurred while %@ tried to read your data. Please contact support for help.", applicationName];
    //    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //    [alertView show];
    
    return _persistentStoreCoordinator;
}
- (NSURL *)applicationStoresDirectory {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSURL *applicationApplicationSupportDirectory = [[fm URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *URL = [applicationApplicationSupportDirectory URLByAppendingPathComponent:@"Stores"];
    
    if (![fm fileExistsAtPath:[URL path]]) {
        NSError *error = nil;
        [fm createDirectoryAtURL:URL withIntermediateDirectories:YES attributes:nil error:&error];
        
        if (error) {
            NSLog(@"Unable to create directory for data stores.");
            
            return nil;
        }
    }
    
    return URL;
}

- (NSURL *)applicationIncompatibleStoresDirectory {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSURL *URL = [[self applicationStoresDirectory] URLByAppendingPathComponent:@"Incompatible"];
    
    if (![fm fileExistsAtPath:[URL path]]) {
        NSError *error = nil;
        [fm createDirectoryAtURL:URL withIntermediateDirectories:YES attributes:nil error:&error];
        
        if (error) {
            NSLog(@"Unable to create directory for corrupt data stores.");
            
            return nil;
        }
    }
    
    return URL;
}
- (NSString *)nameForIncompatibleStore {
    // Initialize Date Formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Configure Date Formatter
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:@"yyyy-MM-dd-HH-mm-ss"];
    
    return [NSString stringWithFormat:@"%@.sqlite", [dateFormatter stringFromDate:[NSDate date]]];
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Lead Sync(Deepak.s)
-(void)checkLeadIfAvailable {
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *isLeadsInOutbox = [prefs stringForKey:@"isLeadsInOutbox"];
    if ([isLeadsInOutbox isEqual:@"YES"]) {
        Reachability *reachableForWiFy=[Reachability reachabilityForInternetConnection];
        NetworkStatus netStatusWify = [reachableForWiFy currentReachabilityStatus];
        if (netStatusWify== NotReachable)
        {
            //Internet not working
            [self createTheNotification];
        }else {
            
        }
    }else {
        NSLog(@"No Leads are there to sync");
    }
    
    
}

-(void)syncAllLeadIfAny {
    //*****Sync Lead if Any*****//
    AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _managedObjectContext= [appdelegate managedObjectContext];
    
    NSEntityDescription *entityDesc13=[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext];
    
    NSFetchRequest *request25=[[NSFetchRequest alloc
                               ]init];
    [request25 setEntity:entityDesc13];
    
    NSPredicate    *pred11 = [NSPredicate predicateWithFormat:@"(username= %@) && (statuso= %@)",cMgr.strUsreName, @"true"];
    [request25 setPredicate:pred11];
    
    NSError *error=nil;
    NSArray  *objects123 = [_managedObjectContext executeFetchRequest:request25  error:&error];
    
    
    NSMutableArray *objectsMutablesw=[NSMutableArray arrayWithArray:objects123];
    
    if ([objectsMutablesw count]==0) {
        cMgr.issSending=YES;
    }else {
        cMgr.issSending=NO;
    }
    
    if (!cMgr.issSending) {
        Reachability *reachableForWiFy=[Reachability reachabilityForInternetConnection];
        NetworkStatus netStatusWify = [reachableForWiFy currentReachabilityStatus];
        if (netStatusWify== NotReachable)
        {
            //Internet not working
            [self createTheNotification];
        }else {
            [self postdatattoserver];
            cMgr.issSending=YES;
        }
        
    }
    
    
    //*****Sync Image Path if Any*****
    NSEntityDescription  *entityDesc4=[NSEntityDescription entityForName:@"Imgpath" inManagedObjectContext:_managedObjectContext];
    
    NSFetchRequest *request2=[[NSFetchRequest alloc
                              ]init];
    [request2 setEntity:entityDesc4];
    
    NSArray *objects2 = [_managedObjectContext executeFetchRequest:request2 error:&error];
    NSMutableArray  *objectsMutables1=[NSMutableArray arrayWithArray:objects2];
    if ([objectsMutables1 count]==0) {
        
    }
    else
    {
        if (!cMgr.isImagesending) {
            Reachability *reachableForWiFy=[Reachability reachabilityForInternetConnection];
            NetworkStatus netStatusWify = [reachableForWiFy currentReachabilityStatus];
            if (netStatusWify== NotReachable)
            {
                //Internet not working
            }else {
                [self sendimageToserver];
            }
        }else{
        }
    }
    
    //*****Sync Audio if Any*****
    NSEntityDescription   *entityDesc2=[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext];
    
    NSFetchRequest *request=[[NSFetchRequest alloc
                             ]init];
    [request setEntity:entityDesc2];
    
    NSPredicate    *pred111 = [NSPredicate predicateWithFormat:@"(username= %@)",cMgr.strUsreName];
    [request setPredicate:pred111];
    
    NSArray    *objects12 = [_managedObjectContext executeFetchRequest:request error:&error];
    NSMutableArray *arrayForsendinga=[NSMutableArray arrayWithArray:objects12];
    
    
    for (int i=0; i<[arrayForsendinga count]; i++)
    {
        Audiopath *audioPathData= (Audiopath*)[NSEntityDescription insertNewObjectForEntityForName:@"Audiopath" inManagedObjectContext:_managedObjectContext];
        
        audioPathData.username=cMgr.strUsreName;
        NSError *savingError = nil;
        if ([self.managedObjectContext save:&savingError]){
            // NSLog(@"Successfully saved the context.");
        }
    }

}

-(void)postdatattoserver
{
    AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _managedObjectContext= [appdelegate managedObjectContext];
    
    NSEntityDescription    *entityDesc1=[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext];
    
    NSFetchRequest  *request122=[[NSFetchRequest alloc
                                 ]init];
    
    [request122 setEntity:entityDesc1];
    
    NSError *error;
    
    NSPredicate  *pred = [NSPredicate predicateWithFormat:@"(statuso= %@)",@"true"];
    [request122 setPredicate:pred];
    NSArray    *objects12 = [_managedObjectContext executeFetchRequest:request122 error:&error];
    NSMutableArray  *objectsMutables=[NSMutableArray arrayWithArray:objects12];
    
    
    
    if ([objectsMutables count]==0) {
        cMgr.issSending=NO;
    }
    
    for (int i=0 ;i<[objectsMutables count]; i++) {
        NSManagedObject *objCustomer = [[NSManagedObject alloc] init];
        objCustomer = [objectsMutables objectAtIndex:i];

        NSString  *strAutoId = [objCustomer valueForKey:@"autoId"];
        NSString  *strDateTimec = [objCustomer valueForKey:@"dateTimec"];
        NSString  *strStatusCustomer = [objCustomer valueForKey:@"statusCustomer"];

        NSString *post;
        NSString *strK=[[NSUserDefaults standardUserDefaults]valueForKey:@"key"];
        
        post = [NSString stringWithFormat:@"&AutoID=%@&SendDateTime=%@&uname=%@&comKey=%@&key=insertMaster",strAutoId, strDateTimec,cMgr.cacheName,strK];
        NSString  *post1;
        if ([strStatusCustomer isEqualToString:@"new"])
            
        {
            /*NSEntityDescription   *entityDesc2=[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext];
            
            NSFetchRequest *request=[[NSFetchRequest alloc
                     ]init];
            [request setEntity:entityDesc2];
            NSError *error;
            
            pred = [NSPredicate predicateWithFormat:@"(autoId = %@)",strAutoId];
            [request setPredicate:pred];
            NSArray  *objects2 = [_managedObjectContext executeFetchRequest:request error:&error];
            NSLog(@"%@", [objects2 valueForKey:@"fName"]);

            if ([objects2 count]>0) {
                post1=[NSString stringWithFormat:@"&FName=%@&LName=%@&Phno=%@&Email=%@&status=new",[[objects2 objectAtIndex:0]fName],[[objects2 objectAtIndex:0]lName],[[objects2 objectAtIndex:0]phNo],[[objects2 objectAtIndex:0]emailId]];
                post=[post stringByAppendingString:post1];
            }*/
            NSString  *strFName = [objCustomer valueForKey:@"fName"];
            NSString  *strLName = [objCustomer valueForKey:@"lName"];
            NSString  *strPhNo = [objCustomer valueForKey:@"phNo"];
            NSString  *strEmailId = [objCustomer valueForKey:@"emailId"];

            post1=[NSString stringWithFormat:@"&FName=%@&LName=%@&Phno=%@&Email=%@&status=new",strFName, strLName, strPhNo, strEmailId];
            post=[post stringByAppendingString:post1];
            
        }else {
            NSString  *strAccountNo = [objCustomer valueForKey:@"accountNo"];
            
            post1=[NSString stringWithFormat:@"&AccountNo=%@&status=old",strAccountNo];
            post=[post stringByAppendingString:post1];
        }
        
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        //    Create URLRequest object and initialize it.
        NSMutableURLRequest *request34 = [[NSMutableURLRequest alloc] init];
        
        NSUserDefaults *de=[NSUserDefaults standardUserDefaults];
        strU=[de stringForKey:@"url"];
        strU =[strU stringByAppendingString:@"?"];
        [request34 setURL:[NSURL URLWithString:strU]];
        
        //    Now, set HTTP method (POST or GET).
        [request34 setHTTPMethod:@"POST"];
        [request34 setHTTPBody:postData];
        
        NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request34 delegate:self];
        
        if (conn) {
            
            /*NSFetchRequest * allCars3 = [[NSFetchRequest alloc] init];
            [allCars3 setEntity:[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext ]];
            [allCars3 setIncludesPropertyValues:NO]; //only fetch the managedObjectID
            NSPredicate *p1=[NSPredicate predicateWithFormat:@"autoId=%@", strAutoId];
            [allCars3 setPredicate:p1];
            NSError * error1 = nil;
            NSArray * cars1 = [_managedObjectContext    executeFetchRequest:allCars3 error:&error1];
            
            NSManagedObject *match=nil;
            if([cars1 count]==0)
            {
                
            }else {
                for (int i=0; i<[cars1 count] ; i++)
                {
                    match=[cars1 objectAtIndex:i];
                    [match setValue:@"send" forKey:@"statuso"];
                    [_managedObjectContext save:&error];
                }
            }*/
            [objCustomer setValue:@"send" forKey:@"statuso"];
            [_managedObjectContext save:&error];
            
        }else {
            
        }
    }
    
    
    NSEntityDescription *entityDesc13=[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext];
    
    NSFetchRequest *request25=[[NSFetchRequest alloc
                               ]init];
    
    [request25 setEntity:entityDesc13];
    
    NSPredicate    *pred111 = [NSPredicate predicateWithFormat:@"(username= %@)",cMgr.strUsreName];
    [request25 setPredicate:pred111];
    
    NSArray  *objects123 = [_managedObjectContext executeFetchRequest:request25  error:&error];
    
    NSMutableArray *objectsMutablesw=[NSMutableArray arrayWithArray:objects123];
    for (int i = 0; i<[objectsMutablesw count]; i++)
        
    {
        NSManagedObject *objServiceDetail = [[NSManagedObject alloc] init];
        objServiceDetail = [objectsMutablesw objectAtIndex:i];

        
        NSString *imagNamelocal = [objServiceDetail valueForKey:@"imgName"];
        NSString *localPath = [objServiceDetail valueForKey:@"savedImagePath"];
        NSString *audioNamelocal = [objServiceDetail valueForKey:@"audioName"];
        NSString *localaudioPath = [objServiceDetail valueForKey:@"audioPath"];
        
        NSString *strAutoId = [objServiceDetail valueForKey:@"autoId"];
        NSString *strServiceId = [objServiceDetail valueForKey:@"serviceId"];
        NSString *strServiceName = [objServiceDetail valueForKey:@"serviceName"];
        NSString *strComment = [objServiceDetail valueForKey:@"comment"];
        NSString *strUrgencyLevel = [objServiceDetail valueForKey:@"urgencyLevel"];
        NSString *strAccountNo = [objServiceDetail valueForKey:@"accountNo"];

        
        if ([imagNamelocal isEqualToString:@""]) {
            imagNamelocal=@"";
        }
        
        else if ([imagNamelocal isEqualToString:@"null"]) {
            imagNamelocal=@"";
        }
        
        else if (imagNamelocal==nil) {
            imagNamelocal=@"";
        }
       
        
        if ([audioNamelocal isEqualToString:@""]) {
            audioNamelocal=@"";
        }
        
        else if ([audioNamelocal isEqualToString:@"null"]) {
            audioNamelocal=@"";
        }
        
        else if (audioNamelocal==nil) {
            audioNamelocal=@"";
        }

        NSString *postn;
        
        postn = [NSString stringWithFormat:@"&autoid=%@&serviceid=%@&servicename=%@&images=%@&audio=%@&comment=%@&urgency=%@&acno=%@&key=detailinsert", strAutoId, strServiceId, strServiceName,  imagNamelocal, audioNamelocal, strComment, strUrgencyLevel, strAccountNo];
        
        NSData *postData1 = [postn dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *request2 = [[NSMutableURLRequest alloc] init];
        [request2 setURL:[NSURL URLWithString:strU]];
        [request2 setHTTPMethod:@"POST"];
        [request2 setHTTPBody:postData1];
        
        NSURLConnection *conn1 = [[NSURLConnection alloc]initWithRequest:request2 delegate:self];
        
        if(conn1)
        {
            
            /*NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
            [allCars setEntity:[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext ]];
            [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
            NSPredicate *p=[NSPredicate predicateWithFormat:@"iD=%@", [[objectsMutablesw objectAtIndex:i]iD],cMgr.strUsreName];
            [allCars setPredicate:p];
            NSError * error = nil;
            NSArray * cars = [_managedObjectContext    executeFetchRequest:allCars error:&error];
            
            NSManagedObject *match=nil;
            
            if([cars count]==0)
            {
                
                
            }
            else
            {
                for (int i=0; i<[cars count] ; i++)
                {
                    match=[cars objectAtIndex:i];
                    [match setValue:@"send" forKey:@"statuso"];
                    [_managedObjectContext save:&error];
                }
            }*/
            [objServiceDetail setValue:@"send" forKey:@"statuso"];
            [_managedObjectContext save:&error];
        }else{
        }
        cMgr.issSending=NO;
    }
    cMgr.issSending=NO;
}

-(void)sendimageToserver
{
    if (!cMgr.isImagesending)
    {
        cMgr.isImagesending=YES;
        NSEntityDescription  *entityDesc4=[NSEntityDescription entityForName:@"Imgpath" inManagedObjectContext:_managedObjectContext];
        
        NSFetchRequest *request2=[[NSFetchRequest alloc
                                  ]init];
        
        [request2 setEntity:entityDesc4];
        
        NSError *error=nil;
        NSArray    *objects2 = [_managedObjectContext executeFetchRequest:request2 error:&error];
        NSMutableArray  *objectsMutables1=[NSMutableArray arrayWithArray:objects2];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            for (int i=0; i<[objectsMutables1 count]; i++)
            {
                
                NSManagedObject *objImgpath = [[NSManagedObject alloc] init];
                objImgpath = [objectsMutables1 objectAtIndex:i];

                
                NSString *strPathImg = [objImgpath valueForKey:@"pathImg"];
                NSString *removalId = [objImgpath valueForKey:@"imgNameimg"];

                
                
                UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,1.0,1)];
                UIImage *_img = [UIImage imageWithContentsOfFile:strPathImg];
                imgView.image = _img;
                
                NSData *imageData;
                imageData =  UIImageJPEGRepresentation(imgView.image, 0.00000000000000000000000000001f);
                
                //      // setting up the URL to post to
                //http://myanteater.com/ImageUploader.ashx
                NSUserDefaults *de=[NSUserDefaults standardUserDefaults];
                strU=[de stringForKey:@"url"];
                NSString *st=[strU stringByReplacingOccurrencesOfString:@"ServiceHandler.ashx" withString:@"ImageUploader.ashx"];
                NSString *urlString = [NSString stringWithFormat:@"%@",st];
                
                
                NSMutableURLRequest *request13 = [[NSMutableURLRequest alloc] init];
                [request13 setURL:[NSURL URLWithString:urlString]];
                [request13 setHTTPMethod:@"POST"];
                
                NSString *boundary = @"---------------------------14737809831466499882746641449";
                NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
                [request13 addValue:contentType forHTTPHeaderField: @"Content-Type"];
                
                NSMutableData *body = [NSMutableData data];
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                NSString *combine = removalId;
                
                
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@.png\"\r\n",combine] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[NSData dataWithData:imageData]];
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                [request13 setHTTPBody:body];
                
                NSData *returnData = [NSURLConnection sendSynchronousRequest:request13 returningResponse:nil error:nil];
                
                NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
                                
                if ([returnString isEqualToString:@"OK"]) {
                    NSString *strPathImg1 = [objImgpath valueForKey:@"pathImg1"];

                    NSData *imgd4;
                    UIImage *img1 = [UIImage imageWithContentsOfFile:strPathImg1];

                    UIImageView *imgView1 =  [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,1.0,1.0)];
                    imgView1.image = img1;
                    imgd4 =  UIImageJPEGRepresentation(imgView1.image, 0.00000000000000000000000000001f);
                    
                    NSString *st=[strU stringByReplacingOccurrencesOfString:@"ServiceHandler.ashx" withString:@"ImageUploader.ashx"];
                    NSString *urlString = [NSString stringWithFormat:@"%@",st];
                    
                    NSMutableURLRequest *request14 = [[NSMutableURLRequest alloc] init];
                    [request14 setURL:[NSURL URLWithString:urlString]];
                    [request14 setHTTPMethod:@"POST"];
                    
                    NSString *boundary = @"---------------------------14737809831466499882746641449";
                    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
                    [request14 addValue:contentType forHTTPHeaderField: @"Content-Type"];
                    
                    NSMutableData *body = [NSMutableData data];
                    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    
                    NSString *combine = removalId;
                    
                    
                    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@.png\"\r\n",combine] dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:[NSData dataWithData:imgd4]];
                    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    [request14 setHTTPBody:body];
                    
                    NSData *returnData = [NSURLConnection sendSynchronousRequest:request14 returningResponse:nil error:nil];
                    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
                    
                    NSFetchRequest * allCars3 = [[NSFetchRequest alloc] init];
                    [allCars3 setEntity:[NSEntityDescription entityForName:@"Imgpath" inManagedObjectContext:_managedObjectContext ]];
                    [allCars3 setIncludesPropertyValues:NO]; //only fetch the managedObjectID
                    NSPredicate *p1=[NSPredicate predicateWithFormat:@"imgNameimg =%@", removalId];
                    [allCars3 setPredicate:p1];
                    NSError * error1 = nil;
                    NSArray * cars1 = [_managedObjectContext    executeFetchRequest:allCars3 error:&error1];

                    for (NSManagedObject * car in cars1) {
                        
                        [_managedObjectContext  deleteObject:car];
                        NSError *saveError = nil;
                        [_managedObjectContext save:&saveError];
                    }
                    cMgr.isImagesending=NO;
                }
            }
        });
    }
}

-(void)createTheNotification {
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    
    UNMutableNotificationContent *content = [UNMutableNotificationContent new];
    content.title = @"Some leads are there in your outbox.";
    content.body = @"Don't forget to sync on server.";
    content.sound = [UNNotificationSound defaultSound];
    
    UNTimeIntervalNotificationTrigger *trigger =     [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:60
                                                                                                        repeats:YES];
    
    
    NSString *identifier = @"UYLLocalNotification";
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:identifier
                                                                          content:content trigger:trigger];

    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Something went wrong: %@",error);
        }
    }];
    
    
}

-(void) stopNotification {
    [[UNUserNotificationCenter currentNotificationCenter] removeAllPendingNotificationRequests];
    [[UNUserNotificationCenter currentNotificationCenter] removeAllDeliveredNotifications];
}


-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    [self stopNotification];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GoToOutBox" object:nil];
}
/*
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       didReceiveNotificationResponse:(UNNotificationResponse *)response
       withCompletionHandler:(void (^)(void))completionHandler {

    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Notification Received" message:@"UNNotificationResponse" delegate:nil     cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
 }*/

#pragma mark-Nsuser

#pragma mark-Nsuser defaults
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
   
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
     cMgr=[CacheManager getInstance];
    NSString *charlieSendString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //
    cMgr.versionCache=charlieSendString ;
    chk= (Checkversion*)[NSEntityDescription insertNewObjectForEntityForName:@"Checkversion" inManagedObjectContext:_managedObjectContext];
    chk.sversion=charlieSendString;
//    NSError *savingError = nil;
//    if ([self.managedObjectContext save:&savingError])
//    {
//       // NSLog(@"Successfully saved the context.");
//    } else {
//       // NSLog(@"Failed to save the context. Error = %@", savingError);
//    }
//   // NSLog(@"%@",charlieSendString);
    
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
}
-(void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken
{
    NSString *deviceToken = [[devToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
   // NSLog(@"content token---%@",deviceToken);
    
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"DeviceToken" message:deviceToken delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//    [alert show];
    
    NSUserDefaults *device=[NSUserDefaults standardUserDefaults];
    [device setObject:deviceToken forKey:@"device"];
    [device synchronize];
    uuidCustom = nil;
     standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if (![standardUserDefaults stringForKey:@"uuidCustom"]) {
        
        CFUUIDRef theUUID = CFUUIDCreate(kCFAllocatorDefault);
        if (theUUID) {
            uuidCustom = CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, theUUID));
            CFRelease(theUUID);
          //  NSLog(@"%@.....uuid", uuidCustom);
            [standardUserDefaults setObject:uuidCustom forKey:@" uuidCustom"];
            [standardUserDefaults synchronize];
        }
    }
    else
    {
        uuidCustom = [standardUserDefaults stringForKey:@"uuidCustom"];
       // NSLog(@"%@...uuid defaultes", uuidCustom);
    }
   
   // [self performSelectorInBackground:@selector(loadadd) withObject:nil];
}

- (void)addMessageFromRemoteNotification:(NSDictionary*)userInfo updateUI:(BOOL)updateUI
{
 //   NSLog(@"msg will %@",userInfo);
    NSString *cancelTitle = @"Close";
   
    NSString *message = [userInfo valueForKey:@"ci"];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"LeadNow"
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:cancelTitle
                                              otherButtonTitles:nil, nil];
    [alertView show];
}

//- (BOOL)migrateStore:(NSURL *)storeURL toVersionTwoStore:(NSURL *)dstStoreURL
//               error:(NSError **)outError {
//    // Try to get an inferred mapping model.
//    NSMappingModel *mappingModel =
//    [NSMappingModel inferredMappingModelForSourceModel:[self managedObjectModel]
//                                      destinationModel:[self managedObjectModelDes] error:outError];
//    // If Core Data cannot create an inferred mapping model, return NO.
//    if (!mappingModel) {
//        return NO;
//    }
//    // Get the migration manager class to perform the migration.
//    NSValue *classValue =
//    [[NSPersistentStoreCoordinator registeredStoreTypes]
//     objectForKey:NSSQLiteStoreType];
//    Class sqliteStoreClass = (Class)[classValue pointerValue];
//    Class sqliteStoreMigrationManagerClass = [sqliteStoreClass
//                                              migrationManagerClass];
//    NSMigrationManager *manager = [[sqliteStoreMigrationManagerClass alloc]
//                                   initWithSourceModel:[self managedObjectModel] destinationModel:[self
//                                                                                            managedObjectModelDes]];
//    BOOL success = [manager migrateStoreFromURL:storeURL type:NSSQLiteStoreType
//                                        options:nil withMappingModel:mappingModel toDestinationURL:dstStoreURL
//                                destinationType:NSSQLiteStoreType destinationOptions:nil error:outError];
//    return success;
//}
-(void)setBadge11{
    AppDelegate *appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _managedObjectContext= [appDelegate1 managedObjectContext];
    
    NSEntityDescription    *entityDesc111=[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext];
    
    NSFetchRequest  *request1211=[[NSFetchRequest alloc
                                   ]init];
    [request1211 setEntity:entityDesc111];
    
//    NSPredicate    *pred1 = [NSPredicate predicateWithFormat:@"(username= %@)",cMgr.strUsreName];
//    NSLog(@"user NAme ==%@",cMgr.strUsreName);
    
//    [request1211 setPredicate:pred1];

    NSError *error;
    
    NSArray    *objects121 = [_managedObjectContext executeFetchRequest:request1211 error:&error];
    objectsMutable = [NSMutableArray  arrayWithArray:objects121];
    
    strBadgeNo = [NSString stringWithFormat:@"%lu",(unsigned long)[objectsMutable count]];
    valueBadge = [strBadgeNo intValue];
}
-(void)setBadge{
    AppDelegate *appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _managedObjectContext= [appDelegate1 managedObjectContext];
    
    NSEntityDescription    *entityDesc111=[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext];
    
    NSFetchRequest  *request1211=[[NSFetchRequest alloc
                                   ]init];
    [request1211 setEntity:entityDesc111];
    
        NSPredicate    *pred1 = [NSPredicate predicateWithFormat:@"(username= %@)",cMgr.strUsreName];
        NSLog(@"user NAme ==%@",cMgr.strUsreName);
    
        [request1211 setPredicate:pred1];
    
    NSError *error;
    
    NSArray    *objects121 = [_managedObjectContext executeFetchRequest:request1211 error:&error];
    objectsMutable = [NSMutableArray  arrayWithArray:objects121];
    
    strBadgeNo = [NSString stringWithFormat:@"%lu",(unsigned long)[objectsMutable count]];
    valueBadge = [strBadgeNo intValue];
}

@end
