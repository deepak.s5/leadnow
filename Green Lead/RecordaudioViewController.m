//
//  RecordaudioViewController.m
//  Lead Now
//
//  Created by Rakesh Jain on 05/10/13.
//  Copyright (c) 2013 Infocrats. All rights reserved.
//

#import "RecordaudioViewController.h"

@interface RecordaudioViewController ()

@end

@implementation RecordaudioViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
