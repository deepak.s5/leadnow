
//
//  MainViewController.m
//  Green Lead
//
//  Created by Rakesh Jain on 17/09/13.
//  Copyright (c) 2013 Infocrats. All rights reserved.
//

#import "MainViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "XMLParser.h"
#import "DeveloperViewController.h"
#import "TabbbarViewController.h"
#import "Reachability.h"
#import "NewcustomerViewController.h"
#import <SafariServices/SafariServices.h>
#import "SplashViewController.h"

#define IS_IPHONE ( [[[UIDevice currentDevice] model] isEqualToString:@"iPhone"] )
#define IS_HEIGHT_GTE_568 [[UIScreen mainScreen ] bounds].size.height >= 568.0f
#define IS_IPHONE_5 ( IS_IPHONE && IS_HEIGHT_GTE_568 )
@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad
{
    
    Reachability *reachableForWiFy=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify = [reachableForWiFy currentReachabilityStatus];
    NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
    
    txtCompanykey.text=[def valueForKey:@"key"];
    [activity setHidden:YES];
    [def synchronize];
    
    self.navigationController.navigationBarHidden=TRUE;
    cMgr=[CacheManager getInstance];
    CALayer *imageLayer = txtviewComment.layer;
    [imageLayer setCornerRadius:1];
    [imageLayer setBorderWidth:1];
    imageLayer.borderColor=[[UIColor lightGrayColor] CGColor];
    
    NSUserDefaults *df=[NSUserDefaults standardUserDefaults];
    
    NSString *myInt = [df objectForKey:@"Remember"];
    
    cMgr.cacheName =[df objectForKey:@"name"];
    
    [df synchronize];
    
    if ([myInt isEqualToString:@"YES"]) {
        [activity setHidden:NO];
        [activity startAnimating];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"checkversionData" object:nil];
        
        [self checkifRemembered];
        /*
         if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
         {
         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
         TabbbarViewController* verificationBadgesViewController = (TabbbarViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Developer"];
         [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
         }
         else
         {
         //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
         
         UIStoryboard *storyboard;
         if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
         {
         storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
         }
         else
         {
         storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
         }
         TabbbarViewController* verificationBadgesViewController = (TabbbarViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Developer"];
         [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
         }
         */
    }
    
    
    [super viewDidLoad];
    // [self getService];
    // Do any additional setup after loading the view, typically from a nib.
    
    
}

-(void)checkifRemembered
{
    Reachability *reachableForWiFy=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify = [reachableForWiFy currentReachabilityStatus];
    if (netStatusWify== NotReachable)
    {
        NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
        BOOL isAD_Existing_Customer =[defsss boolForKey:@"AD_Existing_Customer"];
        BOOL isAD_New_Customer =[defsss boolForKey:@"AD_New_Customer"];
        BOOL isAD_Both =[defsss boolForKey:@"AD_Both"];
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            cMgr.strUsreName=[def objectForKey:@"name"];
            
            if (isAD_Both) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
                TabbbarViewController* verificationBadgesViewController = (TabbbarViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Developer"];
                [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
            } else if (isAD_Existing_Customer){
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
                TabbbarViewController* verificationBadgesViewController = (TabbbarViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Developer"];
                [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
            }else if (isAD_New_Customer){
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
                TabbbarViewController* verificationBadgesViewController = (TabbbarViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Developer"];
                [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
            }
            
        }
        else
        {
            //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            cMgr.strUsreName=[def objectForKey:@"name"];
            UIStoryboard *storyboard;
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
            }
            else
            {
                storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
            }
            if (isAD_Both) {
                TabbbarViewController* verificationBadgesViewController = (TabbbarViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Developer"];
                [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
            } else if (isAD_Existing_Customer){
                TabbbarViewController* verificationBadgesViewController = (TabbbarViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Developer"];
                [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
            }else if (isAD_New_Customer){
                TabbbarViewController* verificationBadgesViewController = (TabbbarViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Developer"];
                [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
            }
        }
    }
    else{
        //key=login&Uname=&Pwd=
        // [DejalBezelActivityView activityViewForView:self.view];
        [activity setHidden:NO];
        [activity startAnimating];
        //  cMgr.cacheName=txtUsername.text;
        
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        //  [def setObject:txtUsername.text forKey:@"name"];
        // [def setObject:txtPwd.text forKey:@"pwd"];
        
        
        // [def setObject:txtCompanykey.text forKey:@"key"];
        // [def synchronize];
        
        
        
        NSString *post = [NSString stringWithFormat:@"key=loginnew&Uname=%@&Pwd=%@&cKey=%@",[[def objectForKey:@"name"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[[def objectForKey:@"pwd"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[[def objectForKey:@"key"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        cMgr.strUsreName=[def objectForKey:@"name"];
        NSLog(@"User NAme If REMENBERD %@",[def objectForKey:@"name"]);
        NSLog(@"CMGR USERNAME %@",cMgr.strUsreName);
        
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        //    You need to send the actual length of your data. Calculate the length of the post string.
        
        
        //    3. Create a Urlrequest with all the properties like HTTP method, http header field with length of the post string.
        //    Create URLRequest object and initialize it.
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        //    Set the Url for which your going to send the data to that request.
        
        [request setURL:[NSURL URLWithString:KLOGINTOAPP]];
        
        //    Now, set HTTP method (POST or GET).
        //    Write this lines as it is in your code.
        
        [request setHTTPMethod:@"POST"];
        
        //    Set HTTP header field with length of the post data.
        
        
        //    Also set the Encoded value for HTTP header Field.
        
        //  [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];
        
        //    Set the HTTPBody of the urlrequest with postData.
        
        [request setHTTPBody:postData];
        
        //    4. Now, create URLConnection object. Initialize it with the URLRequest.
        
        NSURLConnection *conn = [[NSURLConnection alloc]initWithRequest:request delegate:self];
        
        //    It returns the initialized url connection and begins to load the data for the url request. You can check that whether you URL connection is done properly or not using just if/else statement as below.
        if(conn)
        {
            //            NSLog(@"Connection Successful”);
        }
        else
        {
            //  NSLog(@"Connection could not be made”);
        }
        [self performSelectorInBackground:@selector(loadadd) withObject:nil];
    }
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Flipside View Controller

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.flipsidePopoverController dismissPopoverAnimated:YES];
        self.flipsidePopoverController = nil;
    }
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.flipsidePopoverController = nil;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showAlternate"]) {
        [[segue destinationViewController] setDelegate:self];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            UIPopoverController *popoverController = [(UIStoryboardPopoverSegue *)segue popoverController];
            self.flipsidePopoverController = popoverController;
            popoverController.delegate = self;
        }
    }
}

- (IBAction)togglePopover:(id)sender
{
    if (self.flipsidePopoverController) {
        [self.flipsidePopoverController dismissPopoverAnimated:YES];
        self.flipsidePopoverController = nil;
    } else {
        [self performSegueWithIdentifier:@"showAlternate" sender:sender];
    }
}

-(IBAction)login
{
    Reachability *reachableForWiFy=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify = [reachableForWiFy currentReachabilityStatus];
    if (netStatusWify== NotReachable)
    {
        //Internet n ot working
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cellular Data is Turned Off"
                                                        message:@"Turn on cellular data or use Wi-Fi to access data"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        [def setObject:@"No" forKey:@"Remember"];
        [def synchronize];
        
    }
    else{
        
        if (txtCompanykey.text.length==0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert..!!"
                                                            message:@"Please enter company key"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
            
        } else if (txtUsername.text.length==0){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert..!!"
                                                            message:@"Please enter user name"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
        } else if (txtPwd.text.length==0){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert..!!"
                                                            message:@"Please enter password"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            //key=login&Uname=&Pwd=
            // [DejalBezelActivityView activityViewForView:self.view];
            [activity setHidden:NO];
            [activity startAnimating];
            cMgr.cacheName=txtUsername.text;
            NSString *post = [NSString stringWithFormat:@"key=loginnew&Uname=%@&Pwd=%@&cKey=%@",[cMgr.cacheName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[txtPwd.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[txtCompanykey.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            
            NSLog(@" url for login  %@/%@",cMgr.cacheName,post);
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            [def setObject:txtUsername.text forKey:@"name"];
            [def setObject:txtPwd.text forKey:@"pwd"];
            
            
            [def setObject:txtCompanykey.text forKey:@"key"];
            [def synchronize];
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            
            //    You need to send the actual length of your data. Calculate the length of the post string.
            
            
            //    3. Create a Urlrequest with all the properties like HTTP method, http header field with length of the post string.
            //    Create URLRequest object and initialize it.
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            //    Set the Url for which your going to send the data to that request.
            
            [request setURL:[NSURL URLWithString:KLOGINTOAPP]];
            
            //    Now, set HTTP method (POST or GET).
            //    Write this lines as it is in your code.
            
            [request setHTTPMethod:@"POST"];
            
            //    Set HTTP header field with length of the post data.
            
            
            //    Also set the Encoded value for HTTP header Field.
            
            //  [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];
            
            //    Set the HTTPBody of the urlrequest with postData.
            
            [request setHTTPBody:postData];
            
            //    4. Now, create URLConnection object. Initialize it with the URLRequest.
            
            NSURLConnection *conn = [[NSURLConnection alloc]initWithRequest:request delegate:self];
            
            //    It returns the initialized url connection and begins to load the data for the url request. You can check that whether you URL connection is done properly or not using just if/else statement as below.
            if(conn)
            {
                //            NSLog(@"Connection Successful”);
            }
            else
            {
                //  NSLog(@"Connection could not be made”);
            }
            [self performSelectorInBackground:@selector(loadadd) withObject:nil];
        }
    }
}

- (IBAction)SignUp:(id)sender {
    // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://myleadnow.com/Signup.aspx"]];
    [self openLinkNew:@"http://myleadnow.com/Signup.aspx"];
}
- (void)openLinkNew:(NSString *)url {
    NSURL *URL = [NSURL URLWithString:url];
    
    if (URL) {
        if ([SFSafariViewController class] != nil) {
            SFSafariViewController *sfvc = [[SFSafariViewController alloc] initWithURL:URL];
            sfvc.delegate = self;
            [self presentViewController:sfvc animated:YES completion:nil];
        } else {
            if (![[UIApplication sharedApplication] openURL:url]) {
                NSLog(@"%@%@",@"Failed to open url:",[url description]);
            }
        }
    } else {
        // will have a nice alert displaying soon.
    }
}

-(void)loadadd
{
    AppDelegate  *appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *URLString = @"https://www.google.co.in/";
    if(URLString==NULL)
    {
        
    }
    else
    {
        NSUserDefaults *standardUserDefaults;
        
        CFUUIDRef theUUID = CFUUIDCreate(kCFAllocatorDefault);
        if (theUUID) {
            appD.uuidCustom = CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, theUUID));
            CFRelease(theUUID);
            // NSLog(@"%@.....uuid", appD.uuidCustom);
            [standardUserDefaults setObject:appD.uuidCustom forKey:@"uuidCustom"];
            [standardUserDefaults synchronize];
        }
        else
        {
            appD.uuidCustom = [standardUserDefaults stringForKey:@"uuidCustom"];
            // NSLog(@"%@...uuid defaultes", appD.uuidCustom);
        }
        NSUserDefaults *device=[NSUserDefaults standardUserDefaults];
        NSString *token=[device stringForKey:@"device"];
        
        //  NSString *urlString12 = [NSString stringWithFormat:@"http://myleadnow.com/ServiceHandler.ashx?key=deviceReg&regid=%@&imeiNo=%@&username=%@&companykey=%@",[appD.uuidCustom stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[token stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[txtUsername.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[txtCompanykey.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSString *urlString12 = [NSString stringWithFormat:@"%@?key=deviceReg&regid=%@&imeiNo=%@&username=%@&companykey=%@",KLOGINTOAPP,[appD.uuidCustom stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[token stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[txtUsername.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[txtCompanykey.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        
        //NSLog(@" data coming--- %@",urlString12);
        NSURL *url12 = [NSURL URLWithString:[urlString12 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *returnData1 = [NSData dataWithContentsOfURL:url12];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString12]];
        NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        if (conn) {
            // responseData=[[NSMutableData alloc]init];
            [conn start];
        }
        //NSLog(@"handler %@",cMgr.cacheName);
    }
}

-(IBAction)selectReason:(id)sender
{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setObject:cMgr.cacheName forKey:@"name"];
    [def setObject:txtPwd.text forKey:@"pwd"];
    
    [def synchronize];
    UIImage *img=[UIImage imageNamed:@"check.png"];
    NSUserDefaults *df=[NSUserDefaults standardUserDefaults];
    
    NSString *myInt = [df objectForKey:@"name"];
    
    if ([sender tag]==0) {
        
        if (isSelectNoBId)
        {
            
            img=[UIImage imageNamed:@"uncheck.png"];
            [btnCheck setBackgroundImage:img forState:UIControlStateNormal];
            
            isSelectNoBId=NO;
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            [def setObject:@"NO" forKey:@"Remember"];
            [def synchronize];
        }
        else
        {
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            [def setObject:@"YES" forKey:@"Remember"];
            [def synchronize];
            [btnCheck setBackgroundImage:img forState:UIControlStateNormal];
            isSelectNoBId=YES;
        }
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    
    NSError *error=nil;
    NSArray *classData =
    [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    NSDictionary *dict = [classData objectAtIndex:0];
    
    NSLog(@"load data---%lu",(unsigned long)[dict count]);
    if ([dict count]==1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert..!!" message:@"User name or password didn't match" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [activity stopAnimating];
        [activity setHidden:YES];
        NSUserDefaults *df=[NSUserDefaults standardUserDefaults];
        
        NSString *myInt = [df objectForKey:@"Remember"];
        if (![myInt isEqualToString:@"YES"]) {
            cMgr.strCompanykey=txtCompanykey.text;
            cMgr.strUsreName=txtUsername.text;
            cMgr.strPassword=txtPwd.text;
        }
        NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
        
        [def setObject:nil forKey:@"url"];
        [def setBool:NO forKey:@"AD_ZipCode"];
        [def setBool:NO forKey:@"AD_Existing_Customer"];
        [def setBool:NO forKey:@"AD_New_Customer"];
        [def setBool:NO forKey:@"AD_Both"];
        [def setBool:NO forKey:@"Is_Address"];
        [def setBool:NO forKey:@"AD_Commnet"];
        [def setBool:NO forKey:@"AD_Customer_Detail"];
        [def setObject:nil forKey:@"Company_Id"];
        [def setObject:nil forKey:@"Remember"];
        //Remember
        [def synchronize];
        
    }
    else{
        
        cMgr.strUsreName=txtUsername.text;
        NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
        
        if ([txtCompanykey.text isEqualToString:@"1"])
        {
            [def setObject:KLOGINTOAPP forKey:@"url"];
            [def setBool:[[dict valueForKey:@"AD_ZipCode"] boolValue] forKey:@"AD_ZipCode"];
            [def setBool:[[dict valueForKey:@"AD_Existing_Customer"] boolValue] forKey:@"AD_Existing_Customer"];
            [def setBool:[[dict valueForKey:@"AD_New_Customer"] boolValue] forKey:@"AD_New_Customer"];
            [def setBool:[[dict valueForKey:@"AD_Both"] boolValue] forKey:@"AD_Both"];
            [def setBool:[[dict valueForKey:@"Is_Address"] boolValue] forKey:@"Is_Address"];
            [def setBool:[[dict valueForKey:@"AD_Commnet"] boolValue] forKey:@"AD_Commnet"];
            [def setBool:[[dict valueForKey:@"AD_Customer_Detail"] boolValue] forKey:@"AD_Customer_Detail"];
            [def setValue:[dict valueForKey:@"AD_Default_Urgency"] forKey:@"AD_Default_Urgency"];
            [def setObject:[dict valueForKey:@"Company_Id"] forKey:@"Company_Id"];
            [def setObject:[dict valueForKey:@"EmployeeId"] forKey:@"EmployeeId"];
            [def setObject:[dict valueForKey:@"Logo"] forKey:@"Logo"];
            [def setObject:[dict valueForKey:@"Company_Name"] forKey:@"Company_Name"];
            //
            [def synchronize];
        } else {
            // [def setObject:KLOGINTOAPP forKey:@"url"];
            [def setObject:[dict valueForKey:@"WebURL"] forKey:@"url"];
            [def setBool:[[dict valueForKey:@"AD_ZipCode"] boolValue] forKey:@"AD_ZipCode"];
            [def setBool:[[dict valueForKey:@"AD_Existing_Customer"] boolValue] forKey:@"AD_Existing_Customer"];
            [def setBool:[[dict valueForKey:@"AD_New_Customer"] boolValue] forKey:@"AD_New_Customer"];
            [def setBool:[[dict valueForKey:@"AD_Both"] boolValue] forKey:@"AD_Both"];
            [def setBool:[[dict valueForKey:@"Is_Address"] boolValue] forKey:@"Is_Address"];
            [def setBool:[[dict valueForKey:@"AD_Commnet"] boolValue] forKey:@"AD_Commnet"];
            [def setBool:[[dict valueForKey:@"AD_Customer_Detail"] boolValue] forKey:@"AD_Customer_Detail"];
            [def setValue:[dict valueForKey:@"AD_Default_Urgency"] forKey:@"AD_Default_Urgency"];
            [def setObject:[dict valueForKey:@"Company_Id"] forKey:@"Company_Id"];
            [def setObject:[dict valueForKey:@"EmployeeId"] forKey:@"EmployeeId"];
            [def setObject:[dict valueForKey:@"Logo"] forKey:@"Logo"];
            [def setObject:[dict valueForKey:@"Company_Name"] forKey:@"Company_Name"];
            //
            [def synchronize];
        }
        
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSString *strImageName = [NSString stringWithFormat:@"%@",[defsLogindDetail valueForKey:@"Logo"]];
        
        NSString *temp_Image_String = [[NSString alloc] initWithFormat:@"%@/C_Logo/%@",MainUrlLogo,strImageName];
        NSString *newStrUrlString =[temp_Image_String stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url_For_Ad_Image = [[NSURL alloc] initWithString:newStrUrlString];
        NSData *data_For_Ad_Image = [[NSData alloc] initWithContentsOfURL:url_For_Ad_Image];
        UIImage *temp_Ad_Image = [[UIImage alloc] initWithData:data_For_Ad_Image];
        [self saveImage:temp_Ad_Image :strImageName];
        
        
        //Change For Splash Screen 15 may 2017
        
        NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
        
        BOOL isFromSplashView=[defss boolForKey:@"FromSplashView"];
        
        if (isFromSplashView) {
            
            [self goToDeveloperView];
            
        } else {
            
            [self goToSplashView];
            
        }
        
        [defss setBool:NO forKey:@"FromSplashView"];
        [defss setBool:NO forKey:@"FromAppDelegate"];
        [defss synchronize];
        
        
    }
}


-(void)goToSplashView{
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad"
                                                                 bundle: nil];
        SplashViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"SplashViewController"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
        
    }else{
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone"
                                                                 bundle: nil];
        SplashViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"SplashViewController"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
}

-(void)goToDeveloperView{
    
    NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
    BOOL isAD_Existing_Customer =[defsss boolForKey:@"AD_Existing_Customer"];
    BOOL isAD_New_Customer =[defsss boolForKey:@"AD_New_Customer"];
    BOOL isAD_Both =[defsss boolForKey:@"AD_Both"];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if (isAD_Both) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
            TabbbarViewController* verificationBadgesViewController = (TabbbarViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Developer"];
            [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
        } else if (isAD_Existing_Customer){
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
            TabbbarViewController* verificationBadgesViewController = (TabbbarViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Developer"];
            [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
        }else if (isAD_New_Customer){
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
            TabbbarViewController* verificationBadgesViewController = (TabbbarViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Developer"];
            [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
        }
    }
    else
    {
        //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
        
        UIStoryboard *storyboard;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
        }
        else
        {
            
            storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
        }
        if (isAD_Both) {
            TabbbarViewController* verificationBadgesViewController = (TabbbarViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Developer"];
            [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
        } else if (isAD_Existing_Customer){
            TabbbarViewController* verificationBadgesViewController = (TabbbarViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Developer"];
            [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
        }else if (isAD_New_Customer){
            //                NewcustomerViewController* lgn = (NewcustomerViewController*)[storyboard instantiateViewControllerWithIdentifier:@"new"];
            //                [self.navigationController  pushViewController:lgn animated:NO];
            TabbbarViewController* verificationBadgesViewController = (TabbbarViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Developer"];
            [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
        }
    }
    
}
-(void)downloadImage :(NSString*)strImageName
{
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    strImageName = [NSString stringWithFormat:@"%@",[defsLogindDetail valueForKey:@"Logo"]];
    
    NSString *temp_Image_String = [[NSString alloc] initWithFormat:@"%@/C_Logo/%@",MainUrlLogo,strImageName];
    NSURL *url_For_Ad_Image = [[NSURL alloc] initWithString:temp_Image_String];
    NSData *data_For_Ad_Image = [[NSData alloc] initWithContentsOfURL:url_For_Ad_Image];
    UIImage *temp_Ad_Image = [[UIImage alloc] initWithData:data_For_Ad_Image];
    [self saveImage:temp_Ad_Image :strImageName];
}

- (void)saveImage: (UIImage*)image :(NSString*)strImageName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",strImageName]];
    NSData* data = UIImagePNGRepresentation(image);
    [data writeToFile:path atomically:YES];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Unable to connect with server,Please try later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [alert show];
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setObject:@"No" forKey:@"Remember"];
    [def synchronize];
    
    [activity stopAnimating];
    [activity setHidden:YES];
    
}
//This method , you can use to receive the error report in case of connection is not made to server.

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    
    
}
#pragma mark-textfield delegate method


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect textFieldRect =[self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =[self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =(MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    // NSLog(@"%f",denominator);
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =[[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [tex1 resignFirstResponder];
    [tex2 resignFirstResponder];
    [txtCompanykey resignFirstResponder];
    return YES;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{    UITouch *touch = [[event allTouches] anyObject];
    
    [super touchesBegan:touches withEvent:event];
    if (touch) {
        [tex1 resignFirstResponder];
        [tex2 resignFirstResponder];
        [txtCompanykey resignFirstResponder];
    }
}


-(void)checkAppVersion
{
    
    NSString *strAppStoreId=@"734464060";
    NSString *strBundleIdentifier=@"leadnow.quacito.com";
    
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", strBundleIdentifier]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:request
     
                                       queue:[NSOperationQueue mainQueue]
     
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if (!error) {
                                   
                                   NSError* parseError;
                                   
                                   NSDictionary *appMetadataDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&parseError];
                                   
                                   NSArray *resultsArray = (appMetadataDictionary)?[appMetadataDictionary objectForKey:@"results"]:nil;
                                   
                                   NSDictionary *resultsDic = [resultsArray firstObject];
                                   
                                   if (resultsDic) {
                                       
                                       // compare version with your apps local version
                                       
                                       NSString *iTunesVersion = [resultsDic objectForKey:@"version"];
                                       
                                       
                                       
                                       NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)@"CFBundleShortVersionString"];
                                       
                                       if (iTunesVersion && [appVersion compare:iTunesVersion] != NSOrderedDescending&&[appVersion compare:iTunesVersion] != NSOrderedSame) {
                                           
                                           UIAlertController * alert=   [UIAlertController
                                                                         
                                                                         alertControllerWithTitle:@"Update Available"
                                                                         
                                                                         message:@""
                                                                         
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                                           
                                           
                                           
                                           UIAlertAction* updateBtn = [UIAlertAction
                                                                       
                                                                       actionWithTitle:@"Update"
                                                                       
                                                                       style:UIAlertActionStyleDefault
                                                                       
                                                                       handler:^(UIAlertAction * action)
                                                                       
                                                                       {
                                                                           
                                                                           NSString *iTunesLink = [NSString stringWithFormat:@"itms://itunes.apple.com/us/app/apple-store/id%@?mt=8",strAppStoreId];
                                                                           
                                                                           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                                                       }];
                                           
                                           UIAlertAction *Cancelbtn = [UIAlertAction
                                                                       
                                                                       actionWithTitle:@"Cancel"
                                                                       
                                                                       style:UIAlertActionStyleDefault
                                                                       
                                                                       handler:^(UIAlertAction * action)
                                                                       
                                                                       {
                                                                           
                                                                           [self dismissViewControllerAnimated:YES completion:nil];
                                                                           
                                                                       }];
                                           
                                           [alert addAction:Cancelbtn];
                                           
                                           [alert addAction:updateBtn];
                                           [self presentViewController:alert animated:YES completion:nil];
                                       }
                                   }
                               } else {
                                   // error occurred with http(s) request
                                   NSLog(@"error occurred communicating with iTunes");
                               }
                           }];
    
}

@end

