 //
//  OutboxViewController.m
//  Lead Now
//
//  Created by Rakesh Jain on 05/10/13.
//  Copyright (c) 2013 Infocrats. All rights reserved.
//

#import "OutboxViewController.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import "MyObjects.h"
#import "AddleadViewController.h"
#import "DeveloperViewController.h"
#import "DejalActivityView.h"

@interface OutboxViewController ()
{
    MyObjects *objClass;
    BOOL isSendingMaster;

}
@end

@implementation OutboxViewController
@synthesize objectsMutable;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isSendingMaster=YES;
    
    //hide extra separators
    tblView.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];

    objClass = [MyObjects shareManager];
    NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
    
    NSString *strCheckBoxValue = [userDefault valueForKey:@"checkboxvalue"];
    
    if([strCheckBoxValue isEqualToString:@"on"])
    {
       UIImage *img=[UIImage imageNamed:@"check.png"];
        [btnChk setBackgroundImage:img forState:UIControlStateNormal];
         isSelectNoBId=YES;
        
        
    }
    else
    {
        UIImage *img=[UIImage imageNamed:@"uncheck.png"];
        [btnChk setBackgroundImage:img forState:UIControlStateNormal];
         isSelectNoBId=NO;
        //[self postdatatoserver];
    }
    
    tblView.delegate=self;
    tblView.dataSource=self;
    
    cMgr=[CacheManager getInstance];
    
   // NSLog(@"%@", cMgr.cacheName);
    RemoveIdv=[[NSMutableArray alloc]init];
    
    
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(timerOn)
//                                                 name:@"TestNotification"
//                                               object:nil];
    
	// Do any additional setup after loading the view.
    
    
    if ([[UIScreen mainScreen] bounds].size.height <= 736){
        self.height.constant = 60;
    }else{
        self.height.constant = 135;
        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    isMasterData = FALSE;
    // [tblView reloadData];
    AppDelegate *appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _managedObjectContext= [appDelegate1 managedObjectContext];
    
    NSEntityDescription    *entityDesc111=[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext];
    
    NSFetchRequest  *request1211=[[NSFetchRequest alloc
                                  ]init];
    //
    
    // [request setEntity:entityDesc];
    
    [request1211 setEntity:entityDesc111];
    
    NSPredicate    *pred11 = [NSPredicate predicateWithFormat:@"(username= %@)",cMgr.strUsreName];
    [request1211 setPredicate:pred11];
    
    NSError *error;
    
    //objects = [_managedObjectContext executeFetchRequest:request error:&error];
    //   pred = [NSPredicate predicateWithFormat:@"(statuso= %@)",@"true"];
    //   [request1211 setPredicate:pred];
    NSArray    *objects121 = [_managedObjectContext executeFetchRequest:request1211 error:&error];
    objectsMutable = [NSMutableArray  arrayWithArray:objects121];
    if ([objClass.ArrOutBox count]>0) {
        objClass.ArrOutBox=[objClass.ArrOutBox arrayByAddingObjectsFromArray:objectsMutable];
    }
    else{
        objClass.ArrOutBox= [objectsMutable mutableCopy];
    }
    
    //If Somthing in outbox
    if (objectsMutable.count > 0) {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setObject:@"YES" forKey:@"isLeadsInOutbox"];
        [prefs synchronize];
        
        
        NSUserDefaults *de=[NSUserDefaults standardUserDefaults];
        strU=[de stringForKey:@"url"];
        
        if ([objects count]>0) {
            NSUserDefaults *de=[NSUserDefaults standardUserDefaults];
            strU=[de stringForKey:@"url"];
            
            
            // NSLog(@"%@",cMgr.cacheName);
            
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            _managedObjectContext= [appDelegate managedObjectContext];
            
            
            entityDesc1=[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext];
            
            request=[[NSFetchRequest alloc
                     ]init];
            
            [request setEntity:entityDesc1];
            
            NSError *error;
            
            //
            pred = [NSPredicate predicateWithFormat:@"((statuso= %@) && (username= %@))",@"true",cMgr.strUsreName];
            [request setPredicate:pred];
            objects = [_managedObjectContext executeFetchRequest:request error:&error];
        }
        
        NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
        
        NSString *strCheckBoxValue = [userDefault valueForKey:@"checkboxvalue"];
        if([strCheckBoxValue isEqualToString:@"on"])
        {
            
        }
        else
        {
            //[self postdatatoserver];
            Reachability *reachableForWiFy=[Reachability reachabilityForInternetConnection];
            NetworkStatus netStatusWify = [reachableForWiFy currentReachabilityStatus];
            if (netStatusWify== NotReachable)
            {
                //Internet not working
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Check internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                [DejalBezelActivityView activityViewForView:self.view];
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self performSelector:@selector(postdatatoserver) withObject:nil afterDelay:0.5];
                    });
                });
            }
        }
        NSLog(@"audio path %@",localaudioPath);
    }else {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setObject:@"NO" forKey:@"isLeadsInOutbox"];
        [prefs synchronize];
    }
}

-(void)timerOn
{
    if (!isSelectNoBId) {
        
        if ([objectsMutable count]>0) {
            if (![timer isValid])
                
            {
                //
             timer=[NSTimer scheduledTimerWithTimeInterval:0.0 target:self selector:@selector(CheckNetwork) userInfo:nil repeats:YES];
                //
            }
            
            else
            {
                
                
            }
            
        }
        
        
        else
        {
            
            
            if ([timer isValid])
                
                
            {
                
                
                [timer invalidate];
            }
            
            else
                
            {
                
                
            }
            
            
            
        }
        
        
    }
    
    
    else
    {
        
        
        
        
    }
    


}


-(void)CheckNetwork
{
    
      Reachability *reachableForWiFy=[Reachability reachabilityForInternetConnection];
    
    NetworkStatus netStatusWify = [reachableForWiFy currentReachabilityStatus];
    
    if (netStatusWify== NotReachable)
    {//Internet not working
        
        [timer invalidate];
        
    }
    
    
    else
        
    {
        if (!isSelectNoBId) {
            
            
            
            [timer invalidate];
            
//            [sendThread start];
            [self performSelectorInBackground:@selector(postdatatoserver) withObject:nil];
            
 //
        }
        
        
        else
        {
        
        
            [timer invalidate];
        
        }
        
        
        
    }
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{

    
   // NSString *chk2=[[NSUserDefaults standardUserDefaults]valueForKey:@"chk"];
    if ([objectsMutable count]==0)
    {
       if(isMasterData)
       {
           objClass.LeadCountOutBox=0;
           
           // [[NSUserDefaults standardUserDefaults] setObject:objectsMutable forKey:@"KeyArr"];
           [[NSNotificationCenter defaultCenter]
            postNotificationName:@"Badge"
            object:self];
           countdata.text=[NSString stringWithFormat:@"%d",0];
           notificationLabl.text=@"Your Lead has been sent";
           [DejalBezelActivityView removeView];
           isMasterData = FALSE;
           
           NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
           [prefs setObject:@"NO" forKey:@"isLeadsInOutbox"];
           [prefs synchronize];
       }
    }else
    {
        countdata.text=[NSString stringWithFormat:@"%lu",(unsigned long)[objectsMutable count]];
        //return [objectsMutable count];
    }
    objClass.LeadCountOutBox=[objectsMutable count];
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"Badge"
     object:self];
    
       return [objectsMutable count];
    
}
 
    


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *result;
    CGRect contentFrame;
    
    
    
    static NSString *PersonTableViewCell = @"PersonTableViewCell";
    
    result = [tableView dequeueReusableCellWithIdentifier:PersonTableViewCell];
    if (result) {
        result = nil;
        [result removeFromSuperview];
    }
    if (result == nil){
        result =
        [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                               reuseIdentifier:PersonTableViewCell];
        
    }
    result.backgroundView= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"account_detail_feild.png"]];
    result.selectionStyle = UITableViewCellSelectionStyleNone;
    //Tried this to clear old contents but doesn't make a difference
    
    
    
    //    result.textLabel.text =
    //    newRecords.gName;
    
    //    lblGoal.frame=CGRectMake(40, 3, 100, 30);
    ////
    //    lblGoal.text=newRecords.gName;
    //
    //    [result.contentView addSubview:lblGoal];
    
    contentFrame = CGRectMake(5
                              , 0, 200, 30.0);
    lblReferal= [[UILabel alloc] initWithFrame:contentFrame];
    
    lblReferal.backgroundColor=[UIColor clearColor];
    lblReferal.font = [UIFont boldSystemFontOfSize:15];
    
    
    if ([objectsMutable count]>indexPath.row) {
        lblReferal.text=[NSString stringWithFormat:@"Account No : %@",[[objectsMutable objectAtIndex:indexPath.row]accountNo]];
        NSLog(@"Account No Is %@ ",[NSString stringWithFormat:@"Account No : %@",[[objectsMutable objectAtIndex:indexPath.row]accountNo]]);
        [result.contentView addSubview:lblReferal];
         contentFrame = CGRectMake(6, 25, 255, 30.0);
        
        lblReferal= [[UILabel alloc] initWithFrame:contentFrame];
        lblReferal.backgroundColor=[UIColor clearColor];
        lblReferal.font = [UIFont systemFontOfSize:13];
        lblReferal.textColor=[UIColor blackColor];
        lblReferal.text=[NSString stringWithFormat:@"Name of Services : %@",[[objectsMutable objectAtIndex:indexPath.row]serviceName]];
        
        
        [result.contentView addSubview:lblReferal];
        
        contentFrame = CGRectMake(6, 34, 150, 30.0);
        
        lblReferal= [[UILabel alloc] initWithFrame:contentFrame];
        lblReferal.backgroundColor=[UIColor clearColor];
        lblReferal.font = [UIFont systemFontOfSize:13];
        lblReferal.textColor=[UIColor blackColor];
        lblReferal.text=[[objects objectAtIndex:indexPath.row]dateTime];
        [result.contentView addSubview:lblReferal];
        
        result.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        //    result.detailTextLabel.text =
        //    [NSString stringWithFormat:@"Age: %lu",
        //     (unsigned long)[person.age unsignedIntegerValue]];
    }
   if([objectsMutable objectAtIndex:indexPath.row]==0)
   {
      // NSLog(@"Done");
   }
    return result;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *)indexPath
{
    return 61;
}

-(IBAction)selectReason:(id)sender
{
    UIImage *img=[UIImage imageNamed:@"check.png"];
    
     NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
    if (isSelectNoBId)
    {
        [userDefault setValue:@"off" forKey:@"checkboxvalue"];
        [userDefault synchronize];
        img=[UIImage imageNamed:@"uncheck.png"];
         [btnChk setBackgroundImage:img forState:UIControlStateNormal];
        isSelectNoBId=NO;
        if (![timer isValid]) {
            [self timerOn];
        }
        
    }
    else
    {
        [userDefault setValue:@"on" forKey:@"checkboxvalue"];
        [userDefault synchronize];
        
        [btnChk setBackgroundImage:img forState:UIControlStateNormal];
        
        
        isSelectNoBId=YES;
        
        if ([timer isValid]) {
            [timer invalidate];
        }
        
        else
        {
        
        }
        
    }
    
}

#pragma mark-post dtat to server



-(void)postdatatoserver
{
    isSendingMaster=YES;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _managedObjectContext= [appDelegate managedObjectContext];
    
    NSEntityDescription  *entityDesc11=[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext];
    
    NSFetchRequest  *request121=[[NSFetchRequest alloc
                                  ]init];
    
    // [request setEntity:entityDesc];
    
    [request121 setEntity:entityDesc11];
    
    NSPredicate    *pred11 = [NSPredicate predicateWithFormat:@"(username= %@)",cMgr.strUsreName];
    [request121 setPredicate:pred11];
    NSError *error;
    
    //objects = [_managedObjectContext executeFetchRequest:request error:&error];
    pred = [NSPredicate predicateWithFormat:@"(statuso= %@ )",@"true"];
    [request121 setPredicate:pred];
    NSArray    *objects12 = [_managedObjectContext executeFetchRequest:request121 error:&error];
    NSMutableArray  *objectsMutables=[NSMutableArray arrayWithArray:objects12];
    
    // NSLog(@"%d",[objectsMutables count]);
    
    
    if ([objectsMutables count]==0) {
        cMgr.issSending=NO;
    }
    
    
    //ttp://staging.quacito.com/austin/ServiceHandler.ashx?key=insertMaster&AutoID=9336&SendDateTime=6/8/113&uname=ankit.kasliwal&AccountNo=54323&status=old
    
    
    //
     NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
    
    for (int i=0 ;i<[objectsMutables count]; i++) {
        NSString *post;
        NSString *strK=[[NSUserDefaults standardUserDefaults]valueForKey:@"key"];
        post = [NSString stringWithFormat:@"&AutoID=%@&SendDateTime=%@&uname=%@&comKey=%@&key=insertMaster&acomment=%@",[[objectsMutables objectAtIndex:i]autoId],[[objectsMutables objectAtIndex:i]dateTimec],cMgr.cacheName,strK,[def valueForKey:@"acomment"]];
        NSLog(@"%@",post);
        NSLog(@"%@",[[objectsMutables objectAtIndex:i]dateTimec]);
        NSString  *post1;
        if ([[[objectsMutables objectAtIndex:i]statusCustomer] isEqualToString:@"new"] || [[[objectsMutables objectAtIndex:i]statusCustomer] isEqualToString:@"old"])
        {
            
            NSString *status = [NSString stringWithFormat:@"%@",[[objectsMutables objectAtIndex:i]statusCustomer]];
            
            entityDesc2=[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext];
            
            request=[[NSFetchRequest alloc
                      ]init];

            [request setEntity:entityDesc2];
            
            NSError *error;
            
            NSPredicate    *pred11 = [NSPredicate predicateWithFormat:@"(username= %@)",cMgr.strUsreName];
            [request setPredicate:pred11];
            
            pred = [NSPredicate predicateWithFormat:@"(autoId = %@)",[[objectsMutables objectAtIndex:i]autoId]];
            [request setPredicate:pred];
            NSArray  *objects2 = [_managedObjectContext executeFetchRequest:request error:&error];
            
            
            NSString *strAccNumber=[[objectsMutables objectAtIndex:i]accountNo];
            if (strAccNumber.length<1) {
                strAccNumber=@"0";
            }

            if ([objects2 count]>0) {
                post1=[NSString stringWithFormat:@"&FName=%@&LName=%@&Phno=%@&Email=%@&status=%@&AccountNo=%@&Address1=%@&Address2=%@&City=%@&State=%@",[[objects2 objectAtIndex:0]fName],[[objects2 objectAtIndex:0]lName],[[objects2 objectAtIndex:0]phNo],[[objects2 objectAtIndex:0]emailId],status,strAccNumber,[[objects2 objectAtIndex:0]address1],[[objects2 objectAtIndex:0]address2],[[objects2 objectAtIndex:0]city],[[objects2 objectAtIndex:0]stateeee]];
                post=[post stringByAppendingString:post1];
                
            }
        }
        
        else
        {
            NSString *strAccNumber=[[objectsMutables objectAtIndex:i]accountNo];
            if (strAccNumber.length<1) {
                strAccNumber=@"0";
            }

            post1=[NSString stringWithFormat:@"&AccountNo=%@&status=old",strAccNumber];
            post=[post stringByAppendingString:post1];
        }
        
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        //    You need to send the actual length of your data. Calculate the length of the post string.
        
        
        //    3. Create a Urlrequest with all the properties like HTTP method, http header field with length of the post string.
        //    Create URLRequest object and initialize it.
        
        NSMutableURLRequest *request34 = [[NSMutableURLRequest alloc] init];
        
        //    Set the Url for which your going to send the data to that request.
        
        
        NSLog(@"%@",strU);
        
        
        
        strU =[strU stringByAppendingString:@"?"];
        [request34 setURL:[NSURL URLWithString:strU]];
        
        //    Now, set HTTP method (POST or GET).
        //    Write this lines as it is in your code.
        
        [request34 setHTTPMethod:@"POST"];
        
        //    Set HTTP header field with length of the post data.
        
        
        //    Also set the Encoded value for HTTP header Field.
        
        //  [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];
        
        //    Set the HTTPBody of the urlrequest with postData.
        
        [request34 setHTTPBody:postData];
        
        
        
        //    4. Now, create URLConnection object. Initialize it with the URLRequest.
        //   dispatch_async(dispatch_get_main_queue(), ^{
        //update UI here
        //
        NSURLConnection *conn = [[NSURLConnection alloc]initWithRequest:request34 delegate:self];
        
        //            NSHTTPURLResponse  *response = nil;
        //            [NSURLConnection sendSynchronousRequest:request1
        //                                  returningResponse:&response
        //                                              error:nil];
        
        
        
        
        
        
        if (conn) {
            
            
            
            
            
            NSLog(@"poc");
            
            
            NSFetchRequest * allCars3 = [[NSFetchRequest alloc] init];
            [allCars3 setEntity:[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext ]];
            [allCars3 setIncludesPropertyValues:NO]; //only fetch the managedObjectID
            NSPredicate    *pred11 = [NSPredicate predicateWithFormat:@"((username= %@) &&  (autoId=%@))",cMgr.strUsreName,[[objectsMutables objectAtIndex:i]autoId]];
            [allCars3 setPredicate:pred11];
//            NSPredicate *p1=[NSPredicate predicateWithFormat:@"autoId=%@ ",[[objectsMutables objectAtIndex:i]autoId]];
//            [allCars3 setPredicate:p1];
            NSError * error1 = nil;
            NSArray * cars1 = [_managedObjectContext    executeFetchRequest:allCars3 error:&error1];
            
            NSManagedObject *match=nil;
            
            if([cars1 count]==0)
            {
                
            }
            else
            {
                for (int i=0; i<[cars1 count] ; i++)
                {
                    
                    match=[cars1 objectAtIndex:i];
                    
                    [match setValue:@"send" forKey:@"statuso"];
                    
                    [_managedObjectContext save:&error];
                    
                }
                
                
            }
            
            
        }
        
        else
        {
            [DejalBezelActivityView removeView];
        }
        
    }
    
    
}

-(void)sendingDetailNew{
    
    isSendingMaster=NO;
    
    NSError *error;
    
    NSEntityDescription *entityDesc13=[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext];
    
    NSFetchRequest *request25=[[NSFetchRequest alloc
                                ]init];
    
    
    // [request setEntity:entityDesc];
    
    [request25 setEntity:entityDesc13];
    
    NSPredicate    *pred1111 = [NSPredicate predicateWithFormat:@"(username= %@)",cMgr.strUsreName];
    [request25 setPredicate:pred1111];
    
    
    NSPredicate  *pred1 = [NSPredicate predicateWithFormat:@"(statuso= %@)",@"true"];
    [request setPredicate:pred1];
    
    NSArray  *objects123 = [_managedObjectContext executeFetchRequest:request25  error:&error];
    
    
    objectsMutable=[NSMutableArray arrayWithArray:objects123];
    
    for (int i = 0; i<[objectsMutable count]; i++)
    {
        
        imagNamelocal=[[objectsMutable objectAtIndex:i]imgName];
        
        if ([imagNamelocal isEqualToString:@""]) {
            imagNamelocal=@"";
        }
        
        else if ([imagNamelocal isEqualToString:@"null"]) {
            imagNamelocal=@"";
        }
        
        else if (imagNamelocal==nil) {
            imagNamelocal=@"";
        }
        NSLog(@"%@",[[objectsMutable objectAtIndex:i]imgName]);
        localPath=[[objectsMutable objectAtIndex:i]savedImagePath];
        
        audioNamelocal=[[objectsMutable objectAtIndex:i]audioName];
        
        
        if ([audioNamelocal isEqualToString:@""]) {
            audioNamelocal=@"";
        }
        
        else if ([audioNamelocal isEqualToString:@"null"]) {
            audioNamelocal=@"";
        }
        
        else if (audioNamelocal==nil) {
            audioNamelocal=@"";
        }
        localaudioPath=[[objectsMutable objectAtIndex:i]audioPath];
        
        NSLog(@"localaudioPath%d = %@",i,localaudioPath);
        //[self uploadaudio];
        [RemoveIdv insertObject:[[objectsMutable objectAtIndex:i]autoId] atIndex:i];
        
        NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
        
        NSString *postn;
        NSString *strAccNumber=[[objectsMutable objectAtIndex:i]accountNo];
        if (strAccNumber.length<1) {
            strAccNumber=@"0";
        }
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        NSString *strEmpId=[NSString stringWithFormat:@"%@",[defs valueForKey:@"EmployeeId"]];

        NSLog(@"image name will---%@",imagNamelocal);
        postn = [NSString stringWithFormat:@"&autoid=%@&serviceid=%@&servicename=%@&images=%@&audio=%@&comment=%@&urgency=%@&acno=%@&key=detailinsert&coid=%@&zipcode=%@&uname=%@&EmployeeId=%@",[[objectsMutable objectAtIndex:i]autoId],[[objectsMutable objectAtIndex:i]serviceId],[[objectsMutable objectAtIndex:i]serviceName], imagNamelocal,audioNamelocal,[[objectsMutable objectAtIndex:i]comment],[[objectsMutable objectAtIndex:i]urgencyLevel],strAccNumber,[def valueForKey:@"Company_Id"],[def valueForKey:@"zipcode"],cMgr.cacheName,strEmpId];
        
        NSData *postData1 = [postn dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        //    You need to send the actual length of your data. Calculate the length of the post string.
        
        
        //    3. Create a Urlrequest with all the properties like HTTP method, http header field with length of the post string.
        //    Create URLRequest object and initialize it.
        
        NSMutableURLRequest *request2 = [[NSMutableURLRequest alloc] init];
        
        //    Set the Url for which your going to send the data to that request.
        
        [request2 setURL:[NSURL URLWithString:strU]];
        
        //    Now, set HTTP method (POST or GET).
        //    Write this lines as it is in your code.
        [request2 setHTTPMethod:@"POST"];
        
        //    Set HTTP header field with length of the post data.
        
        
        //    Also set the Encoded value for HTTP header Field.
        
        //  [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];
        
        //    Set the HTTPBody of the urlrequest with postData.
        
        [request2 setHTTPBody:postData1];
        
        
        //    4. Now, create URLConnection object. Initialize it with the URLRequest.
        
        
        
        //update UI here
        
        
        NSURLConnection *conn1 = [[NSURLConnection alloc]initWithRequest:request2 delegate:self];
        
        if(conn1)
        {
            
            
            
            //            NSLog(@"Connection Successful”);
            
            NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
            [allCars setEntity:[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext ]];
            [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
            
            NSPredicate    *pred1111 = [NSPredicate predicateWithFormat:@"((username= %@) &&  (iD=%@))",cMgr.strUsreName,[[objectsMutable objectAtIndex:i]iD]];
            [allCars setPredicate:pred1111];
            
            //            NSPredicate *p=[NSPredicate predicateWithFormat:@"iD=%@", [[objectsMutable objectAtIndex:i]iD]];
            //            [allCars setPredicate:p];
            NSError * error = nil;
            NSArray * cars = [_managedObjectContext    executeFetchRequest:allCars error:&error];
            //
            //        //error handling goes here
            
            
            NSManagedObject *match=nil;
            
            if([cars count]==0)
            {
                
                
            }
            else
            {
                for (int i=0; i<[cars count] ; i++)
                {
                    
                    match=[cars objectAtIndex:i];
                    
                    [match setValue:@"send" forKey:@"statuso"];
                    
                    [_managedObjectContext save:&error];
                    
                }
                
                
                
            }
        }
        else
        {
            //  NSLog(@"Connection could not be made”);
        }
        
        //    It returns the initialized url connection and begins to load the data for the url request. You can check that whether you URL connection is done properly or not using just if/else statement as below.
        
        
        //  cMgr.issSending=NO;
        
        //     [self sendAudio];
        
    }
    
    
    
    //cMgr.issSending=NO;
    //[self uploadImage];
    
    
    if([objectsMutable count] > 0)
    {
        isMasterData=TRUE;
    }
    else
    {
        isMasterData=FALSE;
    }
    
    [objectsMutable removeAllObjects];
    
    if ([objClass.ArrOutBox count]>0) {
        objClass.ArrOutBox=nil;
    }
    
    [objClass.ArrOutBox removeAllObjects];
    
    [tblView reloadData];
    
    NSFetchRequest * allCars1 = [[NSFetchRequest alloc] init];
    [allCars1 setEntity:[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext ]];
    [allCars1 setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSPredicate    *pred11111 = [NSPredicate predicateWithFormat:@"(username= %@ && statuso= %@)",cMgr.strUsreName, @"send"];
    [allCars1 setPredicate:pred11111];
    
    NSError * error1 = nil;
    NSArray * cars1 = [_managedObjectContext    executeFetchRequest:allCars1 error:&error1];
    
    //error handling goes here
    for (NSManagedObject * car1 in cars1) {
        [_managedObjectContext  deleteObject:car1];
    }
    NSError *saveError1 = nil;
    [_managedObjectContext save:&saveError1];    // GetService
    
    
    // cMgr.isAutoid=YES;

}

 -(void)uploadImage{
        /*
         turning the image into a NSData object
         getting the image back out of the UIImageView
         setting the quality to 90
         */
    
        //http://staging.quacito.com/austin/ImageUploader.ashx
          
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    _managedObjectContext= [appDelegate managedObjectContext];
    
    
    entityDesc1=[NSEntityDescription entityForName:@"Imgpath" inManagedObjectContext:_managedObjectContext];
    
     NSFetchRequest *request1=[[NSFetchRequest alloc
              ]init];
     NSPredicate    *pred1 = [NSPredicate predicateWithFormat:@"(username= %@)",cMgr.strUsreName];
     [request1 setPredicate:pred1];
    
    [request1 setEntity:entityDesc1];
    
    NSError *error;
    
    //    objects = [_managedObjectContext executeFetchRequest:request error:&error];
    //
    
    //
   
    NSArray *objects2 = [_managedObjectContext executeFetchRequest:request1 error:&error];
    
    
    NSMutableArray  *objectsMutables1=[NSMutableArray arrayWithArray:objects2];
    

     
     NSLog(@"array  count ---%lu",(unsigned long)[objectsMutables1 count]);
    
    for (int i=0; i<[objectsMutables1 count]; i++)
    {
        
   
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
               // [self performSelector:@selector(postdatatoserver) withObject:nil afterDelay:0.5];
      
    
     NSData *imageData;
       
    UIImage *_img = [UIImage imageWithContentsOfFile:[[objectsMutables1 objectAtIndex:i]pathImg]];
        
        removalId=[[objectsMutables1 objectAtIndex:i]imgNameimg];
     UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,1.0,1)];
    imgView.image = _img;
    imageData =  UIImageJPEGRepresentation(imgView.image, 0.00000000000000000000000000001f);
       // NSLog(@"%@",imageData);
    // [self.view addSubview:imgView];
    
    // setting up the request object now
    //  	// setting up the URL to post to
        
         NSString *st=[strU stringByReplacingOccurrencesOfString:@"ServiceHandler.ashx" withString:@"ImageUploader.ashx"];
	NSString *urlString = [NSString stringWithFormat:@"%@",st];
       // NSLog(@"%@",urlString);
	
	NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
	[request1 setURL:[NSURL URLWithString:urlString]];
	[request1 setHTTPMethod:@"POST"];
	
	/*
	 add some header info now
	 we always need a boundary when we post a file
	 also we need to set the content type
	 
	 You might want to generate a random boundary.. this is just the same
	 as my output from wireshark on a valid html post
     */
	NSString *boundary = @"---------------------------14737809831466499882746641449";
	NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
	[request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
	
	/*
	 now lets create the body of the post
     */
	NSMutableData *body = [NSMutableData data];
	[body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
        
        NSString *combine =[[objectsMutables1 objectAtIndex:i]imgNameimg];
        
               
	[body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@.png\"\r\n",combine] dataUsingEncoding:NSUTF8StringEncoding]];
	[body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
	[body appendData:[NSData dataWithData:imageData]];
   // NSLog(@"%@",imageData);
	[body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
	// setting the body of the post to the reqeust
	[request1 setHTTPBody:body];
	
	// now lets make the connection to the web
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
	NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];

        
        
        
            
	//NSLog(@"%@",returnString);
        if ([returnString isEqualToString:@"OK"] ) {
//            
//            combine =[combine stringByAppendingString:@".png"];
//            NSString *comma=@",";
//            combine=[combine stringByAppendingString:comma];
//            NSString *anthr=;
//            anthr=[anthr stringByAppendingString:@".png"];
//            NSString *yhi=[combine stringByAppendingString:anthr];
//            NSLog(@"%@",yhi);
            NSData *imgd4;
            
            UIImage *img1 = [UIImage imageWithContentsOfFile:[[objectsMutables1 objectAtIndex:i]pathImg1]];
            
           // NSLog(@"%@",[[objectsMutables1 objectAtIndex:i]pathImg1]);
            removalId=[[objectsMutables1 objectAtIndex:i]imgNameimg];
            UIImageView *imgView1 =  [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,1.0,1.0)];
            imgView1.image = img1;
           
            
       imgd4 =  UIImageJPEGRepresentation(imgView1.image, 0.00000000000000000000000000001f);
           // NSLog(@"%@",imgd4);
            // [self.view addSubview:imgView];
            
            
            //  	// setting up the URL to post to
    
            
            // setting up the request object now
            
            NSString *st=[strU stringByReplacingOccurrencesOfString:@"ServiceHandler.ashx" withString:@"ImageUploader.ashx"];
            
           // NSLog(@"%@",st);
            NSString *urlString = [NSString stringWithFormat:@"%@",st];
            
            
            NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
            [request1 setURL:[NSURL URLWithString:urlString]];
            [request1 setHTTPMethod:@"POST"];
            
            /*
             add some header info now
             we always need a boundary when we post a file
             also we need to set the content type
             
             You might want to generate a random boundary.. this is just the same
             as my output from wireshark on a valid html post
             */
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            [request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            /*
             now lets create the body of the post
             */
            NSMutableData *body = [NSMutableData data];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            NSString *combine =[[objectsMutables1 objectAtIndex:i]imgNameimg1];
            
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@.png\"\r\n",combine] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:imgd4]];
          //  NSLog(@"%@",imgd4);
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            // setting the body of the post to the reqeust
            [request1 setHTTPBody:body];
            
            // now lets make the connection to the web
            NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
            NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
            
            
            
            
           // NSLog(@"%@",returnString);
            if ([returnString isEqualToString:@"OK"]) {
                
               // if
                
                NSData *imgd4;
                
                UIImage *img1 = [UIImage imageWithContentsOfFile:[[objectsMutables1 objectAtIndex:i]pathImg2]];
                
                // NSLog(@"%@",[[objectsMutables1 objectAtIndex:i]pathImg1]);
                removalId=[[objectsMutables1 objectAtIndex:i]imgNameimg2];
                UIImageView *imgView1 =  [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,1.0,1.0)];
                imgView1.image = img1;
                
                
                imgd4 =  UIImageJPEGRepresentation(imgView1.image, 0.00000000000000000000000000001f);
                // NSLog(@"%@",imgd4);
                // [self.view addSubview:imgView];
                
                
                //  	// setting up the URL to post to
                
                
                // setting up the request object now
                
                NSString *st=[strU stringByReplacingOccurrencesOfString:@"ServiceHandler.ashx" withString:@"ImageUploader.ashx"];
                
                // NSLog(@"%@",st);
                NSString *urlString = [NSString stringWithFormat:@"%@",st];
                
                
                NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
                [request1 setURL:[NSURL URLWithString:urlString]];
                [request1 setHTTPMethod:@"POST"];
                
                /*
                 add some header info now
                 we always need a boundary when we post a file
                 also we need to set the content type
                 
                 You might want to generate a random boundary.. this is just the same
                 as my output from wireshark on a valid html post
                 */
                NSString *boundary = @"---------------------------14737809831466499882746641449";
                NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
                [request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
                
                /*
                 now lets create the body of the post
                 */
                NSMutableData *body = [NSMutableData data];
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                
                
                NSString *combine =[[objectsMutables1 objectAtIndex:i]imgNameimg2];
                
                
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@.png\"\r\n",combine] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[NSData dataWithData:imgd4]];
                //  NSLog(@"%@",imgd4);
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                // setting the body of the post to the reqeust
                [request1 setHTTPBody:body];
                
                // now lets make the connection to the web
                NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
                NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
                
                

                
                if ([returnString isEqualToString:@"OK"]) {
                    
                    // if
                    
                    NSData *imgd4;
                    
                    UIImage *img1 = [UIImage imageWithContentsOfFile:[[objectsMutables1 objectAtIndex:i]pathImg3]];
                    
                    // NSLog(@"%@",[[objectsMutables1 objectAtIndex:i]pathImg1]);
                    removalId=[[objectsMutables1 objectAtIndex:i]imgNameimg3];
                    UIImageView *imgView1 =  [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,1.0,1.0)];
                    imgView1.image = img1;
                    
                    
                    imgd4 =  UIImageJPEGRepresentation(imgView1.image, 0.00000000000000000000000000001f);
                    // NSLog(@"%@",imgd4);
                    // [self.view addSubview:imgView];
                    
                    
                    //  	// setting up the URL to post to
                    
                    
                    // setting up the request object now
                    
                    NSString *st=[strU stringByReplacingOccurrencesOfString:@"ServiceHandler.ashx" withString:@"ImageUploader.ashx"];
                    
                    // NSLog(@"%@",st);
                    NSString *urlString = [NSString stringWithFormat:@"%@",st];
                    
                    
                    NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
                    [request1 setURL:[NSURL URLWithString:urlString]];
                    [request1 setHTTPMethod:@"POST"];
                    
                    /*
                     add some header info now
                     we always need a boundary when we post a file
                     also we need to set the content type
                     
                     You might want to generate a random boundary.. this is just the same
                     as my output from wireshark on a valid html post
                     */
                    NSString *boundary = @"---------------------------14737809831466499882746641449";
                    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
                    [request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
                    
                    /*
                     now lets create the body of the post
                     */
                    NSMutableData *body = [NSMutableData data];
                    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    
                    NSString *combine =[[objectsMutables1 objectAtIndex:i]imgNameimg3];
                    
                    
                    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@.png\"\r\n",combine] dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:[NSData dataWithData:imgd4]];
                    //  NSLog(@"%@",imgd4);
                    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    // setting the body of the post to the reqeust
                    [request1 setHTTPBody:body];
                    
                    // now lets make the connection to the web
                    NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
                    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
                }
            }
        }
            });
        });
}
     
     
     //end here
     NSFetchRequest * allCars3 = [[NSFetchRequest alloc] init];
     [allCars3 setEntity:[NSEntityDescription entityForName:@"Imgpath" inManagedObjectContext:_managedObjectContext ]];
     [allCars3 setIncludesPropertyValues:NO]; //only fetch the managedObjectID
     NSPredicate    *pred1111 = [NSPredicate predicateWithFormat:@"((username= %@) && (imgNameimg =%@))",cMgr.strUsreName,removalId];
     [allCars3 setPredicate:pred1111];

//     NSPredicate *p1=[NSPredicate predicateWithFormat:@"imgNameimg =%@",removalId];
//     [allCars3 setPredicate:p1];
     NSError * error1 = nil;
     NSArray * cars1 = [_managedObjectContext    executeFetchRequest:allCars3 error:&error1];
     //
     //        //error handling goes here
//     for (NSManagedObject * car in cars1) {
//         //
//         //
//         [_managedObjectContext  deleteObject:car];
//         NSError *saveError = nil;
//         [_managedObjectContext save:&saveError];
//         
//         
//         
//     }

    
         }


-(void)uploadaudio
{
    
    //Audiopath
    
    
    
    

  //  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
   // NSString *documentsDirectory = [paths objectAtIndex:0];
   
    NSData *audioData;
 
    
    audioData = [NSData dataWithContentsOfFile:localaudioPath];
    
   // NSLog(@"%@",audioNamelocal);
   // NSLog(@"%@",localaudioPath);
   // NSLog(@"Data:%@.......",audioData);
     NSString *st=[strU stringByReplacingOccurrencesOfString:@"ServiceHandler.ashx" withString:@"AudioHandler.ashx"];
    
   // NSLog(@"%@",st);
	// setting up the URL to post to
    
	NSString *urlString = [NSString stringWithFormat:@"%@",st];
	
	// setting up the request object now
	NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
	[request1 setURL:[NSURL URLWithString:urlString]];
	[request1 setHTTPMethod:@"POST"];
	
	/*
	 add some header info now
	 we always need a boundary when we post a file
	 also we need to set the content type
	 
	 You might want to generate a random boundary.. this is just the same
	 as my output from wireshark on a valid html post
     */
	NSString *boundary = @"---------------------------14737809831466499882746641449";
	NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
	[request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
	
	/*
	 now lets create the body of the post
     */
	NSMutableData *body = [NSMutableData data];
	[body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
	[body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",audioNamelocal] dataUsingEncoding:NSUTF8StringEncoding]];
	[body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
	[body appendData:[NSData dataWithData:audioData]];
	[body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
	// setting the body of the post to the reqeust
	[request1 setHTTPBody:body];
	
	// now lets make the connection to the web
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
	NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
	
	NSLog(@"audio send staus at path %@------>     %@",returnString,localaudioPath);
    
    

    

}
-(void)postToserverDetail
{
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //            // Add code here to do background processing
        //            //
        //            //
        dispatch_async( dispatch_get_main_queue(), ^{
            
        [self uploadImage];
    });
    
     });
    
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //            // Add code here to do background processing
        //            //
        //            //
        dispatch_async( dispatch_get_main_queue(), ^{
    
    
    for (int i = 0; i<[objectsMutable count]; i++) {
        

        imagNamelocal=[[objectsMutable objectAtIndex:i]imgName];
       // NSLog(@"%@",[[objectsMutable objectAtIndex:i]imgName]);
        localPath=[[objectsMutable objectAtIndex:i]savedImagePath];

        audioNamelocal=[[objectsMutable objectAtIndex:i]audioName];
        localaudioPath=[[objectsMutable objectAtIndex:i]audioPath];
        [RemoveIdv insertObject:[[objectsMutable objectAtIndex:i]autoId] atIndex:i];
         //[self uploadaudio];
        
        NSString *postn;
    
      postn = [NSString stringWithFormat:@"&autoid=%@&serviceid=%@&servicename=%@&images=%@&audio=%@&comment=%@&urgency=%@&acno=%@&key=detailinsert",[[objectsMutable objectAtIndex:i]autoId],[[objectsMutable objectAtIndex:i]serviceId],[[objectsMutable objectAtIndex:i]serviceName],[[objectsMutable objectAtIndex:i]imgName],[[objectsMutable objectAtIndex:i]audioName],[[objectsMutable objectAtIndex:i]comment],[[objectsMutable objectAtIndex:i]urgencyLevel],[[objectsMutable objectAtIndex:i]accountNo]];
       
    NSData *postData1 = [postn dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    //    You need to send the actual length of your data. Calculate the length of the post string.
    
    
    //    3. Create a Urlrequest with all the properties like HTTP method, http header field with length of the post string.
    //    Create URLRequest object and initialize it.
    
    NSMutableURLRequest *request2 = [[NSMutableURLRequest alloc] init];
    
    //    Set the Url for which your going to send the data to that request.
    
    [request2 setURL:[NSURL URLWithString:KLOGINTOAPP]];
    
    //    Now, set HTTP method (POST or GET).
    //    Write this lines as it is in your code.
       [request2 setHTTPMethod:@"POST"];
    
    //    Set HTTP header field with length of the post data.
    
    
    //    Also set the Encoded value for HTTP header Field.
    
    //  [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];
    
    //    Set the HTTPBody of the urlrequest with postData.
    
    [request2 setHTTPBody:postData1];
    
    
    
    //    4. Now, create URLConnection object. Initialize it with the URLRequest.
    
    NSURLConnection *conn1 = [[NSURLConnection alloc]initWithRequest:request2 delegate:self];
    
    
    //    It returns the initialized url connection and begins to load the data for the url request. You can check that whether you URL connection is done properly or not using just if/else statement as below.
    if(conn1)
    {
        //            NSLog(@"Connection Successful”);
    }
    else
    {
        //  NSLog(@"Connection could not be made”);
    }

    }
        });
    });
}
//    5. To receive the data from the HTTP request , you can use the delegate methods provided by the URLConnection Class Reference. Delegate methods are as below.


#pragma mark-connection delegate method
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    
    if (isSendingMaster) {
        
        isSendingMaster=NO;
        
        [self sendingDetailNew];
        
    } else {
        
        
        
    }
    
  //  NSLog(@"%@",detaillocalId);
    NSString* newStr = [[NSString alloc] initWithData:data
                                             encoding:NSUTF8StringEncoding];
   // NSLog(@"%@",newStr);
    NSArray *linesxhkspace = [newStr componentsSeparatedByString:@" "];
    
    
    
//    [RemoveIdv insertObject:[[objectsMutable objectAtIndex:i]iD] atIndex:i];
    
    
    
    NSString *ChkEquals=linesxhkspace[0];
    
    if ([newStr isEqualToString:@"ok"])
    {
      
        NSString *chk2=[[NSUserDefaults standardUserDefaults]valueForKey:@"chk"];
        if ([chk2 isEqualToString:@"OK"])
        {
            countdata.text=[NSString stringWithFormat:@"%d",0];
            notificationLabl.text=@"Your Lead has been sent";
            NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
            [def setObject:@"" forKey:@"zipcode"];
            
            [def setObject:@"" forKey:@"acomment"];
            [def synchronize];
            
            

            
        }else
        {
            countdata.text=[NSString stringWithFormat:@"%lu",(unsigned long)[objectsMutable count]];
            //return [objectsMutable count];
        }

       
       // NSLog(@"send");
        
        for (int i=0; i<[RemoveIdv count]; i++) {
            
            //NSLog(@"%@",[RemoveIdv objectAtIndex:i]);
        
 
        NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
        [allCars setEntity:[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext ]];
        [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        NSPredicate *p=[NSPredicate predicateWithFormat:@"autoId=%@",[RemoveIdv objectAtIndex:i]];
        [allCars setPredicate:p];
        NSError * error = nil;
        NSArray * cars = [_managedObjectContext    executeFetchRequest:allCars error:&error];
//        
//        //error handling goes here
       for (NSManagedObject * car in cars) {
//            
//            
            [_managedObjectContext  deleteObject:car];
           NSError *saveError = nil;
           [_managedObjectContext save:&saveError];
           
           
           
                   }
      
    
        
        
        NSFetchRequest * allCars3 = [[NSFetchRequest alloc] init];
        [allCars3 setEntity:[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext ]];
        [allCars3 setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        NSPredicate *p1=[NSPredicate predicateWithFormat:@"iD=%@",[RemoveIdv objectAtIndex:i]];
        [allCars setPredicate:p1];
        NSError * error11 = nil;
        NSArray * cars11 = [_managedObjectContext    executeFetchRequest:allCars3 error:&error11];
        //
        //        //error handling goes here
        for (NSManagedObject * car in cars11) {
            //
            //
            [_managedObjectContext  deleteObject:car];
            NSError *saveError = nil;
            [_managedObjectContext save:&saveError];
        }
    }
        
    }
    else
    {
         //        [[BusyAgent defaultAgent]makeBusy:NO showBusyIndicator:NO];
        //
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"The username or password you entered is incorrect.?" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        //        [alert show];
        //
    }
    
}
//Above method is used to receive the data which we get using post method.

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    //    [[BusyAgent defaultAgent]makeBusy:NO showBusyIndicator:NO];
    
    
}
//This method , you can use to receive the error report in case of connection is not made to server.

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    
    
}
-(IBAction)back:(id)sender
{
    cMgr.accountName=nil;
    NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
    [def setObject:nil forKey:@"zipcode"];
    [def synchronize];
/*
    UIStoryboard *storyboard;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    }
    else
    {
        storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
    }
    DeveloperViewController* lgn = (DeveloperViewController*)[storyboard instantiateViewControllerWithIdentifier:@"dev"];
    [self.navigationController  pushViewController:lgn animated:YES];
    */
  //  [self.navigationController popViewControllerAnimated:NO];
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[DeveloperViewController class]]) {
            [self.navigationController popToViewController:controller
                                                  animated:YES];
            break;
        }
    }
}


@end
