//
//  HistoryViewController.h
//  AuditApp
//
//  Created by Rakesh Jain on 06/02/14.
//  Copyright (c) 2014 Rakesh Jain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "M13BadgeView.h"
#import "NIDropDown.h"
@interface HistoryViewController : UIViewController<UITableViewDataSource,UITabBarDelegate,NSURLConnectionDelegate,UIAlertViewDelegate,UIActionSheetDelegate,NIDropDownDelegate>
{

    IBOutlet UITableView *tblView;
    IBOutlet UIView *veiwdtl;
    IBOutlet UIButton *btnOut;
    
    NSURLConnection* connection;
    NSMutableData *responseData;
    NSMutableArray *arrayNames;
    NSMutableArray *arrayWorkOrder;
    NSMutableArray *arrayStatus;
    NSMutableArray *arrayPhoneNumber;
    IBOutlet UIActivityIndicatorView *activity;
    NSDictionary *jsonDictionary;
    int count;
    NSMutableArray *jsonArray;
    NSMutableArray *arrayData;
    NSMutableArray *arrayTemp;
     NIDropDown *dropDown;
    NSMutableArray *arrayDropDown;
}
- (IBAction)launchDialog:(id)sender;
-(IBAction)back:(id)sender;
-(void)gotoSetting;
-(IBAction)gotoOutBox:(id)sender;
-(IBAction)sortList:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height;

@property (nonatomic, retain) M13BadgeView *badgeView;
@property (nonatomic, retain) NSManagedObjectContext  *managedObjectContext;

@end
