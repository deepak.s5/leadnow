//
//  OutboxViewController.h
//  Lead Now
//
//  Created by Rakesh Jain on 05/10/13.
//  Copyright (c) 2013 Infocrats. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceDetailTable.h"
#import "CacheManager.h"
#import "CustomerTable.h"
#import "Imgpath.h"
@interface OutboxViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

{
    IBOutlet UIButton *btnChk;
    IBOutlet UITableView *tblView;
    IBOutlet UILabel *notificationLabl;
    NSEntityDescription *entityDesc1;
    NSFetchRequest *request;
    UILabel *lblReferal;
    
    NSArray *objects;
    NSMutableArray *objectsMutable;
    NSMutableArray *objectsMutable1;
  NSPredicate  *pred;
    NSArray *objectForarray;
    BOOL isSelectNoBId;
    IBOutlet    UIButton *btnCheck;
    CacheManager *cMgr;
    
    NSMutableData *responseData;
    
   NSTimer *timer;
    
    NSEntityDescription  *entityDesc2;
    NSString *uId;
    
    BOOL isMasterData;
    
    NSString *imagNamelocal,*audioNamelocal,*localPath,*localaudioPath,*localAutoid,*removalId;
 
    IBOutlet UILabel *countdata;
    int f;
    
    NSMutableArray *removeValue,*removeImageName,*RemoveIdv;
    
    
    NSString *detaillocalId;
    NSString *chkforResponse;
    
    NSString *strU;
    
}
@property (nonatomic, retain) NSManagedObjectContext  *managedObjectContext;
@property(nonatomic,retain)NSMutableArray *objectsMutable;


-(IBAction)selectReason:(id)sender;
-(void)timerOn;
-(void)postdatatoserver;
-(IBAction)back:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height;

@end
