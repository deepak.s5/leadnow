//
//  NewcustomerViewController.m
//  Lead Now
//
//  Created by Rakesh Jain on 03/10/13.
//  Copyright (c) 2013 Infocrats. All rights reserved.
//

#import "NewcustomerViewController.h"
#import "AppDelegate.h"
#import "AddleadViewController.h"
#import "HelpViewController.h"

#define MAXLENGTH 100

@interface NewcustomerViewController ()

@end

@implementation NewcustomerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [_helpBtnnNewcustomer setHidden:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    [segment setSelectedSegmentIndex:1];
  //  txtComment.text=@"";
     NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
    BOOL isZippCoce = [def boolForKey:@"AD_ZipCode"];
    NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
    BOOL isAD_New_Customer =[defsss boolForKey:@"AD_New_Customer"];
//    [_lblNewCustomer.layer setCornerRadius:5.0f];
//    [_lblNewCustomer.layer setBorderColor:(__bridge CGColorRef _Nullable)([UIColor colorWithRed:89/255 green:115/255 blue:88/255 alpha:1])];
//    [_lblNewCustomer.layer setBorderWidth:0.8f];
//    [_lblNewCustomer.layer setShadowColor:(__bridge CGColorRef _Nullable)([UIColor colorWithRed:89/255 green:115/255 blue:88/255 alpha:1])];
//    [_lblNewCustomer.layer setShadowOpacity:0.3];
//    [_lblNewCustomer.layer setShadowRadius:3.0];
//    [_lblNewCustomer.layer setShadowOffset:CGSizeMake(2.0, 2.0)];

    if (isAD_New_Customer) {
        [segment setHidden:YES];
        [_btnnnBackkk setHidden:YES];
        [_lblNewCustomer setHidden:NO];
    }
    if (isZippCoce) {
        lblZipCode.hidden=NO;
        imgCommentbackground.hidden=NO;
        txtZipcode.hidden=NO;
    }
    BOOL isComment = [def boolForKey:@"AD_Commnet"];
    BOOL isAddress = [def boolForKey:@"Is_Address"];
    if ((isZippCoce) && (isComment) && (isAddress)) {
        NSLog(@"ALl Three are threrr ");
    }
    else     if ((!isZippCoce) && (!isComment) && (!isAddress)) {
        NSLog(@"ALl Three are not there dskjfkl ");
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            _addleadBtnnn.frame=CGRectMake(self.view.center.x-100, _lblEmail.frame.origin.y+_lblEmail.frame.size.height+25, 200, 45);
        }
        else
        {
            _addleadBtnnn.frame=CGRectMake(self.view.center.x-62, _lblEmail.frame.origin.y+_lblEmail.frame.size.height+25, 125, 34);
        }
    }
    else
    {
    if (!isComment) {
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            lblZipCode.frame=CGRectMake(lblComment.frame.origin.x, lblComment.frame.origin.y-40, lblZipCode.frame.size.width, lblZipCode.frame.size.height);
            
            _lblAddress1.frame=CGRectMake(lblZipCode.frame.origin.x, lblZipCode.frame.origin.y+55, lblZipCode.frame.size.width, lblZipCode.frame.size.height);

            _lblAddress2.frame=CGRectMake(_lblAddress1.frame.origin.x, _lblAddress1.frame.origin.y+55, lblZipCode.frame.size.width, lblZipCode.frame.size.height);

            _lblCity.frame=CGRectMake(_lblAddress2.frame.origin.x, _lblAddress2.frame.origin.y+55, lblZipCode.frame.size.width, lblZipCode.frame.size.height);

            _lblState.frame=CGRectMake(_lblCity.frame.origin.x, _lblCity.frame.origin.y+55, lblZipCode.frame.size.width, lblZipCode.frame.size.height);
            
            
            imgCommentbackground.frame= CGRectMake(imgBackGroundComment.frame.origin.x, imgBackGroundComment.frame.origin.y, imgCommentbackground.frame.size.width, imgCommentbackground.frame.size.height);
            txtZipcode.frame=CGRectMake(imgCommentbackground.frame.origin.x+2, imgCommentbackground.frame.origin.y, txtZipcode.frame.size.width,txtZipcode.frame.size.height);
            
            _imgAddress1.frame= CGRectMake(imgCommentbackground.frame.origin.x, imgCommentbackground.frame.origin.y+imgCommentbackground.frame.size.height+10, imgCommentbackground.frame.size.width, imgCommentbackground.frame.size.height);
            _Address1Txt.frame=CGRectMake(txtZipcode.frame.origin.x+2, txtZipcode.frame.origin.y+txtZipcode.frame.size.height+15, txtZipcode.frame.size.width,txtZipcode.frame.size.height);
            
            _imgAddress2.frame= CGRectMake(_imgAddress1.frame.origin.x, _imgAddress1.frame.origin.y+_imgAddress1.frame.size.height+10, _imgAddress1.frame.size.width, _imgAddress1.frame.size.height);
            _Address2Txt.frame=CGRectMake(_Address1Txt.frame.origin.x+2, _Address1Txt.frame.origin.y+_Address1Txt.frame.size.height+15, _Address1Txt.frame.size.width,_Address1Txt.frame.size.height);
            
            _imgCity.frame= CGRectMake(_imgAddress2.frame.origin.x, _imgAddress2.frame.origin.y+_imgAddress2.frame.size.height+10, _imgAddress2.frame.size.width, _imgAddress2.frame.size.height);
            _cityTxt.frame=CGRectMake(_Address2Txt.frame.origin.x+2, _Address2Txt.frame.origin.y+_Address2Txt.frame.size.height+15, _Address2Txt.frame.size.width,_Address2Txt.frame.size.height);
            
            _imgState.frame= CGRectMake(_imgCity.frame.origin.x, _imgCity.frame.origin.y+_imgCity.frame.size.height+10, _imgCity.frame.size.width, _imgCity.frame.size.height);
            _stateTxt.frame=CGRectMake(_cityTxt.frame.origin.x+2, _cityTxt.frame.origin.y+_cityTxt.frame.size.height+15, _cityTxt.frame.size.width,_cityTxt.frame.size.height);


        }
        else
        {
        lblZipCode.frame=CGRectMake(lblComment.frame.origin.x, lblComment.frame.origin.y, lblZipCode.frame.size.width, lblZipCode.frame.size.height);
            
            _lblAddress1.frame=CGRectMake(lblZipCode.frame.origin.x, lblZipCode.frame.origin.y+49, lblZipCode.frame.size.width, lblZipCode.frame.size.height);
            
            _lblAddress2.frame=CGRectMake(_lblAddress1.frame.origin.x, _lblAddress1.frame.origin.y+46, lblZipCode.frame.size.width, lblZipCode.frame.size.height);
            
            _lblCity.frame=CGRectMake(_lblAddress2.frame.origin.x, _lblAddress2.frame.origin.y+43, lblZipCode.frame.size.width, lblZipCode.frame.size.height);
            
            _lblState.frame=CGRectMake(_lblCity.frame.origin.x, _lblCity.frame.origin.y+40, lblZipCode.frame.size.width, lblZipCode.frame.size.height);
            
            imgCommentbackground.frame= CGRectMake(imgBackGroundComment.frame.origin.x, imgBackGroundComment.frame.origin.y, imgCommentbackground.frame.size.width, imgCommentbackground.frame.size.height);
            txtZipcode.frame=CGRectMake(imgCommentbackground.frame.origin.x+2, imgCommentbackground.frame.origin.y, txtZipcode.frame.size.width,txtZipcode.frame.size.height);
            
            _imgAddress1.frame= CGRectMake(imgCommentbackground.frame.origin.x, imgCommentbackground.frame.origin.y+imgCommentbackground.frame.size.height+19, imgCommentbackground.frame.size.width, imgCommentbackground.frame.size.height);
            _Address1Txt.frame=CGRectMake(txtZipcode.frame.origin.x+2, txtZipcode.frame.origin.y+txtZipcode.frame.size.height+20, txtZipcode.frame.size.width,txtZipcode.frame.size.height);
            
            _imgAddress2.frame= CGRectMake(_imgAddress1.frame.origin.x, _imgAddress1.frame.origin.y+_imgAddress1.frame.size.height+18, _imgAddress1.frame.size.width, _imgAddress1.frame.size.height);
            _Address2Txt.frame=CGRectMake(_Address1Txt.frame.origin.x+2, _Address1Txt.frame.origin.y+_Address1Txt.frame.size.height+18, _Address1Txt.frame.size.width,_Address1Txt.frame.size.height);
            
            _imgCity.frame= CGRectMake(_imgAddress2.frame.origin.x, _imgAddress2.frame.origin.y+_imgAddress2.frame.size.height+17, _imgAddress2.frame.size.width, _imgAddress2.frame.size.height);
            _cityTxt.frame=CGRectMake(_Address2Txt.frame.origin.x+2, _Address2Txt.frame.origin.y+_Address2Txt.frame.size.height+15, _Address2Txt.frame.size.width,_Address2Txt.frame.size.height);
            
            _imgState.frame= CGRectMake(_imgCity.frame.origin.x, _imgCity.frame.origin.y+_imgCity.frame.size.height+14, _imgCity.frame.size.width, _imgCity.frame.size.height);
            _stateTxt.frame=CGRectMake(_cityTxt.frame.origin.x+2, _cityTxt.frame.origin.y+_cityTxt.frame.size.height+12, _cityTxt.frame.size.width,_cityTxt.frame.size.height);

        }
        

    }
    
    if (!isZippCoce) {
        CGFloat hi;
        CGFloat wi;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            
            _lblAddress1.frame=CGRectMake(_lblEmail.frame.origin.x, _lblEmail.frame.origin.y+55, _lblEmail.frame.size.width, _lblEmail.frame.size.height);
            
            _lblAddress2.frame=CGRectMake(_lblAddress1.frame.origin.x, _lblAddress1.frame.origin.y+55, lblZipCode.frame.size.width, lblZipCode.frame.size.height);
            
            _lblCity.frame=CGRectMake(_lblAddress2.frame.origin.x, _lblAddress2.frame.origin.y+55, lblZipCode.frame.size.width, lblZipCode.frame.size.height);
            
            _lblState.frame=CGRectMake(_lblCity.frame.origin.x, _lblCity.frame.origin.y+55, lblZipCode.frame.size.width, lblZipCode.frame.size.height);
            hi=407;
            wi=39;
            _imgAddress1.frame= CGRectMake(_imgEmail.frame.origin.x, _imgEmail.frame.origin.y+_imgEmail.frame.size.height+10, _imgEmail.frame.size.width, _imgEmail.frame.size.height);
            _Address1Txt.frame=CGRectMake(_lblEmail.frame.origin.x+120, _lblEmail.frame.origin.y+_lblEmail.frame.size.height+22, hi,wi);
            
            _imgAddress2.frame= CGRectMake(_imgAddress1.frame.origin.x, _imgAddress1.frame.origin.y+_imgAddress1.frame.size.height+10, _imgAddress1.frame.size.width, _imgAddress1.frame.size.height);
            _Address2Txt.frame=CGRectMake(_Address1Txt.frame.origin.x+2, _Address1Txt.frame.origin.y+_Address1Txt.frame.size.height+17, _Address1Txt.frame.size.width,_Address1Txt.frame.size.height);
            
            _imgCity.frame= CGRectMake(_imgAddress2.frame.origin.x, _imgAddress2.frame.origin.y+_imgAddress2.frame.size.height+10, _imgAddress2.frame.size.width, _imgAddress2.frame.size.height);
            _cityTxt.frame=CGRectMake(_Address2Txt.frame.origin.x+2, _Address2Txt.frame.origin.y+_Address2Txt.frame.size.height+15, _Address2Txt.frame.size.width,_Address2Txt.frame.size.height);
            
            _imgState.frame= CGRectMake(_imgCity.frame.origin.x, _imgCity.frame.origin.y+_imgCity.frame.size.height+10, _imgCity.frame.size.width, _imgCity.frame.size.height);
            _stateTxt.frame=CGRectMake(_cityTxt.frame.origin.x+2, _cityTxt.frame.origin.y+_cityTxt.frame.size.height+14, _cityTxt.frame.size.width,_cityTxt.frame.size.height);

        }
        else
        {
            
            _lblAddress1.frame=CGRectMake(_lblEmail.frame.origin.x, _lblEmail.frame.origin.y+49, _lblEmail.frame.size.width, _lblEmail.frame.size.height);
            
            _lblAddress2.frame=CGRectMake(_lblAddress1.frame.origin.x, _lblAddress1.frame.origin.y+46, lblZipCode.frame.size.width, lblZipCode.frame.size.height);
            
            _lblCity.frame=CGRectMake(_lblAddress2.frame.origin.x, _lblAddress2.frame.origin.y+43, lblZipCode.frame.size.width, lblZipCode.frame.size.height);
            
            _lblState.frame=CGRectMake(_lblCity.frame.origin.x, _lblCity.frame.origin.y+40, lblZipCode.frame.size.width, lblZipCode.frame.size.height);
            
            hi=133;
            wi=30;
            _imgAddress1.frame= CGRectMake(_imgEmail.frame.origin.x, _imgEmail.frame.origin.y+_imgEmail.frame.size.height+19, _imgEmail.frame.size.width, _imgEmail.frame.size.height);
            _Address1Txt.frame=CGRectMake(_lblEmail.frame.origin.x+122, _lblEmail.frame.origin.y+_lblEmail.frame.size.height+20, hi,wi);
            
            _imgAddress2.frame= CGRectMake(_imgAddress1.frame.origin.x, _imgAddress1.frame.origin.y+_imgAddress1.frame.size.height+18, _imgAddress1.frame.size.width, _imgAddress1.frame.size.height);
            _Address2Txt.frame=CGRectMake(_Address1Txt.frame.origin.x+2, _Address1Txt.frame.origin.y+_Address1Txt.frame.size.height+18, _Address1Txt.frame.size.width,_Address1Txt.frame.size.height);
            
            _imgCity.frame= CGRectMake(_imgAddress2.frame.origin.x, _imgAddress2.frame.origin.y+_imgAddress2.frame.size.height+17, _imgAddress2.frame.size.width, _imgAddress2.frame.size.height);
            _cityTxt.frame=CGRectMake(_Address2Txt.frame.origin.x+2, _Address2Txt.frame.origin.y+_Address2Txt.frame.size.height+15, _Address2Txt.frame.size.width,_Address2Txt.frame.size.height);
            
            _imgState.frame= CGRectMake(_imgCity.frame.origin.x, _imgCity.frame.origin.y+_imgCity.frame.size.height+14, _imgCity.frame.size.width, _imgCity.frame.size.height);
            _stateTxt.frame=CGRectMake(_cityTxt.frame.origin.x+2, _cityTxt.frame.origin.y+_cityTxt.frame.size.height+12, _cityTxt.frame.size.width,_cityTxt.frame.size.height);

        }
        
    }
    }
    if ((isZippCoce) && (isComment) && (isAddress)) {
        NSLog(@"ALl Three are threrr ");
    }
    else
    {
    if(isComment)
    {
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
        _addleadBtnnn.frame=CGRectMake(self.view.center.x-100, imgCommentbackground.frame.origin.y+imgCommentbackground.frame.size.height+25, 200, 45);
        }
        else
        {
            _addleadBtnnn.frame=CGRectMake(self.view.center.x-62, imgCommentbackground.frame.origin.y+imgCommentbackground.frame.size.height+25, 125, 34);
        }
    }
    if(isZippCoce)
    {
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            _addleadBtnnn.frame=CGRectMake(self.view.center.x-100, imgBackGroundComment.frame.origin.y+imgBackGroundComment.frame.size.height+70, 200, 45);
        }
        else
        {
            _addleadBtnnn.frame=CGRectMake(self.view.center.x-62, imgBackGroundComment.frame.origin.y+imgBackGroundComment.frame.size.height+50, 125, 34);
        }
    }
    if (isAddress) {
        [_scrollviews setScrollEnabled:YES];
        _scrollviews.scrollEnabled = YES;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            _addleadBtnnn.frame=CGRectMake(self.view.center.x-100, _imgState.frame.origin.y+_imgState.frame.size.height+25, 200, 45);
        }
        else
        {
            _addleadBtnnn.frame=CGRectMake(self.view.center.x-62, _imgState.frame.origin.y+_imgState.frame.size.height+25, 125, 34);
        }
    }
    }
    if(!isAddress)
    {
        [_scrollviews setScrollEnabled:NO];
        _scrollviews.scrollEnabled = NO;
    }
}
-(void)dismissKeyboard
{
    [txtComment resignFirstResponder];
}
-(void)viewWillDisappear:(BOOL)animated{
    [_helpBtnnNewcustomer setHidden:YES];
}
- (void)viewDidLoad
{
    [_helpBtnnNewcustomer setHidden:NO];
    [_scrollviews setScrollEnabled:YES];
    _scrollviews.scrollEnabled = YES;
    [_scrollviews setContentSize:CGSizeMake(300, 680)];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
       [super viewDidLoad];
    txtPhone.delegate=self;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _managedObjectContext= [appDelegate managedObjectContext];
    
    cMgr=[CacheManager getInstance];

    NSDate *date = [NSDate date];
    //Create the dateformatter object
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init] ;
    //Set the required date format
    [formatter setDateFormat:@"yyyy-MM-dd-HH-MM-SS"];
    //Get the string date
    NSString *dateSet = [formatter stringFromDate:date];
    
    
    //****RAndom number
    
    NSUInteger r = arc4random_uniform(1000000000);
    
    dateSet = [NSString stringWithFormat:@"%lu",(unsigned long)r];
    
    // NSInteger intRandom =   arc4random() %8;
    
    //******
    
   cMgr.selectedService=dateSet;
    cMgr.isAutoid=YES;
    
    NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
    
    BOOL isComment = [def boolForKey:@"AD_Commnet"];
    if (isComment) {
        lblComment.hidden=NO;
        imgBackGroundComment.hidden=NO;
        txtComment.hidden=NO;
    }
    BOOL isAddress = [def boolForKey:@"Is_Address"];
    if (isAddress) {
        _lblAddress1.hidden=NO;
        _lblAddress2.hidden=NO;
        _lblCity.hidden=NO;
        _lblState.hidden=NO;
        _Address1Txt.hidden=NO;
        _Address2Txt.hidden=NO;
        _cityTxt.hidden=NO;
        _stateTxt.hidden=NO;
        _imgAddress1.hidden=NO;
        _imgAddress2.hidden=NO;
        _imgCity.hidden=NO;
        _imgState.hidden=NO;
    }

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureAction:)];
    tapGesture.numberOfTouchesRequired=1;
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    
    [_scrollviews addGestureRecognizer:tapGesture];

	// Do any additional setup after loading the view.
//    NSString *strCompanyName=[NSString stringWithFormat:@"%@",[def valueForKey:@"Company_Name"]];
//    lblCompanyName.text=strCompanyName;
    
    if ([[UIScreen mainScreen] bounds].size.height <= 736){
        self.height.constant = 60;
    }else{
        self.height.constant = 135;
        
    }

}
-(void)gestureAction:(UITapGestureRecognizer *) sender
{
    [txtFname resignFirstResponder];
    [txtLname resignFirstResponder];
    [txtEmail  resignFirstResponder];
    [txtPhone resignFirstResponder];
    [txtComment resignFirstResponder];
    [txtZipcode resignFirstResponder];
    [_Address2Txt resignFirstResponder];
    [_Address1Txt resignFirstResponder];
    [_cityTxt resignFirstResponder];
    [_stateTxt resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)saveServiceId
{
    NSUserDefaults *boolUserDefaults = [NSUserDefaults standardUserDefaults];
    [boolUserDefaults setValue:@"" forKey:@"strIndexNew"];
    [boolUserDefaults synchronize];

    
    cMgr.isFromCustomer=YES;
    static NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    
    if ([txtFname.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert" message: @"Please Enter First Name" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else if([txtLname.text isEqualToString:@""] )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert" message: @"Please Enter Last Name" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else if ([txtPhone.text isEqualToString:@""]){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert" message: @"Please Enter Phone Number" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }
   /*
    else if([txtEmail.text isEqualToString:@""])
    {
       // UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert" message: @"Please Enter Email" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        //[alert show];
        
        
       
    }
    */
  /*  else if (![emailPredicate evaluateWithObject:txtEmail.text])
        
    {
    
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please enter a valid email address" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
       
    }
*/
    
   else if ([txtEmail.text length]>0 && ![emailPredicate evaluateWithObject:txtEmail.text]) {
        
       
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please enter a valid email address" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
    }
    
    else
        
    {
        
        if (txtComment.text.length>0) {
            NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
            
            [def setObject:txtComment.text forKey:@"acomment"];
        }
        else
        {
            NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
            
            [def setObject:@"" forKey:@"acomment"];
        }
        
        
        if (txtZipcode.text.length>0) {
            
            NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
            
            [def setObject:txtZipcode.text forKey:@"zipcode"];
            [def synchronize];
            
        }
        else
        {
            NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
            [def setObject:@"" forKey:@"zipcode"];
            [def synchronize];
        }

    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MMddyyhhmm"];
    
    cTable= (CustomerTable*)[NSEntityDescription insertNewObjectForEntityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext];

    cTable.fName=txtFname.text;
    cMgr.accountName=txtFname.text;
    cMgr.accountLName=txtLname.text;
    cTable.lName=txtLname.text;
    cTable.phNo=txtPhone.text;
        cTable.username=cMgr.strUsreName;
        if (txtEmail.text.length==0) {
            cTable.emailId=@"";
        }
        else{
    cTable.emailId=txtEmail.text;
        }
 
        if (_Address1Txt.text.length==0) {
            cTable.address1=@"";
        }
        else{
            cTable.address1=_Address1Txt.text;
        }
        if (_Address2Txt.text.length==0) {
            cTable.address2=@"";
        }
        else{
            cTable.address2=_Address2Txt.text;
        }
        if (_cityTxt.text.length==0) {
            cTable.city=@"";
        }
        else{
            cTable.city=_cityTxt.text;
        }
        if (_stateTxt.text.length==0) {
            cTable.stateeee=@"";
        }
        else{
            cTable.stateeee=_stateTxt.text;
        }

    cTable.statusCustomer=@"new";
    cTable.accountNo=@"0";
        
    NSDate *date1 = [NSDate date];
    //Create the dateformatter object
    NSDateFormatter* formatter1 = [[NSDateFormatter alloc] init] ;
    //Set the required date format
    [formatter1 setDateFormat:@"MM-dd-yyyy hh:mm:ss a"];
    //Get the string date
    NSString *dateSet1 = [formatter1 stringFromDate:date1];
        
        
        cTable.dateTimec=dateSet1;
   // NSLog(@"%@",dateSet1);
    
    cTable.autoId=cMgr.selectedService;
        
    NSError *savingError = nil;
    if ([self.managedObjectContext save:&savingError]){
       // NSLog(@"Successfully saved the context.");
        
       // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
        
        UIStoryboard *storyboard;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
        }
        else
        {
            
            storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
        }

        AddleadViewController* lgn = (AddleadViewController*)[storyboard instantiateViewControllerWithIdentifier:@"addLead1"];
          [self.navigationController  pushViewController:lgn animated:YES];
     }
    else {
       // NSLog(@"Failed to save the context. Error = %@", savingError);
    }
    }
}

-(IBAction)back{
    [self.navigationController popViewControllerAnimated:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [txtFname resignFirstResponder];
    [txtLname resignFirstResponder];
    [txtEmail  resignFirstResponder];
    [txtPhone resignFirstResponder];
    [txtComment resignFirstResponder];
    [txtZipcode resignFirstResponder];
    [_Address2Txt resignFirstResponder];
    [_Address1Txt resignFirstResponder];
    [_cityTxt resignFirstResponder];
    [_stateTxt resignFirstResponder];

     self.view.frame= CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{    UITouch *touch = [[event allTouches] anyObject];
    
    [super touchesBegan:touches withEvent:event];
    if (touch) {
        [txtFname resignFirstResponder];
        [txtLname resignFirstResponder];
        [txtEmail  resignFirstResponder];
        [txtPhone resignFirstResponder];
        [txtComment resignFirstResponder];
        [txtZipcode resignFirstResponder];
        [_Address2Txt resignFirstResponder];
        [_Address1Txt resignFirstResponder];
        [_cityTxt resignFirstResponder];
        [_stateTxt resignFirstResponder];
     }
}

/*
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{

    
    BOOL IsYes=NO;
    
    if ([textField tag]==2) {
        
     if ([textField.text length]<13) {
        
        IsYes=YES;
       // return YES;
      
        

    
    //    if ((textField.tag == 5 )||(textField.tag==6)||(textField.tag==7)) {
    //        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    //        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    //
    //        NSNumber* candidateNumber;
    //
    //        NSString* candidateString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //
    //        range = NSMakeRange(0, [candidateString length]);
    //
    //        [numberFormatter getObjectValue:&candidateNumber forString:candidateString range:&range error:nil];
    //
    //        if (([candidateString length] > 0) && (candidateNumber == nil || range.length < [candidateString length])) {
    //
    //            return NO;
    //        }
    //        else
    //        {
    //            return YES;
    //        }
    //        
    //    }
         
     }

    }
    
    
    else
        
    {
        IsYes =NO;
    }
    return IsYes;
}

*/

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
//    
//    if (textField.tag == 2) {
//        return newText.length <= 13; // only allow 13 or less characters
//    }  // only allow 1 or less characters
//     else {
//        return YES;
//    }
//}
- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }

    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= MAXLENGTH || returnKey;
}
#pragma mark-textfield delegate method


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect =[self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =[self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =(MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
   // NSLog(@"%f",denominator);
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =[[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    CGRect textFieldRect =[self.view.window convertRect:textView.bounds fromView:textView];
    CGRect viewRect =[self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y  * textFieldRect.size.height-20;
    CGFloat numerator =midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =(MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    // NSLog(@"%f",denominator);
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =[[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];


}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];

}

- (IBAction)goToiPadHelp:(id)sender {
    UIStoryboard *storyboard;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    }
    HelpViewController* lgn = (HelpViewController*)[storyboard instantiateViewControllerWithIdentifier:@"HelpViewControlleriPadNew"];
    [self.navigationController presentViewController:lgn animated:NO completion:nil];
}
@end
