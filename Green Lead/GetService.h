//
//  GetService.h
//  Green Lead
//
//  Created by Rakesh Jain on 18/09/13.
//  Copyright (c) 2013 Infocrats. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface GetService : NSManagedObject

@property (nonatomic, retain) NSString * sErvice_Id;
@property (nonatomic, retain) NSString * sErviceName;



@end
