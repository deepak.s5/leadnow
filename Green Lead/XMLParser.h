//
//  XMLParser.h
//  FFLocation
//
//  Created by Amrita S on 21/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CacheManager.h"
#import "GetService.h"

@interface XMLParser : NSObject<NSXMLParserDelegate>
{
    
   
    NSMutableString *currentElementValue;
    CacheManager *cMgr;
   
    NSString *serviceId,*serviceName;
    
    GetService *getService;
    
}

@property (nonatomic, retain) NSManagedObjectContext        *managedObjectContext;
@property (nonatomic, retain) NSFetchedResultsController    *fetchedResultsController;
@end
