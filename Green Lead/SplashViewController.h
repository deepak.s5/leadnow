//
//  SplashViewController.h
//  Lead Now
//
//  Created by Saavan Patidar on 15/05/17.
//  Copyright © 2017 Infocrats. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *splashImage;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) UIWindow *window;
@end
