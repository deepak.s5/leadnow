 //
//  AddleadViewController.m
//  Green Lead
//
//  Created by Rakesh Jain on 19/09/13.
//  Copyright (c) 2013 Infocrats. All rights reserved.
//

#import "AddleadViewController.h"
#import  <QuartzCore/QuartzCore.h>
#import "NewcustomerViewController.h"
#import "AppDelegate.h"
#import "DejalActivityView.h"
#import "DeveloperViewController.h"
#import "SelectedServiceViewController.h"
#import "HelpViewController.h"

#define DOCUMENTS_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


@interface AddleadViewController ()
{
    int check;
   // UIView *popoverViews;
   // UIView *popoverViews2;
    BOOL val,val2;
  //  UIViewController *controller;
    BOOL fromcamera;
}
@end

@implementation AddleadViewController
@synthesize txtviewComment;
@synthesize imgCapture;
@synthesize imgShow,imgShow1;
@synthesize changeImgbtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error != NULL)
    {
        // handle error
    }
    else
    {
        // handle ok status
    }
}


- (void)dismiskeyboard:(UITapGestureRecognizer*)sender {
  //  UIView *view = sender.view;
   //By tag, you can find out where you had typed.
    
    [txtviewComment resignFirstResponder];
    [tex2 resignFirstResponder];
    [tex1 resignFirstResponder];
    [tex3  resignFirstResponder];
    [tex4 resignFirstResponder];
    [tex5 resignFirstResponder];
}
-(void)refreshOnAddLead{
 lblService.text=@"Select service";
hcmLabel.text=@"Level of Urgency";
    imgShow.image=nil;
    imgShow1.image=nil;
    imgShow2.image=nil;
    imgShow3.image=nil;
}
- (void)viewDidLoad

{
    fromcamera=NO;
    // Do any additional setup after loading the view
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSString *strLevelOfUrgency=[defs valueForKey:@"AD_Default_Urgency"];
    if (strLevelOfUrgency.length>0) {
        hcmLabel.text=strLevelOfUrgency;
    }

    [super viewDidLoad];
    val=YES;
    val2=YES;
  //  pickerView.frame=CGRectMake(pickerView.frame.origin.x, pickerView.frame.origin.y, pickerView.frame.size.width+105, pickerView.frame.size.height);
   // popoverViews2= [[UIView alloc] init];
    [scrollView setContentOffset:
     CGPointMake(0, -scrollView.contentInset.top) animated:YES];
    if ((imgShow.image==nil) && (imgShow1.image==nil) && (imgShow2.image==nil) && (imgShow3.image==nil)) {
        [_afterImageCaptureNxtBtn setHidden:YES];
        [_hideNxtBtn setHidden:NO];
    }
    else if (imgShow.image==nil) {
        if ((imgShow1.image!=nil) || (imgShow2.image!=nil) || (imgShow3.image!=nil)) {
            [_afterImageCaptureNxtBtn setHidden:NO];
            [_hideNxtBtn setHidden:YES];
        } else {
            [_afterImageCaptureNxtBtn setHidden:YES];
            [_hideNxtBtn setHidden:NO];
        }
    }
    else {
        [_afterImageCaptureNxtBtn setHidden:NO];
        [_hideNxtBtn setHidden:YES];
    }
    
    
    [scrollView flashScrollIndicators];
   // txtviewComment.text = @"Description";
   // txtviewComment.textColor = [UIColor lightGrayColor];

    myArrayOfNames = [[NSMutableArray alloc]init];
    myArrayOfNames1 = [[NSMutableArray alloc]init];
    check=0;
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(refreshOnAddLead) name:@"NotificationAddLead" object:nil];
    UITapGestureRecognizer *letterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiskeyboard:)];
    letterTapRecognizer.numberOfTapsRequired = 1;
    [scrollView addGestureRecognizer:letterTapRecognizer];
//    NSURL *url = [NSURL URLWithString:@"http://myanteater.com/ServiceHandler.ashx?key=getMaxCount"];
//    
//    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
//    
//   NSOperationQueue *queue = [[NSOperationQueue alloc] init];
//    
//    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
//     
//    {
//        NSLog(@"%@", data);
//        
//        NSString *charlieSendString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//        
//        NSLog(@"%@",charlieSendString);
//    }];
//    
    
    arrayForobjects=[[NSMutableArray alloc]init];
  
    txtviewComment.delegate=self;
   [self startChangingImage];
    

    //[txtviewComment setReturnKeyType:UIReturnKeyDone];
  
   
     cMgr=[CacheManager getInstance];
  
    

    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//       
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self searchallValues];
//        });
//    });
    
     [self performSelector:@selector(searchallValues) withObject:nil afterDelay:0.5];
    
    
    
    
    
    cMgr.isYesimg=NO;
    if (cMgr.isFromaddLead) {
        
        
       // [self performSelector:@selector(startChangingImage) withObject:nil afterDelay:0.5];
        
        [self timerStarthere];
        
        [self startRecording];
        cMgr.isFromaddLead=NO;
    }
    lblAccount.text= cMgr.accountName;
    
//    UIImage  *yourImage=[UIImage imageNamed:@"login2.png"];
//      UIImageWriteToSavedPhotosAlbum(yourImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
//    [imageLayer setCornerRadius:1];
//    [imageLayer setBorderWidth:1];
//    imageLayer.borderColor=[[UIColor lightGrayColor] CGColor];
    
    
    pickerSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:nil cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
	pickerSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    
    pickerView = [[UIPickerView alloc] init];
    
    [pickerView setFrame:CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width - 17, 250)];
    pickerView.delegate =self;
    pickerView.dataSource =self;
    pickerView.tag = 1;
    
    //  € ¥ ₹ £ $
    [pickerSheet addSubview:pickerView];
    
    [pickerView setBackgroundColor:[UIColor whiteColor]];
    pickerViewforHotColdMIld = [[UIPickerView alloc] init];
    pickerViewforHotColdMIld.delegate =self;
    pickerViewforHotColdMIld.dataSource =self;
    pickerViewforHotColdMIld.tag = 2;
    
    
    //  € ¥ ₹ £ $
    [pickerSheet addSubview:pickerViewforHotColdMIld];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
	[barItems addObject:flexSpace];
	
	UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissPickerSheet:)];
	[barItems addObject:doneBtn];
    
	pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
	pickerToolbar.barStyle = UIBarStyleBlackOpaque;
	
	pickerToolbar.items = barItems;
	
	[pickerSheet addSubview:pickerToolbar];
    
    
    pickerView.hidden=TRUE;
    pickerViewforHotColdMIld.hidden=TRUE;
    
    
    
   
    objectsarray=[[NSArray alloc]initWithObjects:@"Warm",@"Cold",@"Hot",@"Service Issue", nil];
    [self addLongPressGestureToPiece:imgShow];
    
    
    
    UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(myFunction)];
    tapped.numberOfTapsRequired = 1;
    [imgShow addGestureRecognizer:tapped];
   
    
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(activateDeletionMode:)];
    longPress.delegate = (id)self;
    longPress.minimumPressDuration = 1.0;
    [imgShow setUserInteractionEnabled:YES];
    [imgShow addGestureRecognizer:longPress];
    
    UILongPressGestureRecognizer *longPress1 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(activateDeletionMode1:)];
    longPress1.delegate = (id)self;
    longPress1.minimumPressDuration = 1.0;
    [imgShow1 setUserInteractionEnabled:YES];
    [imgShow1 addGestureRecognizer:longPress1];
    
    UILongPressGestureRecognizer *longPress2 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(activateDeletionMode2:)];
    longPress2.delegate = (id)self;
    longPress2.minimumPressDuration = 1.0;
    [imgShow2 setUserInteractionEnabled:YES];
    [imgShow2 addGestureRecognizer:longPress2];
    
    UILongPressGestureRecognizer *longPress3 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(activateDeletionMode3:)];
    longPress3.delegate = (id)self;
    longPress3.minimumPressDuration = 1.0;
    [imgShow3 setUserInteractionEnabled:YES];
    [imgShow3 addGestureRecognizer:longPress3];
    
    if ([[UIScreen mainScreen] bounds].size.height == 480){
        [scrollView setFrame:CGRectMake(scrollView.frame.origin.x, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height)];
        [scrollView setContentSize:CGSizeMake(320, 590)];
    }
    
    [txtviewComment.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [txtviewComment.layer setBorderWidth:1.0];
    txtviewComment.layer.cornerRadius = 5;
    txtviewComment.clipsToBounds = YES;
    
    if ([[UIScreen mainScreen] bounds].size.height <= 736){
        self.height.constant = 60;
    }else{
        self.height.constant = 135;
    }
    
}
- (void)activateDeletionMode:(UILongPressGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateBegan)
    {
        imgShow.image =nil;
        if (imgShow.image==nil) {
            if ((imgShow1.image!=nil) || (imgShow2.image!=nil) || (imgShow3.image!=nil)) {
                [_afterImageCaptureNxtBtn setHidden:NO];
                [_hideNxtBtn setHidden:YES];
            } else {
                [_afterImageCaptureNxtBtn setHidden:YES];
                [_hideNxtBtn setHidden:NO];
            }
        }
        else {
            [_afterImageCaptureNxtBtn setHidden:NO];
            [_hideNxtBtn setHidden:YES];
        }
      //  [imgShow setHidden:YES];
    }
}
- (void)activateDeletionMode1:(UILongPressGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateBegan)
    {
        datesetg1=@"";
        imgShow1.image =nil;
        if (imgShow1.image==nil) {
            if ((imgShow.image!=nil) || (imgShow2.image!=nil) || (imgShow3.image!=nil)) {
                [_afterImageCaptureNxtBtn setHidden:NO];
                [_hideNxtBtn setHidden:YES];
            } else {
                [_afterImageCaptureNxtBtn setHidden:YES];
                [_hideNxtBtn setHidden:NO];
            }
        }
        else {
            [_afterImageCaptureNxtBtn setHidden:NO];
            [_hideNxtBtn setHidden:YES];
        }
      //  [imgShow1 setHidden:YES];
    }
}
- (void)activateDeletionMode2:(UILongPressGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateBegan)
    {
        datesetg2=@"";
        imgShow2.image =nil;
        if (imgShow2.image==nil) {
            if ((imgShow1.image!=nil) || (imgShow.image!=nil) || (imgShow3.image!=nil)) {
                [_afterImageCaptureNxtBtn setHidden:NO];
                [_hideNxtBtn setHidden:YES];
            } else {
                [_afterImageCaptureNxtBtn setHidden:YES];
                [_hideNxtBtn setHidden:NO];
            }
        }
        else {
            [_afterImageCaptureNxtBtn setHidden:NO];
            [_hideNxtBtn setHidden:YES];
        }
      //  [imgShow2 setHidden:YES];
    }
}
- (void)activateDeletionMode3:(UILongPressGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateBegan)
    {
        datesetg3=@"";
        imgShow3.image =nil;
        if (imgShow3.image==nil) {
            if ((imgShow1.image!=nil) || (imgShow2.image!=nil) || (imgShow.image!=nil)) {
                [_afterImageCaptureNxtBtn setHidden:NO];
                [_hideNxtBtn setHidden:YES];
            } else {
                [_afterImageCaptureNxtBtn setHidden:YES];
                [_hideNxtBtn setHidden:NO];
            }
        }
        else {
            [_afterImageCaptureNxtBtn setHidden:NO];
            [_hideNxtBtn setHidden:YES];
        }
     //   [imgShow3 setHidden:YES];
    }
}

-(void)timerStarthere

{
    totalsecond=60;
  
    lblTimer.text=[NSString stringWithFormat:@"%d",totalsecond];
    twoMinTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                   target:self
                                                 selector:@selector(timer)
                                                 userInfo:nil
                                                  repeats:YES];
}

- (void)timer {
     totalsecond--;
   lblTimer.text =[NSString stringWithFormat:@"%d",totalsecond];
    if (  totalsecond == 0 ) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [twoMinTimer invalidate];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Your audio recorded successfully !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)myFunction
{
  //  NSLog(@"Remove");

}

- (void)addLongPressGestureToPiece:(UIView *)piece
{
   // NSLog(@"addLongPressGestureToPiece");
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(RemoveImage:)];
    [longPressGesture setDelegate:self];
    [piece addGestureRecognizer:longPressGesture];
}

-(void)RemoveImage:(UILongPressGestureRecognizer*)pGesture{
    imgShow.image=nil;
    dateSetg=@"";
}
-(void)searchallValues
{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _managedObjectContext= [appDelegate managedObjectContext];
    
    
    entityDesc1=[NSEntityDescription entityForName:@"GetService" inManagedObjectContext:_managedObjectContext];
    
    request=[[NSFetchRequest alloc
              ]init];
    //
    
    request1=[[NSFetchRequest alloc
               ]init];
    
    
    // [request setEntity:entityDesc];
    
    [request1 setEntity:entityDesc1];
    
    
    
    
    NSError *error;
  
    //    objects = [_managedObjectContext executeFetchRequest:request error:&error];
    //
    objects = [_managedObjectContext executeFetchRequest:request1 error:&error];

   // NSLog(@"%d",[objects count]);
    if ([objects count] == 0) {
        
//        //    //new Change just to test
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"No Services Downloaded" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil , nil];
//        [alert show];
//        //    //end new change just to test
        
        
    } else {
        
//        //    //new Change just to test
//        NSArray *arr = [objects valueForKey:@"sErviceName"];
//        NSString *strMsg = [NSString stringWithFormat:@"%@",arr];
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Services" message:strMsg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil , nil];
//        [alert show];
//        //    //end new change just to test

        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        _managedObjectContext= [appDelegate managedObjectContext];
        
        
        entityDesc1=[NSEntityDescription entityForName:@"Checkversion" inManagedObjectContext:_managedObjectContext];
        
        request=[[NSFetchRequest alloc
                  ]init];
        //
        
        request1=[[NSFetchRequest alloc
                   ]init];
        
        
        // [request setEntity:entityDesc];
        
        [request1 setEntity:entityDesc1];
        
        
        
        
        NSError *error;
        
        //    objects = [_managedObjectContext executeFetchRequest:request error:&error];
        //
        objVersion = [_managedObjectContext executeFetchRequest:request1 error:&error];
        
        //NSLog(@"%d",[objVersion count]);
        
        
        
        if ([objVersion count]>0)
        
        {
            
            
            NSString *obj=[[objVersion objectAtIndex:0]sversion];
            
            
            int k=[obj intValue];
         
            
            
            int y=[cMgr.versionCache intValue];
            
         //   NSLog(@"%d",y);
         //   NSLog(@"%d",k);
            if (y==k) {
                
                
            
            }
            
            else
            {
               // [DejalBezelActivityView activityViewForView:self.view];
                
             [[NSNotificationCenter defaultCenter] postNotificationName:@"checkversionData" object:nil];
                
                AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                _managedObjectContext= [appdelegate managedObjectContext];
                
                
                entityDesc1=[NSEntityDescription entityForName:@"GetService" inManagedObjectContext:_managedObjectContext];
                
                request=[[NSFetchRequest alloc
                          ]init];
                //
                
                request1=[[NSFetchRequest alloc
                           ]init];
                
                
                // [request setEntity:entityDesc];
                
                [request1 setEntity:entityDesc1];
                  NSError *error;
                
                //    objects = [_managedObjectContext executeFetchRequest:request error:&error];
                //
                objects = [_managedObjectContext executeFetchRequest:request1 error:&error];
                
              //  NSLog(@"%d",[objects count]);
            
            
            }
            
        }
        //        for(int i=0; i<[objects count]; i++)
        //        {
        ////            matches = [objects objectAtIndex:i];
        ////            cMgr.cacheEmpid =  [matches valueForKey:@"eEmployeeId"];
        ////
        ////
        ////            NSLog(@"%@",cMgr.cacheEmpid);
        //        }
        //
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)back
{
    NSUserDefaults *boolUserDefaults = [NSUserDefaults standardUserDefaults];
    BOOL CheckisFromUpdate = [boolUserDefaults boolForKey:@"isFromUpdate"];
    BOOL CheckisFromViewMore = [boolUserDefaults boolForKey:@"isFromAddMoreViewNew"];
    if (CheckisFromUpdate) {
        NSUserDefaults *boolUserDefaults = [NSUserDefaults standardUserDefaults];
        [boolUserDefaults setBool:NO forKey:@"isFromUpdate"];
        [boolUserDefaults synchronize];
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
            SelectedServiceViewController* verificationBadgesViewController = (SelectedServiceViewController*)[storyboard instantiateViewControllerWithIdentifier:@"select"];
            
            
            [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
        }
        else
        {
        SelectedServiceViewController *wc = [[UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"select"];
        [self.navigationController pushViewController:wc animated:YES];
        }
    }
    else if (CheckisFromViewMore)
    {
        NSUserDefaults *boolUserDefaults = [NSUserDefaults standardUserDefaults];
        [boolUserDefaults setBool:NO forKey:@"isFromAddMoreViewNew"];
        [boolUserDefaults synchronize];
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
            SelectedServiceViewController* verificationBadgesViewController = (SelectedServiceViewController*)[storyboard instantiateViewControllerWithIdentifier:@"select"];
            
            
            [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
        }
        else
        {
            SelectedServiceViewController *wc = [[UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"select"];
            [self.navigationController pushViewController:wc animated:YES];
        }
    }
    else
    {
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        _managedObjectContext= [appDelegate managedObjectContext];
        entityDesc2=[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext];
        request2=[[NSFetchRequest alloc
                  ]init];
        [request2 setEntity:entityDesc2];
        NSError *error;
        objects1 = [_managedObjectContext executeFetchRequest:request2 error:&error];
        if ([objects1 count] == 0) {
        }
        else
        {
            [self deleteCustomerTable];
        }
        entityDesc3=[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext];
        request3=[[NSFetchRequest alloc
                   ]init];
        [request3 setEntity:entityDesc3];
        NSError *error1;
        objects2 = [_managedObjectContext executeFetchRequest:request3 error:&error1];
        if ([objects2 count] == 0) {
        }
        else
        {
            [self deleteServiceDetailTable];
        }
        
        UIStoryboard *storyboard;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
        }
        else
        {
            
            storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
        }
    DeveloperViewController *wc = [storyboard instantiateViewControllerWithIdentifier:@"dev"];
        [self.navigationController popViewControllerAnimated:NO];
        
  // [self.navigationController pushViewController:wc animated:YES];
        
//        int temp = 0;
//
//        for (int i= 0 ; i < [[self.navigationController viewControllers]count] ; i++) {
//            if ( [[[self.navigationController viewControllers] objectAtIndex:i] isKindOfClass:[DeveloperViewController class]]) {
//                temp = i;
//            }
//        }
//        [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:temp] animated:NO];

    cMgr.accountName=nil;
    NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
    [def setObject:nil forKey:@"zipcode"];
      
        // Upadted by navin For back manage
        [def setValue:@"Addlead" forKey:@"backStatus"];
        [def synchronize];
        [self deleteImagePathTable];
        [self deleteAudioPathTable];
    }
}

-(void)deleteCustomerTable
{
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [_managedObjectContext executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [_managedObjectContext deleteObject:data];
    }
    NSError *saveError = nil;
    [_managedObjectContext save:&saveError];
}

-(void)deleteServiceDetailTable
{
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [_managedObjectContext executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [_managedObjectContext deleteObject:data];
    }
    NSError *saveError = nil;
    [_managedObjectContext save:&saveError];
}

-(void)deleteImagePathTable
{
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"Imgpath" inManagedObjectContext:_managedObjectContext]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [_managedObjectContext executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [_managedObjectContext deleteObject:data];
    }
    NSError *saveError = nil;
    [_managedObjectContext save:&saveError];
}

-(void)deleteAudioPathTable
{
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"Audiopath" inManagedObjectContext:_managedObjectContext]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [_managedObjectContext executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [_managedObjectContext deleteObject:data];
    }
    NSError *saveError = nil;
    [_managedObjectContext save:&saveError];
}

-(IBAction)backforRecording
{
    [twoMinTimer invalidate];
    
	[recorder stop];
    
    recorder = nil;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Your audio recorded successfully !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [alert show];
  [self dismissViewControllerAnimated:YES completion:nil];
  //  [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark-textfield delegate method

- (BOOL)textViewShouldReturn:(UITextView *)textView
{
    [textView resignFirstResponder];
    [txtviewComment resignFirstResponder];

    return YES;
}
-(void) textViewDidChange:(UITextView *)textView
{
//    if(txtviewComment.text.length == 0){
//        [txtviewComment resignFirstResponder];
//    }
//    if ([txtviewComment.textColor isEqual:[UIColor lightGrayColor]])
//    {
//        txtviewComment.text = @"";
//        txtviewComment.textColor =[UIColor blackColor];
//    }
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
//    if ([txtviewComment.textColor isEqual:[UIColor lightGrayColor]])
//    {
//        txtviewComment.text = @"";
//        txtviewComment.textColor =[UIColor blackColor];
//    }

    if ([text hasSuffix:@"\n"]) {
        NSLog(@"Return pressed");
        [txtviewComment resignFirstResponder];
        if(txtviewComment.text.length == 0){
            //            _helpMsg.textColor = [UIColor blackColor];
            //            _helpMsg.text = @"Help Me ! My Location";
            [txtviewComment resignFirstResponder];
        }
        return NO;
    }
    return YES;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    [super touchesBegan:touches withEvent:event];
   // if (touch) {
        [txtviewComment resignFirstResponder];
        [tex2 resignFirstResponder];
        [tex1 resignFirstResponder];
        [tex3  resignFirstResponder];
        [tex4 resignFirstResponder];
        [tex5 resignFirstResponder];
   // }
}

-(IBAction)chkHcm:(UIButton*)button{
    //NSLog(@"Button clicked! %@", button);
    leadcircle=@"Hot";
    checkmarkImage  = [UIImage imageNamed:@"CircleGov.png"];
    
    //NSLog(@"Imageview: %d", button.imageView.tag);
    if (button.tag == 0){
        chkh.image=checkmarkImage;
        leadcircle=@"hot";
        chkc.image=nil;
        chkm.image=nil;
        
         chckCustomer.image=nil;
    }
    else if(button.tag==1)
        
    {
        chkc.image=checkmarkImage;
        leadcircle=@"cold";
        chkh.image=nil;
        chkm.image=nil;
         chckCustomer.image=nil;
    }
  else if(button.tag==2)
    {
        chkm.image=checkmarkImage;
        leadcircle=@"Mild";
        chkh.image=nil;
        chkc.image=nil;
        chckCustomer.image=nil;
        
    }
  else
  {
      chckCustomer.image=checkmarkImage;
      leadcircle=@"Concern";
      chkh.image=nil;
      chkc.image=nil;
      chkm.image=nil;
      
            
  }
}
-(IBAction)showvalueondropdown
{
    
    if (objects.count==0) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Services have not been added. Please login to www.myleadnow.com/login/" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    } else {
    
        BOOL yesFound;
        yesFound=NO;
        for (int k=0; k<objects.count; k++) {
            
            if ([[[objects objectAtIndex:k]sErviceName] isEqualToString:lblService.text]) {
                
                yesFound=YES;
                [pickerView selectRow:k inComponent:0 animated:NO];
                lblService.text=[[objects objectAtIndex:k]sErviceName];

            }
        }
        
        if (yesFound) {
            
            
            
        } else {
            
            [pickerView selectRow:0 inComponent:0 animated:NO];
            lblService.text=[[objects objectAtIndex:0]sErviceName];

        }
        
        
    if(UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPad)
    {
        //********* iPad************//
        
       UIViewController *controller = [[UIViewController alloc] init];
        UIView *popoverViews = [[UIView alloc] init];   //view
        popoverViews.backgroundColor = [UIColor clearColor];
        controller.view = popoverViews;
        // pickerView.showsSelectionIndicator=true;
        pickerView.hidden = NO;
        // ** Nilind ** //
    
     if(val)
     {
        
         pickerView.frame=CGRectMake(pickerView.frame.origin.x, pickerView.frame.origin.y, UIScreen.mainScreen.bounds.size.width, pickerView.frame.size.height);
         val=NO;
     }
        // ** Nilind ** //
        [popoverViews addSubview:pickerView];
        popOverView = [[UIPopoverController alloc] initWithContentViewController:controller];
       
               // *****Nilind ****** //
        
//        UIView *toolView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 305.0f, 44.f)];
//        toolView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blank-headerNew.png"]];
//        CGRect buttonFrame = CGRectMake(220, 5, 70, 30);
//        UIButton *selButton = [[UIButton alloc] initWithFrame:buttonFrame];
//        
//        [selButton setTitle:@"Done" forState:UIControlStateNormal];
//        [selButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [selButton addTarget:self
//                      action:@selector(dismissActionSheet)
//            forControlEvents: UIControlEventTouchDown];
//        
//        [toolView addSubview:selButton];
        // *** Nilind ******//
        
        
        
        //*************************//
        
        if([popOverView isPopoverVisible])
        {
            [popOverView dismissPopoverAnimated:YES];
        }
        else
        {
            CGRect popRect = CGRectMake(btnSelectService.frame.origin.x, btnSelectService.frame.origin.y+60, btnSelectService.frame.size.width, btnSelectService.frame.size.height);//60
        
  
           //[popOverView setPopoverContentSize:CGSizeMake(txtviewComment.frame.size.width+20, txtviewComment.frame.size.height+13) animated:YES];//320 200
             [popOverView setPopoverContentSize:CGSizeMake(UIScreen.mainScreen.bounds.size.width,200) animated:YES];
            
            [popOverView presentPopoverFromRect:popRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
         
            
            // *****Nilind ****** //
           
                    UIView *toolView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, 50)];
                   toolView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blank-headerNew.png"]];
                   // toolView.backgroundColor=[UIColor redColor];
                    CGRect buttonFrame = CGRectMake(20, 13, 70, 30);//220+90
                    UIButton *selButton = [[UIButton alloc] initWithFrame:buttonFrame];
                   //[selButton setBackgroundColor:[UIColor blueColor]];
                    selButton.titleLabel.font=[UIFont systemFontOfSize:22];
                    [selButton setTitle:@"Done" forState:UIControlStateNormal];
                    [selButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    [selButton addTarget:self
                                  action:@selector(dismissActionSheet)
                        forControlEvents: UIControlEventTouchDown];
                    
                    [toolView addSubview:selButton];
            [popoverViews addSubview:toolView];
           // popoverViews2=popoverViews;
            [selButton addTarget:self
                          action:@selector(dismissActionSheet)
                forControlEvents: UIControlEventTouchDown];
        
            // *** Nilind ******//
            
           

        }
    }
    else
    {

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        
        pickerView.hidden = FALSE;
        pickerViewforHotColdMIld.hidden = TRUE;
        pickerView.showsSelectionIndicator=true;

    alertController = [UIAlertController alertControllerWithTitle:nil
                                                          message:@"\n\n\n\n\n\n\n\n\n\n"
                                                   preferredStyle:UIAlertControllerStyleActionSheet];
    UIView *toolView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width -17, 44.f)];
    toolView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blank-headerNew.png"]];
    CGRect buttonFrame = CGRectMake(20, 5, 70, 30);
    UIButton *selButton = [[UIButton alloc] initWithFrame:buttonFrame];
   
    [selButton setTitle:@"Done" forState:UIControlStateNormal];
        [selButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [selButton addTarget:self
                  action:@selector(dismissActionSheet)
        forControlEvents: UIControlEventTouchDown];
    
    [toolView addSubview:selButton];
    [alertController.view addSubview:pickerView];
    [alertController.view addSubview:toolView];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    }
    else
    {
    pickerView.hidden = FALSE;
     pickerViewforHotColdMIld.hidden = TRUE;
        [pickerView setBackgroundColor:[UIColor whiteColor]];
    pickerView.showsSelectionIndicator=true;
    [pickerSheet showFromRect:[pickerView bounds] inView:self.tabBarController.tabBar animated:YES];
    
    [pickerSheet setBounds:CGRectMake(0, 0, 320, 500)];
         pickerSheet.backgroundColor=[UIColor whiteColor];
    [pickerView becomeFirstResponder];
    }
}
    }
}
-(void)dismissActionSheet
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }
   if(UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPad)
   {
         //[alertController dismissViewControllerAnimated:YES completion:nil];
      // [popoverViews setHidden:YES];
      // [popoverViews2 dismissPopoverAnimated:YES];
       //[popoverViews2 setHidden:YES];
    //   [controller setHidesBottomBarWhenPushed:YES];
     //  [pickerView setHidden:YES];
        [popOverView dismissPopoverAnimated:YES];
       
   }
    
  
}
-(IBAction)showvalueondropdownHot
{
   // hcmLabel.text=[objectsarray objectAtIndex:0];
    
    BOOL yesFound;
    yesFound=NO;
    for (int k=0; k<objectsarray.count; k++) {
        
        if ([[objectsarray objectAtIndex:k] isEqualToString:hcmLabel.text]) {
            
            yesFound=YES;
            [pickerViewforHotColdMIld selectRow:k inComponent:0 animated:NO];
            hcmLabel.text=[objectsarray objectAtIndex:k];
            
        }
    }
    
    if (yesFound) {
        
        
        
    } else {
        
        [pickerViewforHotColdMIld selectRow:0 inComponent:0 animated:NO];
        hcmLabel.text=[objectsarray objectAtIndex:0];
        
    }

    if(UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPad)
    {
         //*********iPad************//
        UIViewController *controller = [[UIViewController alloc] init];
        UIView *popoverViews = [[UIView alloc] init];   //view
        popoverViews.backgroundColor = [UIColor clearColor];
        controller.view = popoverViews;
        
        pickerView.hidden = TRUE;
        pickerViewforHotColdMIld.hidden = FALSE;
        pickerViewforHotColdMIld.showsSelectionIndicator=true;
        // ** Nilind ** //
       // pickerView.frame=CGRectMake(pickerView.frame.origin.x, pickerView.frame.origin.y, pickerView.frame.size.width+105, pickerView.frame.size.height);
        if(val2)
        {
            
            pickerViewforHotColdMIld.frame=CGRectMake(pickerViewforHotColdMIld.frame.origin.x, pickerViewforHotColdMIld.frame.origin.y, pickerViewforHotColdMIld.frame.size.width+105, pickerViewforHotColdMIld.frame.size.height);
            val2=NO;
        }

        // ** Nilind ** //
        
        [popoverViews addSubview:pickerViewforHotColdMIld];
        popOverView = [[UIPopoverController alloc] initWithContentViewController:controller];
        
        //*************************//
        
        if([popOverView isPopoverVisible])
        {
            [popOverView dismissPopoverAnimated:YES];
        }
        else
        {
        CGRect popRect = CGRectMake(btnLevelOfUrgency.frame.origin.x, btnLevelOfUrgency.frame.origin.y+60, btnLevelOfUrgency.frame.size.width, btnLevelOfUrgency.frame.size.height);
            [popOverView setPopoverContentSize:CGSizeMake(406,180) animated:YES];
            // [popOverView setPopoverContentSize:CGSizeMake(320, 200) animated:YES];
           // [popOverView setPopoverContentSize:CGSizeMake(420, 200) animated:YES];//320 200
             [popOverView presentPopoverFromRect:popRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            
            // ** Nilind ** //
//            CGRect popRect = CGRectMake(btnSelectService.frame.origin.x, btnSelectService.frame.origin.y+60, btnSelectService.frame.size.width, btnSelectService.frame.size.height);//60
//            
//            [popOverView setPopoverContentSize:CGSizeMake(txtviewComment.frame.size.width+20, txtviewComment.frame.size.height+13) animated:YES];//320 200
//            
//            [popOverView presentPopoverFromRect:popRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
//            
//           
            
            UIView *toolView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, txtviewComment.frame.size.width+20+50, 44.f)];
            toolView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blank-headerNew.png"]];
            // toolView.backgroundColor=[UIColor redColor];
            CGRect buttonFrame = CGRectMake(txtviewComment.frame.size.width-60, 5, 70, 30);
            UIButton *selButton = [[UIButton alloc] initWithFrame:buttonFrame];
            //[selButton setBackgroundColor:[UIColor blueColor]];
            selButton.titleLabel.font=[UIFont systemFontOfSize:22];
            [selButton setTitle:@"Done" forState:UIControlStateNormal];
            [selButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [selButton addTarget:self
                          action:@selector(dismissActionSheet)
                forControlEvents: UIControlEventTouchDown];
            
            [toolView addSubview:selButton];
            [popoverViews addSubview:toolView];

            [selButton addTarget:self
                          action:@selector(dismissActionSheet)
                forControlEvents: UIControlEventTouchDown];
    
            
            // ** Nilind ** //
        }
    }
    else
    {
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        pickerView.hidden = TRUE;
        pickerViewforHotColdMIld.hidden = FALSE;
        pickerView.showsSelectionIndicator=true;
        alertController = [UIAlertController alertControllerWithTitle:nil
                                                              message:@"\n\n\n\n\n\n\n\n\n\n"
                                                       preferredStyle:UIAlertControllerStyleActionSheet];
        //pickerView.frame=CGRectMake(0, 0, 120, 30);
        UIView *toolView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width -17, 44.f)];
        toolView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blank-headerNew.png"]];
        CGRect buttonFrame = CGRectMake(20, 5, 70, 30);
        UIButton *selButton = [[UIButton alloc] initWithFrame:buttonFrame];
        //[selButton setBackgroundImage:[UIImage imageNamed:@"red_bgnew.png"] forState:UIControlStateNormal];
        [selButton setTitle:@"Done" forState:UIControlStateNormal];
        [selButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [selButton addTarget: self
                      action: @selector(dismissActionSheet)
            forControlEvents: UIControlEventTouchDown];
        [toolView addSubview:selButton];
        [alertController.view addSubview:pickerViewforHotColdMIld];
        [alertController.view addSubview:toolView];
        [self presentViewController:alertController animated:YES completion:nil];
    }

    else
    {
    pickerView.hidden = TRUE;
     pickerViewforHotColdMIld.hidden = FALSE;
    pickerViewforHotColdMIld.showsSelectionIndicator=true;
    [pickerSheet showFromRect:[pickerViewforHotColdMIld bounds] inView:self.tabBarController.tabBar animated:YES];
    
    //[pickerSheet showFromTabBar:self.tabBarController.tabBar];
    [pickerSheet setBounds:CGRectMake(0, 0, 320, 500)];
        [pickerView setBackgroundColor:[UIColor whiteColor]];
        pickerSheet.backgroundColor=[UIColor whiteColor];
    [pickerViewforHotColdMIld becomeFirstResponder];
    }
}

}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerViews numberOfRowsInComponent:(NSInteger)component {
    int i;
    
    if (pickerViews.tag==1) {
        i=(int)[objects count];
    }
    else
    {
    
        i=(int)[objectsarray count];
        
    }
    return i;
}



//    return [lisstOfAge count];

- (UIView *)pickerView:(UIPickerView *)pickerViews viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    
    UILabel *label =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 27)];
      label.backgroundColor = [UIColor clearColor];
    label.font=[UIFont systemFontOfSize:20];
    
     if (pickerViews.tag==1) {
         
    
        label.text = [[objects objectAtIndex:row]sErviceName];
        label.textAlignment = NSTextAlignmentCenter;
         
         //Nilind//
        // label.text = [objectsarray objectAtIndex:row];
        // label.textAlignment = NSTextAlignmentCenter;

         //Nilind//
     
         
    }
    
    else
   {
        
       label.text = [objectsarray objectAtIndex:row];
       label.textAlignment = NSTextAlignmentCenter;
    
    }
    return label;
}

-(void)pickerView:(UIPickerView *)pickerViews didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerViews.tag==1) {
    lblService.text=[[objects objectAtIndex:row]sErviceName];
        lservice_id=[[objects objectAtIndex:row]sErvice_Id];
        
        //Nilind//
       // hcmLabel.text=[objectsarray objectAtIndex:row];
        //Nilind//
  }
    else
        
    {
       hcmLabel.text=[objectsarray objectAtIndex:row];
    }

}

- (void)dismissPickerSheet:(id)sender {
    
	[pickerSheet dismissWithClickedButtonIndex:0 animated:YES];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
   // imgShow2.image=nil;
   //imgShow3.image=nil;
}
-(IBAction)addleadValue
{
    cMgr.accountName=txtaccountNumber.text;
}
-(IBAction)saveServiceId
{
    if ([lblService.text isEqualToString:@"Select service"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert" message: @"Please Select service" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
   else if ([hcmLabel.text isEqualToString:@"Level of Urgency"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert" message: @"Please select Level of Urgency" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
   else if ([lblService.text isEqualToString:@""]) {
       UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert" message: @"Please Select service" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
       [alert show];
   }
   else if ([hcmLabel.text isEqualToString:@""]) {
       UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert" message: @"Please select Level of Urgency" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
       [alert show];
   }
   else if (lblService.text.length==0) {
       UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert" message: @"Please Select service" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
       [alert show];
   }
   else if (hcmLabel.text.length==0) {
       UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert" message: @"Please select Level of Urgency" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
       [alert show];
   }
    else
    {
    NSDate *date = [NSDate date];
    //Create the dateformatter object
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init] ;
    //Set the required date format
    [formatter setDateFormat:@"yyyy-MM-dd-HH-MM-SS"];
    //Get the string date
    NSString *dateSet = [formatter stringFromDate:date];
    NSUInteger r = arc4random_uniform(1000000000);
    dateSet = [NSString stringWithFormat:@"%lu",(unsigned long)r];
    if ([iDToUpdate length]==0) {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MMddyyhhmm"];

    serviceDetail= (ServiceDetailTable*)[NSEntityDescription insertNewObjectForEntityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext];
    imgpathsub= (Imgpath*)[NSEntityDescription insertNewObjectForEntityForName:@"Imgpath" inManagedObjectContext:_managedObjectContext];
        if (imgShow.image==nil)
        {
            dateSetg=@"";
            savedImagePath=@"";
        }
        if (imgShow1.image==nil)
        {
            datesetg1=@"";
            savedImagePath1=@"";
        }
        if (imgShow2.image==nil)
        {
            datesetg2=@"";
            savedImagePath2=@"";
        }
        if (imgShow3.image==nil)
        {
            datesetg3=@"";
            savedImagePath3=@"";
        }
        imgpathsub.imgNameimg=dateSetg;
        imgpathsub.imgNameimg1=datesetg1;
        imgpathsub.imgNameimg2=datesetg2;
        imgpathsub.imgNameimg3=datesetg3;
        imgpathsub.pathImg=savedImagePath;
        imgpathsub.pathImg1=savedImagePath1;
        imgpathsub.pathImg2=savedImagePath2;
        imgpathsub.pathImg3=savedImagePath3;
        imgpathsub.autoidimg=cMgr.selectedService;
        imgpathsub.iD=dateSet;//dateSetg;
        imgpathsub.username=cMgr.strUsreName;
        NSError *savingError = nil;
        if ([self.managedObjectContext save:&savingError]){
        }

    serviceDetail.serviceName=lblService.text;
        
        if ([txtviewComment.text isEqualToString:@"Description"]) {
            txtviewComment.text=@"";
        }
        serviceDetail.statuso = @"true";

    serviceDetail.comment=txtviewComment.text;
    serviceDetail.urgencyLevel=hcmLabel.text;
    serviceDetail.username=cMgr.strUsreName;
        if ([FinalImageName isEqualToString:@""]) {
            FinalImageName=@"";
        }
        else if ([FinalImageName isEqualToString:@"null"])
        {
        FinalImageName=@"";
        }
        
        FinalImageName=@"";
        if (dateSetg.length>1) {
            FinalImageName=[NSString stringWithFormat:@"%@.png",dateSetg];
        }
        else{
            FinalImageName=@"";
        }
        if (datesetg1.length>1){
            if (FinalImageName.length==0) {
                FinalImageName=[NSString stringWithFormat:@"%@.png",datesetg1];
            } else {
            FinalImageName=[NSString stringWithFormat:@"%@,%@.png",FinalImageName,datesetg1];
            }
        }
        if (datesetg2.length>1){
            if (FinalImageName.length==0) {
                FinalImageName=[NSString stringWithFormat:@"%@.png",datesetg2];
            } else {
            FinalImageName=[NSString stringWithFormat:@"%@,%@.png",FinalImageName,datesetg2];
            }
        }
        if (datesetg3.length>1){
            if (FinalImageName.length==0) {
                FinalImageName=[NSString stringWithFormat:@"%@.png",datesetg3];
            } else {
            FinalImageName=[NSString stringWithFormat:@"%@,%@.png",FinalImageName,datesetg3];
            }
        }

    serviceDetail.imgName= FinalImageName;
        if ([cMgr.cacheAudioname isEqualToString:@""]) {
            cMgr.cacheAudioname=@"";
        }
        else if ([cMgr.cacheAudioname isEqualToString:@"null"])
        {
          cMgr.cacheAudioname=@"";
        }      
          serviceDetail.audioName=cMgr.cacheAudioname;
         cMgr.cacheAudioname=@"";
          serviceDetail.serviceId=lservice_id;
          serviceDetail.iD=dateSet;
         serviceDetail.savedImagePath=savedImagePath;
         serviceDetail.audioPath=cMgr.cacheaudioPath;
    if (cMgr.isFromCustomer)
    {
           serviceDetail.status1=@"new";
           serviceDetail.accountNo=@"0";
           serviceDetail.cName=lblAccount.text;
           serviceDetail.autoId=cMgr.selectedService;
          cMgr.isFromCustomer=NO;
    }
    else
    {
        if ([lblAccount.text intValue]==0) {
            serviceDetail.status1=@"new";
            serviceDetail.accountNo=@"0";
            serviceDetail.cName=lblAccount.text;
            serviceDetail.autoId=cMgr.selectedService;
            cMgr.isFromCustomer=NO;
                   }
        else
        {
           serviceDetail.status1=@"old";
            serviceDetail.accountNo=lblAccount.text;
            serviceDetail.autoId=cMgr.selectedService;
        }
    }
    NSDate *date1 = [NSDate date];
    //Create the dateformatter object
    NSDateFormatter* formatter1 = [[NSDateFormatter alloc] init] ;
    //Set the required date format
    [formatter1 setDateFormat:@"MM-dd-yyyy hh:mm:ss a"];
    //Get the string date
    NSString *dateSet1 = [formatter1 stringFromDate:date1];
    
      serviceDetail.dateTime=dateSet1;
      //  NSLog(@"%@",dateSet1);
   
    
    if ([self.managedObjectContext save:&savingError]){
      //  NSLog(@"Successfully saved the context.");
        [txtviewComment resignFirstResponder];
        txtviewComment.text = @"";
        //imgShow.image = nil;
        //imgShow1.image = nil;
        //imgShow3.image=nil;
       // imgShow2.image=nil;
        //lblService.text=@"Select service";
        //hcmLabel.text=@"Level of Urgency";
        cMgr.isYesimg = NO;
        [pickerView reloadAllComponents];
        [pickerViewforHotColdMIld reloadAllComponents];
        
        NSLog(@"selected service is---%@",cMgr.selectedService);
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
            SelectedServiceViewController* verificationBadgesViewController = (SelectedServiceViewController*)[storyboard instantiateViewControllerWithIdentifier:@"select"];
            
            
            [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
        }
        else
        {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
        SelectedServiceViewController* verificationBadgesViewController = (SelectedServiceViewController*)[storyboard instantiateViewControllerWithIdentifier:@"select"];
        
         [self.navigationController pushViewController:verificationBadgesViewController animated:NO];
        }
    }
    else
    {
    }
        iDToUpdate=nil;
        dateSetg=nil;
        datesetg1=nil;
        datesetg2=nil;
        datesetg3=nil;
        savedImagePath=nil;
        savedImagePath1=nil;
        savedImagePath2=nil;
        savedImagePath3=nil;
}
    else
    {
        NSFetchRequest *request2=[[NSFetchRequest alloc] init];
        NSEntityDescription *entDescriptio=[NSEntityDescription entityForName:@"ServiceDetailTable"  inManagedObjectContext:_managedObjectContext];
        
        [request2 setEntity:entDescriptio];
        NSPredicate    *pred = [NSPredicate predicateWithFormat:@"((iD= %@) && (username= %@))",iDToUpdate,cMgr.strUsreName];
        [request2 setPredicate:pred];
        NSManagedObject *match=nil;
        NSError *error;
        NSArray *object=[_managedObjectContext executeFetchRequest:request2 error:&error];
        NSFetchRequest *request3=[[NSFetchRequest alloc] init];
        NSEntityDescription *entDescriptio1=[NSEntityDescription entityForName:@"Imgpath"  inManagedObjectContext:_managedObjectContext];
        
        [request3 setEntity:entDescriptio1];
        NSPredicate    *pred1 = [NSPredicate predicateWithFormat:@"((iD= %@) && (username= %@))",imgIdtoupdate,cMgr.strUsreName];
        [request3 setPredicate:pred1];
        NSManagedObject *match1=nil;
        NSError *error1;
        NSArray *object1=[_managedObjectContext executeFetchRequest:request3 error:&error1];

        if([object count]==0)
        {
        }
        else
        {
           //===============================================================================
           //===============================================================================
           //===============================================================================
           //===============================================================================

            if([object1 count]==0)
            {
            }
            else
            {
                myArrayOfNames=[[NSMutableArray alloc]init];
                match1=[object1 objectAtIndex:0];
                if (imgShow.image==nil)
                {
                } else {
                    NSString *strImg1 = [NSString stringWithFormat:@"%@.png",[match1 valueForKey:@"imgNameimg"]];
                    [myArrayOfNames addObject:strImg1];
                }
                if (imgShow1.image==nil)
                {
                } else {
                    NSString *strImg2 = [NSString stringWithFormat:@"%@.png",[match1 valueForKey:@"imgNameimg1"]];
                    [myArrayOfNames addObject:strImg2];
                }if (imgShow2.image==nil)
                {
                } else {
                    NSString *strImg3 = [NSString stringWithFormat:@"%@.png",[match1 valueForKey:@"imgNameimg2"]];
                    [myArrayOfNames addObject:strImg3];
                }if (imgShow3.image==nil)
                {
                } else {
                    NSString *strImg4 = [NSString stringWithFormat:@"%@.png",[match1 valueForKey:@"imgNameimg3"]];
                    [myArrayOfNames addObject:strImg4];
                }
                mainStringName = [[NSString alloc]init];
                mainStringName = @"";
                for (int k=0;k<myArrayOfNames.count;k++)
                {
                    mainStringName = [mainStringName stringByAppendingFormat:@"%@,",[myArrayOfNames objectAtIndex:k]];
                }
            }
          //===============================================================================
          //===============================================================================
          //===============================================================================
          //===============================================================================
            if([object1 count]==0)
            {
            }
            else
            {
                myArrayOfNames1 =[[NSMutableArray alloc]init];
                match1=[object1 objectAtIndex:0];
                if (imgShow.image==nil)
                {
                } else {
                    if (dateSetg==nil)
                    {
                        NSString *strImg1 = [NSString stringWithFormat:@"%@.png",[match1 valueForKey:@"imgNameimg"]];
                        [myArrayOfNames1 addObject:strImg1];
 
                    } else {
                        dateSetg=[NSString stringWithFormat:@"%@.png",dateSetg];
                        [myArrayOfNames1 addObject:dateSetg];
                    }
                }
                if (imgShow1.image==nil)
                {
                } else {
                    if (datesetg1==nil)
                    {
                        NSString *strImg2 = [NSString stringWithFormat:@"%@.png",[match1 valueForKey:@"imgNameimg1"]];
                        [myArrayOfNames1 addObject:strImg2];

                    } else {
                        datesetg1=[NSString stringWithFormat:@"%@.png",datesetg1];
                        [myArrayOfNames1 addObject:datesetg1];
                     }

                }if (imgShow2.image==nil)
                {
                } else {
                    if (datesetg2==nil)
                    {
                        NSString *strImg3 = [NSString stringWithFormat:@"%@.png",[match1 valueForKey:@"imgNameimg2"]];
                        [myArrayOfNames1 addObject:strImg3];

                    } else {
                        datesetg2=[NSString stringWithFormat:@"%@.png",datesetg2];
                        [myArrayOfNames1 addObject:datesetg2];
                    }

                }if (imgShow3.image==nil)
                {
                } else {
                    if (datesetg3==nil)
                    {
                        NSString *strImg4 = [NSString stringWithFormat:@"%@.png",[match1 valueForKey:@"imgNameimg3"]];
                        [myArrayOfNames1 addObject:strImg4];
                    } else {
                        datesetg3=[NSString stringWithFormat:@"%@.png",datesetg3];
                        [myArrayOfNames1 addObject:datesetg3];
                    }
                }
                mainStringName1 = [[NSString alloc]init];
                mainStringName1 = @"";
                for (int k=0;k<myArrayOfNames1.count;k++)
                {
                    mainStringName1 = [mainStringName1 stringByAppendingFormat:@"%@,",[myArrayOfNames1 objectAtIndex:k]];
                }
            }
         //===============================================================================
         //===============================================================================
         //===============================================================================
         //===============================================================================

            if (check==1)
            {
                 match=[object objectAtIndex:0];
                [match setValue:lblService.text forKey:@"serviceName"];
                [match setValue:txtviewComment.text forKey:@"comment"];
                [match setValue:hcmLabel.text forKey:@"urgencyLevel"];
                [match setValue:mainStringName forKey:@"imgName"];
                [match setValue:cMgr.cacheAudioname forKey:@"audioName"];
                [match setValue:lservice_id forKey:@"serviceId"];
                [match setValue:dateSet forKey:@"iD"];
                [match setValue:savedImagePath forKey:@"savedImagePath"];
                [match setValue:cMgr.cacheaudioPath forKey:@"audioPath"];
                mainStringName=nil;
                myArrayOfNames=nil;
            }
            
            
        //===============================================================================
        //===============================================================================
        //===============================================================================
        //===============================================================================

            else if(check==2)
            {
                 match=[object objectAtIndex:0];
                [match setValue:lblService.text forKey:@"serviceName"];
                [match setValue:txtviewComment.text forKey:@"comment"];
                [match setValue:hcmLabel.text forKey:@"urgencyLevel"];
                [match setValue:mainStringName1 forKey:@"imgName"];
                [match setValue:cMgr.cacheAudioname forKey:@"audioName"];
                [match setValue:lservice_id forKey:@"serviceId"];
                [match setValue:dateSet forKey:@"iD"];
                [match setValue:savedImagePath forKey:@"savedImagePath"];
                [match setValue:cMgr.cacheaudioPath forKey:@"audioPath"];
                mainStringName1=nil;
                myArrayOfNames1=nil;
                if (cMgr.isFromCustomer)
                {
                    [match setValue:@"new"forKey:@"status"];
                    [match setValue:@"0" forKey:@"accountNo"];
                    [match setValue:lblAccount.text forKey:@"cName"];
                }
            }
            NSDate *date1 = [NSDate date];
            //Create the dateformatter object
            NSDateFormatter* formatter1 = [[NSDateFormatter alloc] init] ;
            //Set the required date format
            [formatter1 setDateFormat:@"MM-dd-yyyy"];
            //Get the string date
            NSString *dateSet1 = [formatter1 stringFromDate:date1];
            [match setValue:dateSet1 forKey:@"dateTime"];
            NSError *savingError = nil;
            if ([self.managedObjectContext save:&savingError])
                
            {
            }
        }
        
        
        
        //===============================================================================
        //===============================================================================
        //===============================================================================
        //===============================================================================

if([object1 count]==0)
        {
        }
        else
        {
            if (check==1)
            {
                match1=[object1 objectAtIndex:0];
                if (imgShow.image==nil)
                {
                    [match1 setValue:nil forKey:@"imgNameimg"];
                    [match1 setValue:nil forKey:@"pathImg"];
                    
                } else {
                  //  [match1 setValue:dateSetg forKey:@"imgNameimg"];
                  //  [match1 setValue:savedImagePath forKey:@"pathImg"];
                }
                
                if (imgShow1.image==nil)
                {
                    [match1 setValue:nil forKey:@"imgNameimg1"];
                    [match1 setValue:nil forKey:@"pathImg1"];
                } else {
                  //  [match1 setValue:datesetg1 forKey:@"imgNameimg1"];
                  //  [match1 setValue:savedImagePath1 forKey:@"pathImg1"];
                }
                
                if (imgShow2.image==nil)
                {
                    [match1 setValue:nil forKey:@"imgNameimg2"];
                    [match1 setValue:nil forKey:@"pathImg2"];
                    
                } else {
                 //   [match1 setValue:datesetg2 forKey:@"imgNameimg2"];
                 //   [match1 setValue:savedImagePath2 forKey:@"pathImg2"];
                }
                
                if (imgShow3.image==nil)
                {
                    [match1 setValue:nil forKey:@"imgNameimg3"];
                    [match1 setValue:nil forKey:@"pathImg3"];
                } else {
                  //  [match1 setValue:datesetg3 forKey:@"imgNameimg3"];
                  //  [match1 setValue:savedImagePath3 forKey:@"pathImg3"];
                }
                
            }
        //===============================================================================
        //===============================================================================
        //===============================================================================
        //===============================================================================
           
            
            else if(check==2)
            {
                match1=[object1 objectAtIndex:0];
                if (imgShow.image==nil)
                {
                    [match1 setValue:nil forKey:@"imgNameimg"];
                    [match1 setValue:nil forKey:@"pathImg"];
                    
                } else {
                    if (dateSetg==nil)
                    {
                        [match1 setValue:[match1 valueForKey:@"imgNameimg"] forKey:@"imgNameimg"];
                        [match1 setValue:[match1 valueForKey:@"pathImg"] forKey:@"pathImg"];
                    } else {
                        [match1 setValue:dateSetg forKey:@"imgNameimg"];
                        [match1 setValue:savedImagePath forKey:@"pathImg"];
                    }
                                 }
                
                if (imgShow1.image==nil)
                {
                    [match1 setValue:nil forKey:@"imgNameimg1"];
                    [match1 setValue:nil forKey:@"pathImg1"];
                } else {
                    if (datesetg1==nil)
                    {
                        [match1 setValue:[match1 valueForKey:@"imgNameimg1"] forKey:@"imgNameimg1"];
                        [match1 setValue:[match1 valueForKey:@"pathImg1"] forKey:@"pathImg1"];
                    } else {
                        [match1 setValue:datesetg1 forKey:@"imgNameimg1"];
                        [match1 setValue:savedImagePath1 forKey:@"pathImg1"];
                    }
                }
                
                if (imgShow2.image==nil)
                {
                    [match1 setValue:nil forKey:@"imgNameimg2"];
                    [match1 setValue:nil forKey:@"pathImg2"];
                    
                } else {
                    if (datesetg2==nil)
                    {
                        [match1 setValue:[match1 valueForKey:@"imgNameimg2"] forKey:@"imgNameimg2"];
                        [match1 setValue:[match1 valueForKey:@"pathImg2"] forKey:@"pathImg2"];
                    } else {
                        [match1 setValue:datesetg2 forKey:@"imgNameimg2"];
                        [match1 setValue:savedImagePath2 forKey:@"pathImg2"];
                    }
                }
                
                if (imgShow3.image==nil)
                {
                    [match1 setValue:nil forKey:@"imgNameimg3"];
                    [match1 setValue:nil forKey:@"pathImg3"];
                } else {
                    if (datesetg3==nil)
                    {
                        [match1 setValue:[match1 valueForKey:@"imgNameimg3"] forKey:@"imgNameimg3"];
                        [match1 setValue:[match1 valueForKey:@"pathImg3"] forKey:@"pathImg3"];
                    } else {
                        [match1 setValue:datesetg3 forKey:@"imgNameimg3"];
                        [match1 setValue:savedImagePath3 forKey:@"pathImg3"];
                    }
                }
            }
            NSDate *date1 = [NSDate date];
            //Create the dateformatter object
            NSDateFormatter* formatter1 = [[NSDateFormatter alloc] init] ;
            //Set the required date format
            [formatter1 setDateFormat:@"MM-dd-yyyy"];
            //Get the string date
            NSString *dateSet1 = [formatter1 stringFromDate:date1];
            [match setValue:dateSet1 forKey:@"dateTime"];
            NSError *savingError = nil;
            if ([self.managedObjectContext save:&savingError])
                
            {
            }
        }
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
            SelectedServiceViewController* verificationBadgesViewController = (SelectedServiceViewController*)[storyboard instantiateViewControllerWithIdentifier:@"select"];
            
            
            [self.navigationController pushViewController:verificationBadgesViewController animated:YES];
        }
        else
        {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
        SelectedServiceViewController* verificationBadgesViewController = (SelectedServiceViewController*)[storyboard instantiateViewControllerWithIdentifier:@"select"];
        
        
        [self.navigationController pushViewController:verificationBadgesViewController animated:NO];
        }

   }
        iDToUpdate=nil;
        imgIdtoupdate=nil;
        dateSetg=nil;
        datesetg1=nil;
        datesetg2=nil;
        datesetg3=nil;
        savedImagePath=nil;
        savedImagePath1=nil;
        savedImagePath2=nil;
        savedImagePath3=nil;
}
  //  txtviewComment.text=nil;
}
-(IBAction)captureImage
{

    if(imgShow.image==nil)
    {
        [_afterImageCaptureNxtBtn setHidden:NO];
        [_hideNxtBtn setHidden:YES];
    imgCapture = [[UIImagePickerController alloc] init];
    imgCapture.delegate = self;
        
    self.imgCapture.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:self.imgCapture  animated:YES completion:nil];
    }
    else if(imgShow1.image==nil)
    {
        [_afterImageCaptureNxtBtn setHidden:NO];
        [_hideNxtBtn setHidden:YES];
        imgCapture = [[UIImagePickerController alloc] init];
        imgCapture.delegate = self;
        self.imgCapture.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:self.imgCapture  animated:YES completion:nil];
 
    }
    else if(imgShow2.image==nil)
    {
        [_afterImageCaptureNxtBtn setHidden:NO];
        [_hideNxtBtn setHidden:YES];
        imgCapture = [[UIImagePickerController alloc] init];
        imgCapture.delegate = self;
        self.imgCapture.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:self.imgCapture  animated:YES completion:nil];

    }
    else if(imgShow3.image==nil)
    {
        [_afterImageCaptureNxtBtn setHidden:NO];
        [_hideNxtBtn setHidden:YES];
        imgCapture = [[UIImagePickerController alloc] init];
        imgCapture.delegate = self;
        self.imgCapture.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:self.imgCapture  animated:YES completion:nil];

    }
    else
    {
        [_afterImageCaptureNxtBtn setHidden:NO];
        [_hideNxtBtn setHidden:YES];
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"You can select maximum four photos" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info

{
    fromcamera=YES;
    if (imgShow.image==nil) {
        imgShow.image = [info objectForKey:UIImagePickerControllerOriginalImage];

        
        //[self performSelectorInBackground:@selector(saveRecord:) withObject:@"1"];
        [self saveRecord:1];
    }
    
    else if (imgShow1.image==nil)
    {
      
        
        imgShow1.image = [info objectForKey:UIImagePickerControllerOriginalImage];
        //[self performSelectorInBackground:@selector(saveRecord:) withObject:@"2"];
    [self saveRecord:2];
    
    }
    else if (imgShow2.image==nil)
    {
        imgShow2.image = [info objectForKey:UIImagePickerControllerOriginalImage];
        //[self performSelectorInBackground:@selector(saveRecord:) withObject:@"3"];
        [self saveRecord:3];
        
        
    }
    else if (imgShow3.image==nil)
    {
        imgShow3.image = [info objectForKey:UIImagePickerControllerOriginalImage];
       // [self performSelectorInBackground:@selector(saveRecord:) withObject:@"4"];
        [self saveRecord:4];
    }


  
    
        
        [self.imgCapture dismissViewControllerAnimated:YES completion:nil];
             
    if ([[UIScreen mainScreen] bounds].size.height == 480){
        
        
   [scrollView setContentOffset:CGPointMake(scrollView.frame.origin.x,100) animated:YES];
        
    }
}



-(void)saveRecord : (int)ImgeNumber
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSError *error;
        //Create the dateformatter object
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init] ;
    //Set the required date format
   NSUInteger r = arc4random_uniform(1000000) + 1;

    //Get the string date
  //  NSLog(@"%@",dateSetg);
    NSData *imageData;
    
    
    if (ImgeNumber==1) {
        
        NSDate *date = [NSDate date];
         [formatter setDateFormat:@"HH-MM-SS.SSS"];
        dateSetg =[formatter stringFromDate:date];
        dateSetg =[dateSetg stringByAppendingFormat:@"%lu",(unsigned long)r];
        FinalImageName = dateSetg;
         FinalImageName = [FinalImageName stringByAppendingString:@".png"];
        savedImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"img%@.png",dateSetg]];
        imageData = UIImageJPEGRepresentation(imgShow.image, 0.00000000000000000000000000001f);
        cMgr.isYesimg=YES;
         [imageData writeToFile:savedImagePath atomically:YES];
    }
    else if (ImgeNumber==2)
    {
        NSDate *date2 = [NSDate date];
        [formatter setDateFormat:@"HH-MM-SS.SSS"];
        datesetg1 =[formatter stringFromDate:date2];
        datesetg1 =[datesetg1 stringByAppendingFormat:@"%lu",(unsigned long)r];
         FinalImageName =[NSString stringWithFormat:@"%@,%@.png",FinalImageName,datesetg1];
        savedImagePath1 = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"img%@.png",datesetg1]];
        imageData = UIImageJPEGRepresentation(imgShow1.image, 0.00000000000000000000000000001f);
       [imageData writeToFile:savedImagePath1 atomically:YES];
        
    }
    else if (ImgeNumber==3)
    {
        NSDate *date3 = [NSDate date];
        [formatter setDateFormat:@"HH-MM-SS.SSS"];
         datesetg2 =[formatter stringFromDate:date3];
        datesetg2 =[datesetg2 stringByAppendingFormat:@"%lu",(unsigned long)r];
        FinalImageName =[NSString stringWithFormat:@"%@,%@.png",FinalImageName,datesetg2];
        savedImagePath2 = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"img%@.png",datesetg2]];
        imageData = UIImageJPEGRepresentation(imgShow2.image, 0.00000000000000000000000000001f);
        [imageData writeToFile:savedImagePath2 atomically:YES];
    }

    else if (ImgeNumber==4)
    {
        NSDate *date4 = [NSDate date];
        [formatter setDateFormat:@"HH-MM-SS.SSS"];
        datesetg3 =[formatter stringFromDate:date4];
        datesetg3 =[datesetg3 stringByAppendingFormat:@"%lu",(unsigned long)r];
        FinalImageName =[NSString stringWithFormat:@"%@,%@.png",FinalImageName,datesetg3];
        savedImagePath3 = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"img%@.png",datesetg3]];
        
        imageData = UIImageJPEGRepresentation(imgShow3.image, 0.00000000000000000000000000001f);
        [imageData writeToFile:savedImagePath3 atomically:YES];
        
    }
  check=2;
}
-(void)saveaudio
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSError *error;
    NSDate *date = [NSDate date];
    //Create the dateformatter object
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init] ;
    //Set the required date format
    [formatter setDateFormat:@"yyyy-MM-dd-HH-MM-SS"];
    //Get the string date
    NSString *dateSet = [formatter stringFromDate:date];
    
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"img%@.png",dateSet]];
   // NSLog(@"savedImagePath:%@",savedImagePath);
    NSData *imageData = UIImageJPEGRepresentation(imgShow.image, 0.001f);
    
    
    [imageData writeToFile:savedImagePath atomically:YES];
	
	
    //	NSData *audioData = [NSData dataWithContentsOfFile:[url path] options: 0 error:&err];
    //	if(audioData)
    //	{
    //		NSFileManager *fm = [NSFileManager defaultManager];
    //		//[fm removeItemAtPath:[url path] error:&err];
    //	}
	
	
    
}

-(void)startChangingImage
{

    NSArray *array = [[NSArray alloc] initWithObjects:
                      [UIImage imageNamed:@"anim1.png"],
                      [UIImage imageNamed:@"anim2.png"],
                      nil];
    
    self.changeImgbtn.image = [array lastObject];
    self.changeImgbtn.animationImages = array;
    self.changeImgbtn.animationDuration = 3;
    self.changeImgbtn.animationRepeatCount = 30;
  //  NSLog(@"%ld",(long)self.changeImgbtn.animationRepeatCount );
    
   
    [self.changeImgbtn startAnimating];
    
}



-(IBAction)startRecording
{
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
	NSError *err = nil;
	[audioSession setCategory :AVAudioSessionCategoryPlayAndRecord error:&err];
	if(err){
      //  NSLog(@"audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        return;
	}
	[audioSession setActive:YES error:&err];
	err = nil;
	if(err){
       // NSLog(@"audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        return;
	}
	
	recordSetting = [[NSMutableDictionary alloc] init];
	
		
	
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    [recordSetting setValue:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
    [recordSetting setValue:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsBigEndianKey];
    [recordSetting setValue:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsFloatKey];
    
	// Create a new dated file
	NSDate *now = [NSDate date];
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init] ;
    //Set the required date format
    [formatter setDateFormat:@"yyyy-MM-dd-HH-MM-SS"];
    //Get the string date
    NSString *dateSet = [formatter stringFromDate:now];
   // NSLog(@"Amo:%@",dateSet);
    audioName= [NSString stringWithFormat:@"%@.mp4",dateSet];
    cMgr.cacheAudioname=audioName;
    
   // NSLog(@"%@",audioName);
    recorderFilePath = [NSString stringWithFormat:@"%@/%@",DOCUMENTS_FOLDER,audioName];
    
    cMgr.cacheaudioPath= recorderFilePath;
	//recorderFilePath = [[NSString stringWithFormat:@"%@/MySound.caf", DOCUMENTS_FOLDER] retain];
	
	//NSLog(@"recorderFilePath: %@",recorderFilePath);
	
	NSURL *url = [NSURL fileURLWithPath:recorderFilePath];
	
	err = nil;
	
    //	NSData *audioData = [NSData dataWithContentsOfFile:[url path] options: 0 error:&err];
    //	if(audioData)
    //	{
    //		NSFileManager *fm = [NSFileManager defaultManager];
    //		//[fm removeItemAtPath:[url path] error:&err];
    //	}
	
	err = nil;
	recorder = [[ AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:&err];
	if(!recorder){
      //  NSLog(@"recorder: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        UIAlertView *alert1 =
        [[UIAlertView alloc] initWithTitle: @"Warning"
								   message: [err localizedDescription]
								  delegate: nil
						 cancelButtonTitle:@"OK"
						 otherButtonTitles:nil];
        [alert1 show];
       
        return;
	}
	
	//prepare to record
	[recorder setDelegate:self];
	[recorder prepareToRecord];
	recorder.meteringEnabled = YES;
	
	BOOL audioHWAvailable = audioSession.inputIsAvailable;
	if (! audioHWAvailable) {
        UIAlertView *cantRecordAlert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
								   message: @"Audio input hardware not available"
								  delegate: nil
						 cancelButtonTitle:@"OK"
						 otherButtonTitles:nil];
        [cantRecordAlert show];
        
        return;
	}
	
	// start recording
    [recorder record];
	//[recorder recordForDuration:(NSTimeInterval) 30];
	//[recorder recordForDuration:(NSTimeInterval) 30];
	
    //	lblStatusMsg.text = @"Recording...";
    //	progressView.progress = 0.0;
    //	timer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(handleTimer) userInfo:nil repeats:YES];}
}

- (IBAction)stopRecording
{
    
     [twoMinTimer invalidate];
	[recorder stop];
   
    recorder = nil;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Your audio recorded successfully !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [alert show];
    [self dismissViewControllerAnimated:YES completion:nil];
   
}

-(void)viewWillAppear:(BOOL)animated
{
    if (fromcamera) {
        fromcamera=NO;
    }
    else{
    
    NSUserDefaults *boolUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *strIndex = [boolUserDefaults valueForKey:@"strIndex"];
    int i = [strIndex intValue];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    
//========================================================================================
    NSUserDefaults *boolUserDefaultss = [NSUserDefaults standardUserDefaults];
    BOOL isCheck = [boolUserDefaultss boolForKey:@"isFromAddMoreView"];
    if (isCheck) {
        NSUserDefaults *boolUserDefaults = [NSUserDefaults standardUserDefaults];
        [boolUserDefaults setBool:NO forKey:@"isFromAddMoreView"];
        [boolUserDefaults synchronize];
        txtviewComment.text=@"";
        [scrollView setContentOffset:
         CGPointMake(0, -scrollView.contentInset.top) animated:YES];

        if ((imgShow.image==nil) && (imgShow1.image==nil) && (imgShow2.image==nil) && (imgShow3.image==nil)) {
            [_afterImageCaptureNxtBtn setHidden:YES];
            [_hideNxtBtn setHidden:NO];
        }
        else if (imgShow.image==nil) {
            if ((imgShow1.image!=nil) || (imgShow2.image!=nil) || (imgShow3.image!=nil)) {
                [_afterImageCaptureNxtBtn setHidden:NO];
                [_hideNxtBtn setHidden:YES];
            } else {
                [_afterImageCaptureNxtBtn setHidden:YES];
                [_hideNxtBtn setHidden:NO];
            }
        }
        else {
            [_afterImageCaptureNxtBtn setHidden:NO];
            [_hideNxtBtn setHidden:YES];
        }
    }
    
    else 
    if(cMgr.isUpdate)
    {
        
        [scrollView setContentOffset:
         CGPointMake(0, -scrollView.contentInset.top) animated:YES];

        lblAccount.text=[[objectsMutable objectAtIndex:i]accountNo];
        
        if ([lblAccount.text isEqualToString:@"0"]) {
            
            lblAccount.text=[[objectsMutable objectAtIndex:i]cName];
            
        }
        
        lblService.text=[[objectsMutable objectAtIndex:i]serviceName];
        txtviewComment.text=[[objectsMutable objectAtIndex:i]comment];
        hcmLabel.text=[[objectsMutable objectAtIndex:i]urgencyLevel];
      //  NSString *imgName1 =[[objectsMutable objectAtIndex:i]savedImagePath];
        
        iDToUpdate=[[objectsMutable objectAtIndex:i]iD];
        
        NSString *imgName1=[[pImg objectAtIndex:i]pathImg];
        NSString *appFile = imgName1;
        UIImage *_img = [UIImage imageWithContentsOfFile:appFile];
        imgShow.image=_img;
        
        NSString    *imgName2=[[pImg objectAtIndex:i]pathImg1];
        NSString *appFile1 = imgName2;
        UIImage *_img1 = [UIImage imageWithContentsOfFile:appFile1];
        imgShow1.image=_img1;
        
        NSString    *imgName3=[[pImg objectAtIndex:i]pathImg2];
        NSString *appFile2 = imgName3;
        UIImage *_img2 = [UIImage imageWithContentsOfFile:appFile2];
        imgShow2.image=_img2;

        NSString    *imgName4=[[pImg objectAtIndex:i]pathImg3];
        NSString *appFile3 = imgName4;
        UIImage *_img3 = [UIImage imageWithContentsOfFile:appFile3];
        imgShow3.image=_img3;
        
        imgIdtoupdate=[[pImg objectAtIndex:i]iD];
        cMgr.isUpdate=NO;
        check=1;
        NSUserDefaults *boolUserDefaults = [NSUserDefaults standardUserDefaults];
        [boolUserDefaults setValue:@"" forKey:@"strIndex"];
        [boolUserDefaults synchronize];
        
        if ((imgShow.image==nil) && (imgShow1.image==nil) && (imgShow2.image==nil) && (imgShow3.image==nil)) {
            [_afterImageCaptureNxtBtn setHidden:YES];
            [_hideNxtBtn setHidden:NO];
        }
        else if (imgShow.image==nil) {
            if ((imgShow1.image!=nil) || (imgShow2.image!=nil) || (imgShow3.image!=nil)) {
                [_afterImageCaptureNxtBtn setHidden:NO];
                [_hideNxtBtn setHidden:YES];
            } else {
                [_afterImageCaptureNxtBtn setHidden:YES];
                [_hideNxtBtn setHidden:NO];
            }
        }
        else {
            [_afterImageCaptureNxtBtn setHidden:NO];
            [_hideNxtBtn setHidden:YES];
        }

    }
    else
    {
        if ((imgShow.image==nil) && (imgShow1.image==nil) && (imgShow2.image==nil) && (imgShow3.image==nil)) {
            [_afterImageCaptureNxtBtn setHidden:YES];
            [_hideNxtBtn setHidden:NO];
        }
        
        if (imgShow.image==nil) {
            if ((imgShow1.image!=nil) || (imgShow2.image!=nil) || (imgShow3.image!=nil)) {
                [_afterImageCaptureNxtBtn setHidden:NO];
                [_hideNxtBtn setHidden:YES];
            } else {
                [_afterImageCaptureNxtBtn setHidden:YES];
                [_hideNxtBtn setHidden:NO];
            }
        } else {
            [_afterImageCaptureNxtBtn setHidden:NO];
            [_hideNxtBtn setHidden:YES];
        }
    }
    if (!cMgr.isAutoid)
    {
    }
    }
}
-(IBAction)showingToCustomer
{
}
-(IBAction)actionForRecordingandTimer
{
    cMgr.isFromaddLead=YES;
    
    UIStoryboard *storyboard;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    }
    else
    {
   
        storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
    }
    AddleadViewController* lgn = (AddleadViewController*)[storyboard instantiateViewControllerWithIdentifier:@"addr"];
     [self presentViewController:lgn animated:YES completion:nil];
}
-(IBAction)imagefromgallery{
    
    if(imgShow.image==nil)
    {
        [_afterImageCaptureNxtBtn setHidden:NO];
        [_hideNxtBtn setHidden:YES];
        imgCapture = [[UIImagePickerController alloc] init];
        imgCapture.delegate = self;
        self.imgCapture.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.imgCapture  animated:YES completion:nil];
    }
    else if(imgShow1.image==nil)
    {
        [_afterImageCaptureNxtBtn setHidden:NO];
        [_hideNxtBtn setHidden:YES];
        imgCapture = [[UIImagePickerController alloc] init];
        imgCapture.delegate = self;
        self.imgCapture.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.imgCapture  animated:YES completion:nil];
    }
    else if(imgShow2.image==nil)
    {
        [_afterImageCaptureNxtBtn setHidden:NO];
        [_hideNxtBtn setHidden:YES];
        imgCapture = [[UIImagePickerController alloc] init];
        imgCapture.delegate = self;
        self.imgCapture.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.imgCapture  animated:YES completion:nil];
    }
    else if(imgShow3.image==nil)
    {
        [_afterImageCaptureNxtBtn setHidden:NO];
        [_hideNxtBtn setHidden:YES];
        imgCapture = [[UIImagePickerController alloc] init];
        imgCapture.delegate = self;
        self.imgCapture.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.imgCapture  animated:YES completion:nil];
    }
    else
    {
        [_afterImageCaptureNxtBtn setHidden:NO];
        [_hideNxtBtn setHidden:YES];
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"You can select maximum four photos" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }

}
- (IBAction)goToAddHelp:(id)sender {
    UIStoryboard *storyboard;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    }
    HelpViewController* lgn = (HelpViewController*)[storyboard instantiateViewControllerWithIdentifier:@"HelpViewControlleriPadAddLead"];
    [self.navigationController presentViewController:lgn animated:NO completion:nil];
}

-(IBAction)removeImage
{
}
-(void)viewDidDisappear:(BOOL)animated
{
}
-(void)dealloc
{
//    txtviewComment.text=nil;
    lblService.text=nil;
    hcmLabel.text=nil;
}
@end
