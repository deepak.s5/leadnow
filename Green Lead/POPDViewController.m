//
//  POPDViewController.m
//  popdowntable
//
//  Created by Alex Di Mango on 15/09/2013.
//  Copyright (c) 2013 Alex Di Mango. All rights reserved.
//

#import "POPDViewController.h"
#import "POPDCell.h"

#define TABLECOLOR [UIColor colorWithRed:62.0/255.0 green:76.0/255.0 blue:87.0/255.0 alpha:1.0]
#define CELLSELECTED [UIColor colorWithRed:52.0/255.0 green:64.0/255.0 blue:73.0/255.0 alpha:1.0]
#define SEPARATOR [UIColor colorWithRed:31.0/255.0 green:38.0/255.0 blue:43.0/255.0 alpha:1.0]
#define SEPSHADOW [UIColor colorWithRed:80.0/255.0 green:97.0/255.0 blue:110.0/255.0 alpha:1.0]
#define SHADOW [UIColor colorWithRed:69.0/255.0 green:84.0/255.0 blue:95.0/255.0 alpha:1.0]
#define TEXT [UIColor colorWithRed:223.0/255.0 green:223.0/255.0 blue:213.0/255.0 alpha:1.0]

static NSString *kheader = @"menuSectionHeader";
static NSString *ksubSection = @"menuSubSection";

@interface POPDViewController ()
@property NSArray *sections;
@property (strong, nonatomic) NSMutableArray *sectionsArray;
@property (strong, nonatomic) NSMutableArray *showingArray;
@property (strong, nonatomic) NSMutableArray *arraySelecteValues;
@end

@implementation POPDViewController
@synthesize delegate;

- (id)initWithMenuSections:(NSArray *) menuSections
{
    self = [super init];
    if (self) {
        self.sections = menuSections;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isClose=YES;
    isClose1=YES;
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *days = [prefs stringForKey:@"keyToSaveDays"];
    if (days.length<1) {//if number of days not available set 100 as default
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        // saving an NSString
        [prefs setObject:@"100" forKey:@"keyToSaveDays"];
        [prefs synchronize];
    }
    isAlreadyValues=NO;
    isSelected=NO;
  // self.tableView.backgroundColor = TABLECOLOR;
   [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    //self.tableView.frame = CGRectMake(0, 150, 320, 480);
    self.sectionsArray = [NSMutableArray new];
    self.showingArray = [NSMutableArray new];
    self.arraySelecteValues= [[NSMutableArray alloc] init];
   [self setMenuSections:self.sections];
}
-(void)setMenuSections:(NSArray *)menuSections{
    for (NSDictionary *sec in menuSections) {
        NSString *header = [sec objectForKey:kheader];
        NSArray *subSection = [sec objectForKey:ksubSection];

        NSMutableArray *section = [NSMutableArray new];
        [section addObject:header];
        
        for (NSString *sub in subSection) {
            [section addObject:sub];
        }
        [self.sectionsArray addObject:section];
        [self.showingArray addObject:[NSNumber numberWithBool:NO]];
    }
    [self.tableView reloadData];
}
-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.sectionsArray count];
   // NSLog(@"section array %@",self.sectionsArray);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    
    if (![[self.showingArray objectAtIndex:section]boolValue]) {
        return 1;
    }
    else{
        return [[self.sectionsArray objectAtIndex:section]count];;
    }
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

    if(indexPath.row ==0){
    if([[self.showingArray objectAtIndex:indexPath.section]boolValue]){
       // [cell setBackgroundColor:CELLSELECTED];
    }else{
        [cell setBackgroundColor:[UIColor clearColor]];
    }
}
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"menuCell";
   // #warning : Use here your custom cell, instead of POPDCell
    
    POPDCell *cell = nil;
    
    cell = (POPDCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell) {
        cell=nil;
        [cell removeFromSuperview];
    }

    NSArray *topLevelObjects;// = [[NSBundle mainBundle] loadNibNamed:@"POPDCell" owner:self options:nil];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"POPDCell_iPad" owner:self options:nil];
    }
    else
    {
        topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"POPDCell" owner:self options:nil];
        
    }
    if (cell == nil) {
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    cell.labelText.text = [[self.sectionsArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];

         if (indexPath.section==0)
  {
    
    if (indexPath.row==0) { //for first section
         [cell setBackgroundColor:[UIColor lightGrayColor]];
          UIFont *myFont = [UIFont fontWithName:@"Marion Bold" size: 18.0 ];
          cell.textLabel.font  = myFont;
        
        UIImageView *imgView;// = [[UIImageView alloc] initWithFrame:CGRectMake(280, 5, 25, 30)];
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            imgView = [[UIImageView alloc] initWithFrame:CGRectMake(UIScreen.mainScreen.bounds.size.width - 50, 30, 25, 30)];
        }
        else
        {
            imgView = [[UIImageView alloc] initWithFrame:CGRectMake(UIScreen.mainScreen.bounds.size.width - 50, 5, 25, 30)];
        }
        if (isClose==YES)
        {
             isClose=NO;
            [imgView setImage:[UIImage imageNamed:@"Arrow-expand-icon.png"]];
           
        } else {
            isClose=YES;
            [imgView setImage:[UIImage imageNamed:@"drop-down2.png"]];
        }
             [cell.contentView addSubview:imgView];
                           }
    else if (indexPath.row>0)
    {
       NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSArray *arrayvalus = [userDefaults objectForKey:@"keySelectedValues"];
    
        for (int i=0; i<[arrayvalus count]; i++)
            {
               isAlreadyValues=YES;
               NSString *valueTocheck = [arrayvalus objectAtIndex:i];
              if ([[[self.sectionsArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] isEqualToString:valueTocheck])
              {
                 [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                 [self.arraySelecteValues addObject:valueTocheck];
              }
            }
     }
  }
//=========================================================================================
//========================================================================================
//========================================================================================
//========================================================================================
    
else // if section is 1
    {
        if (indexPath.row==0) {
             [cell setBackgroundColor:[UIColor lightGrayColor]];
            UIImageView *imgViewArrow;//= [[UIImageView alloc] initWithFrame:CGRectMake(280, 5, 25, 30)];
            
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                imgViewArrow = [[UIImageView alloc] initWithFrame:CGRectMake(UIScreen.mainScreen.bounds.size.width - 50, 30, 25, 30)];
                
            }
            else
            {
                imgViewArrow = [[UIImageView alloc] initWithFrame:CGRectMake(UIScreen.mainScreen.bounds.size.width - 50, 5, 25, 30)];
            }
            if (isClose1==YES)
            {
                isClose1=NO;
               [imgViewArrow setImage:[UIImage imageNamed:@"Arrow-expand-icon.png"]];
                
            } else {
                isClose1=YES;
                 [imgViewArrow setImage:[UIImage imageNamed:@"drop-down2.png"]];
            }
            [cell.contentView addSubview:imgViewArrow];
        }
        if (indexPath.section==1 &&  indexPath.row==1) {
            
            [cell.separator setHidden:YES];
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 8, 371, 50)];
                [imgView setImage:[UIImage imageNamed:@"inputbox.png"]];
                [cell.contentView addSubview:imgView];
                
                UITextField *txtDays = [[UITextField alloc] initWithFrame:CGRectMake(14, 8, 350, 50)];
                txtDays.delegate=self;
                txtDays.tag=201;
                txtDays.keyboardType=UIKeyboardTypeNumberPad;
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSString *days = [prefs stringForKey:@"keyToSaveDays"];
                txtDays.text=days;
                txtDays.layer.borderWidth = 0.0f;
                // txtDays.layer.borderColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"button.png"]].CGColor;
                [cell.contentView addSubview:txtDays];
                
                UIButton *btnSave = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                btnSave.frame = CGRectMake(264, 60, 120, 45);
                [btnSave setBackgroundImage:[UIImage imageNamed:@"button.png"] forState:UIControlStateNormal];
                [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [btnSave setTitle:@"Save" forState:UIControlStateNormal];
                btnSave.tag=202;
                [btnSave addTarget:self action:@selector(saveNumberOfDays:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:btnSave];
                
                UILabel *lblDetail = [[UILabel alloc] initWithFrame:CGRectMake(11, 40, 350, 80)];
                lblDetail.font=[UIFont fontWithName:@"system" size:22];
                lblDetail.text=@"Limit 100 days only";
                lblDetail.textColor= [UIColor lightGrayColor];
                [cell.contentView addSubview:lblDetail];
            }
            else
            {
            UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 8, 271, 30)];
            [imgView setImage:[UIImage imageNamed:@"inputbox.png"]];
            [cell.contentView addSubview:imgView];
            
            UITextField *txtDays = [[UITextField alloc] initWithFrame:CGRectMake(14, 8, 250, 30)];
            txtDays.delegate=self;
            txtDays.tag=201;
            txtDays.keyboardType=UIKeyboardTypeNumberPad;
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *days = [prefs stringForKey:@"keyToSaveDays"];
            txtDays.text=days;
            txtDays.layer.borderWidth = 0.0f;
           // txtDays.layer.borderColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"button.png"]].CGColor;
            [cell.contentView addSubview:txtDays];
            
            UIButton *btnSave = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            btnSave.frame = CGRectMake(200, 40, 80, 25);
            [btnSave setBackgroundImage:[UIImage imageNamed:@"button.png"] forState:UIControlStateNormal];
            [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnSave setTitle:@"Save" forState:UIControlStateNormal];
            btnSave.tag=202;
            [btnSave addTarget:self action:@selector(saveNumberOfDays:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:btnSave];
            
            UILabel *lblDetail = [[UILabel alloc] initWithFrame:CGRectMake(10, 35, 250, 40)];
            lblDetail.font=[UIFont fontWithName:@"system" size:12];
            lblDetail.text=@"Limit 100 days only";
            lblDetail.textColor= [UIColor lightGrayColor];
            [cell.contentView addSubview:lblDetail];
         }
        }
        
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }

    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 3) ? NO : YES;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([[self.showingArray objectAtIndex:indexPath.section]boolValue]){
        if (indexPath.row==0) {
            [self.showingArray setObject:[NSNumber numberWithBool:NO] atIndexedSubscript:indexPath.section];
            [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
        }
    }else{
        [self.showingArray setObject:[NSNumber numberWithBool:YES] atIndexedSubscript:indexPath.section];
        [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
    }
  
    NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
    
    if (indexPath.section==0 && indexPath.row>0) {
        
                            if (index != NSNotFound) {
                                                        UITableViewCell *cell = [[tableView visibleCells] objectAtIndex:index];
                                                        if ([cell accessoryType] == UITableViewCellAccessoryNone) {
                                                        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                                                          
                                                            
                                                            [self.arraySelecteValues addObject:[[self.sectionsArray objectAtIndex:0] objectAtIndex:indexPath.row]];
                                                      }
                                                    else {
                                                        [cell setAccessoryType:UITableViewCellAccessoryNone];
                                                         [self.arraySelecteValues removeObject:[[self.sectionsArray objectAtIndex:0] objectAtIndex:indexPath.row]];
                                                        }
}
        if ([self.sectionsArray count]>0) {
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:self.arraySelecteValues forKey:@"keySelectedValues"];
            [userDefaults synchronize];
        }

    }
    
        NSLog(@"selected -- %@",self.sectionsArray);
       [self.delegate didSelectRowAtIndexPath:indexPath];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==1 && indexPath.row==1) {
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            
            return 170.0;

            
        }else{
            
            return 70.0;

        }
    }
    else
    {
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            
            return 100.0;
            
            
        }else{
            
            return 44.0;
            
        }

    }
    
}
#pragma mark - TextField Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;

}
#pragma mark - Other Funtions
-(void)saveNumberOfDays:(id)sender
{
    
    
    UIButton *btn = (UIButton *)sender;
    UITextField *txtFldDays = (UITextField *)[self.view viewWithTag:btn.tag-1];
    
    
    if ([txtFldDays.text length]==0 || [txtFldDays.text isEqualToString:@"00"] || [txtFldDays.text isEqualToString:@"000"] || [txtFldDays.text isEqualToString:@"0"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter number of days between 1 to 100" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        
    }
   else if ([txtFldDays.text intValue]<=100) {
        
   
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    [prefs setObject:txtFldDays.text forKey:@"keyToSaveDays"];
    [prefs synchronize];
    
    [txtFldDays resignFirstResponder];
    
    NSString *strTitle = [NSString stringWithFormat:@"Now you can see last %@ days Leads in history",txtFldDays.text];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Saved" message:strTitle delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
    
    [alert show];
    }
  

    else if ([txtFldDays.text intValue]<0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Re-enter !" message:@"Please enter valid number" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    
    }
        else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Re-enter !" message:@"Limit 100 days only" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    
    }
}
-(BOOL)isNumeric:(NSString*)inputString{
    BOOL isValid = NO;
    NSCharacterSet *alphaNumbersSet = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *stringSet = [NSCharacterSet characterSetWithCharactersInString:inputString];
    isValid = [alphaNumbersSet isSupersetOfSet:stringSet];
    return isValid;
}
@end
