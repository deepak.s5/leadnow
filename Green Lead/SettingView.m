//
//  POPDSampleViewController.m
//  popdowntable
//
//  Created by Alex Di Mango on 15/09/2013.
//  Copyright (c) 2013 Alex Di Mango. All rights reserved.
//

#import "SettingView.h"
#import "POPDViewController.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import "DejalActivityView.h"

static NSString *kheader = @"menuSectionHeader";
static NSString *ksubSection = @"menuSubSection";

@interface SettingView()<POPDDelegate>

@end

@implementation SettingView
- (void)viewDidLoad
{
    [super viewDidLoad];
    cMgr=[CacheManager getInstance];
    appDelegate =
    (id)[[UIApplication sharedApplication] delegate];
    
    context =[appDelegate managedObjectContext];

    NSArray *sucSectionsA = [NSArray arrayWithObjects:@"Total Leads",@"New Lead",@"Scheduled",@"Total Open",@"Total Complete",@"Not Interested",@"First Attempt",@"Second Attempt",@"Not Reached",@"Sold",@"Complete Proposed",@"Invalid", nil];
    NSArray *sucSectionsB = [NSArray arrayWithObjects:@"",nil];
    NSDictionary *sectionA = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @"Lead Status", kheader,
                                    sucSectionsA, ksubSection,
                                    nil];
    NSDictionary *sectionB = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"Historical Data Days", kheader,
                        sucSectionsB, ksubSection,
                        nil];
    NSArray *menu = [NSArray arrayWithObjects:sectionA,sectionB, nil];
    POPDViewController *popMenu = [[POPDViewController alloc]initWithMenuSections:menu];
    popMenu.delegate = self;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
    popMenu.view.frame = CGRectMake(0, 190, self.view.frame.size.width, self.view.frame.size.height-150);
      
    }
    else
    {
    popMenu.view.frame = CGRectMake(0, 175, self.view.frame.size.width, self.view.frame.size.height-150);
    }
    //ios7 status bar
     popMenu.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
    [self addChildViewController:popMenu];
    [self.view addSubview:popMenu.view];
    
    
    if ([[UIScreen mainScreen] bounds].size.height <= 736){
        self.hieght.constant = 60;
    }else{
        self.hieght.constant = 135;
       // self.hieght.constant = 117;
        
    }
    
}
#pragma mark POPDViewController delegate

-(void) didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    //NSLog(@"didSelectRowAtIndexPath: %d,%d",indexPath.section,indexPath.row);
}
- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)RefreshAction:(id)sender {
    [self getService];
}

- (IBAction)aboutAction:(id)sender {
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    NSString *strDate =[defs valueForKey:@"DateAppInstalled"];
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    NSString *strVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *strAppVersions = [NSString stringWithFormat:@"App Version: %@",strVersion];//@"App Version: 3.0.5";
    NSString *strdates = [NSString stringWithFormat:@"Installed Date: %@",strDate];
    NSString *strIOSversions =[NSString stringWithFormat:@"current iOS Version %@",currSysVer];
    NSString *strInfo = [NSString stringWithFormat:@"**This App supports iOS version greater then 8.0**"];
    
    NSString *strVersionDetals = [NSString stringWithFormat:@"%@ \n %@ \n %@ \n %@",strAppVersions,strdates,strIOSversions,strInfo];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Version Details" message:strVersionDetals delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

-(IBAction)back:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(void)getService
{
    //    if ([arrAllObj count] == 0)
    //    {
    Reachability *reachableForWiFy=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify = [reachableForWiFy currentReachabilityStatus];
    if (netStatusWify== NotReachable)
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Error"
                              message:@"Please check your network connection and relaunch the application"
                              delegate:self
                              cancelButtonTitle:@"Dismiss"
                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [DejalBezelActivityView activityViewForView:self.view];
        [self deleteDatabase];
        NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
        NSString *va=[def valueForKey:@"url"];
        
        NSString   *strUk=[def stringForKey:@"key"];
        
        NSString   *strU1=[def stringForKey:@"name"];
        
        NSURL *url1 = [NSURL URLWithString:[NSString stringWithFormat:@"%@?key=getServices&Uname=%@&comKey=%@",[va stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[strU1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[strUk stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
        cMgr.serviceName=@"getServices";
        
        NSXMLParser *xmlParser = [[NSXMLParser alloc]initWithContentsOfURL:url1];
        XMLParser   *parser = [[XMLParser alloc] init];
        [xmlParser setDelegate:parser];
        
        BOOL success = [xmlParser parse];
        if (success) {
            // NSLog(@"SUCCESS:%d",[personalGoalarray count]);
            if ([personalGoalarray count]>0) {
            }
        }
    [self performSelector:@selector(removeDezal) withObject:nil afterDelay:0.7];
    }
}
-(void)removeDezal
{
    [DejalActivityView removeView];
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Successfully Refreshed"
                          message:nil
                          delegate:self
                          cancelButtonTitle:@"Dismiss"
                          otherButtonTitles:nil, nil];
    [alert show];
}

-(void)deleteDatabase
{
    
    //GetService *getService= (GetService*)[NSEntityDescription insertNewObjectForEntityForName:@"GetService" inManagedObjectContext:_managedObjectContext];
    NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
    [allCars setEntity:[NSEntityDescription entityForName:@"GetService" inManagedObjectContext:context ]];
    [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * cars = [context  executeFetchRequest:allCars error:&error];
    
    //error handling goes here
    for (NSManagedObject * car in cars) {
        [context  deleteObject:car];
    }
    NSError *saveError = nil;
    //   [_managedObjectContext save:&saveError];
    [context save:&saveError];
    
}

@end
