//
//  CustomerTable+CoreDataProperties.m
//  Lead Now
//
//  Created by Rakesh Jain on 18/02/16.
//  Copyright © 2016 Infocrats. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CustomerTable+CoreDataProperties.h"

@implementation CustomerTable (CoreDataProperties)

@dynamic accountNo;
@dynamic autoId;
@dynamic dateTimec;
@dynamic emailId;
@dynamic fName;
@dynamic lName;
@dynamic phNo;
@dynamic status1;
@dynamic statusCustomer;
@dynamic statuso;
@dynamic userkey;
@dynamic username;
@dynamic address1;
@dynamic address2;
@dynamic city;
@dynamic stateeee;

@end
