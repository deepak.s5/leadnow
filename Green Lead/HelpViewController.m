//
//  HelpViewController.m
//  Lead Now
//
//  Created by Rakesh Jain on 25/09/13.
//  Copyright (c) 2013 Infocrats. All rights reserved.
//

#import "HelpViewController.h"
#import "NewcustomerViewController.h"
#import "DeveloperViewController.h"
#import "AddleadViewController.h"
#import "SelectedServiceViewController.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    if ([[UIScreen mainScreen] bounds].size.height <= 736){
        self.height.constant = 60;
        
    }else{
        self.height.constant = 135;
        
        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backiPadNewCustomer:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)backiPadExistingCustomer:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)backAddLeadView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)backSelectedView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)loadHelp:(NSString *)loadAll
{
    txtView.text=loadAll;

}
-(IBAction)back
{
    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
