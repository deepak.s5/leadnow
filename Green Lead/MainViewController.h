//
//  MainViewController.h
//  Green Lead
//
//  Created by Rakesh Jain on 17/09/13.
//  Copyright (c) 2013 Infocrats. All rights reserved.
//

#import "FlipsideViewController.h"
#import "CacheManager.h"
#import "DejalActivityView.h"
@interface MainViewController : UIViewController <FlipsideViewControllerDelegate, UIPopoverControllerDelegate,UITextFieldDelegate,NSURLConnectionDelegate>

{
    IBOutlet UITextView *txtviewComment;
    CacheManager *cMgr;
    
  IBOutlet  UITextField *txtUsername,*txtPwd;
    
    BOOL isSelectNoBId;
  IBOutlet  UIButton *btnCheck;
   IBOutlet UITextField *tex1,*tex2;
    
     CGFloat     animatedDistance ;
    
    IBOutlet UIActivityIndicatorView *activity;
    
    
    IBOutlet UITextField *txtCompanykey;
}

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) UIPopoverController *flipsidePopoverController;

-(IBAction)login;
- (IBAction)SignUp:(id)sender;



@end
