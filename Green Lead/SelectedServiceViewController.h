//
//  SelectedServiceViewController.h
//  Green Lead
//
//  Created by Rakesh Jain on 24/09/13.
//  Copyright (c) 2013 Infocrats. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CacheManager.h"
#import "ServiceDetailTable.h"
#import "Imgpath.h"
#import <CoreData/CoreData.h>

@interface SelectedServiceViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate>

{
    NSEntityDescription *entityDesc1,*entityDesc2,*entityDesc3;
  
    UILabel *lblAccount;
    
    NSMutableArray * objectsMutablesw;
    CacheManager *cMgr;
    IBOutlet UITableView *tblView;
    UILabel *lblReferal;
    NSMutableArray *getReferal;
    
      NSMutableArray *getDecription;
    NSMutableArray *urgency;
    
     NSFetchRequest *request,*request1,*request2;
    NSArray *objects,*objects1;
    IBOutlet UILabel *lblAccountNum;
    NSString *  removalId;
    NSString *  chkforResponse;
    NSString *  imagNamelocal;
    NSString *localPath;
    NSString *  audioNamelocal;
    NSString *localaudioPath;
    NSMutableArray *RemoveIdv;
    NSString *strU;
    BOOL isAlreadySend;
    
}
- (IBAction)goToiPadHelp:(id)sender;
-(IBAction)back;
-(IBAction)submitTooutbox;
-(IBAction)addMore;
-(void)postdatatoserver;
@property (nonatomic, retain) NSManagedObjectContext  *managedObjectContext;
@property(nonatomic,retain)NSMutableArray *mutableArray;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height;


@end
