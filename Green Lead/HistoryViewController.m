//
//  HistoryViewController.m
//  AuditApp
//
//  Created by Rakesh Jain on 06/02/14.
//  Copyright (c) 2014 Rakesh Jain. All rights reserved.
//

#import "HistoryViewController.h"
#import "HistoryCell.h"
#import "JSONKit.h"
#import "AppDelegate.h"
#import "OutboxViewController.h"
#import "MyObjects.h"
#import "NIDropDown.h"
#import "DejalActivityView.h"

@interface HistoryViewController ()
{
    MyObjects *objClass;
    NSString *days;
    NSString *ckey;
    NSString *uName;
}
@end

@implementation HistoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont boldSystemFontOfSize:15.0];
        titleView.shadowColor = [UIColor clearColor];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        
    }
    titleView.text = title;
    [titleView sizeToFit];
}

- (void)viewDidLoad
{
     count=0;
    [super viewDidLoad];
       [tblView setBackgroundColor:[UIColor clearColor]];
    [self setTitle:@"My History"];
    [tblView setBackgroundColor:[UIColor clearColor]];

    //hide extra separators
    tblView.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];

    
    arrayData = [[NSMutableArray alloc] init];
    arrayTemp = [[NSMutableArray alloc] init];
    objClass = [MyObjects shareManager];
    arrayDropDown = [[NSMutableArray alloc] initWithObjects:@"Total Leads",@"New Leads",@"Pending Leads",@"Open Leads",@"Not Interested",@"First Attempt",@"Second Attempt", nil];
   // [self setBadge];
    responseData = [[NSMutableData alloc] init];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    days = [prefs stringForKey:@"keyToSaveDays"];
    ckey= [prefs stringForKey:@"key"];
    uName= [prefs stringForKey:@"name"];
    [self urlConnection];
    [activity setHidden:NO];
    [activity startAnimating];
    [DejalBezelActivityView activityViewForView:self.view];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    if ([[UIScreen mainScreen] bounds].size.height <= 736){
        self.height.constant = 60;
    }else{
        self.height.constant = 135;
        
    }
    
}
-(void)urlConnection
{
    @try {
        if (days.length<1) {
       //     NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://myleadnow.com/ServiceHandler.ashx?key=getleadhistory&Uname=%@&cKey=%@&ndays=%@",[uName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[ckey stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],@"100"]]];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?key=getleadhistory&Uname=%@&cKey=%@&ndays=%@",KLOGINTOAPP,[uName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[ckey stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],@"100"]]];

            [NSURLConnection connectionWithRequest:request delegate:self];
        }
        else{
         //   NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://myleadnow.com/ServiceHandler.ashx?key=getleadhistory&Uname=%@&cKey=%@&ndays=%@",[uName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[ckey stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],days]]];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?key=getleadhistory&Uname=%@&cKey=%@&ndays=%@",KLOGINTOAPP,[uName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[ckey stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],days]]];

            [NSURLConnection connectionWithRequest:request delegate:self];
        }
    }
    @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Sorry something went wrong" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [activity stopAnimating];
        [activity setHidden:YES];
        [DejalBezelActivityView removeView];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
    @finally {
        
    }
}
-(void)setBadge{
    _badgeView = [[M13BadgeView alloc] initWithFrame:CGRectMake(0, 0, 24.0, 24.0)];
    
    _badgeView.text = [NSString stringWithFormat:@"%d",objClass.LeadCountOutBox];
    if (objClass.LeadCountOutBox>0) {
        [btnOut addSubview:_badgeView];
    }
    else {
        for (UIView *view in [btnOut subviews]) {
            [view removeFromSuperview];
        }
    }
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
     [responseData setLength:0];
 }
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
     [responseData appendData:data];
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSString *strMsg;
    if (error) {
        strMsg = [NSString stringWithFormat:@"Please check your network connection and relaunch the application [%@]",error];
    }
    else
    {
        strMsg=[NSString stringWithFormat:@"Please check your network connection and relaunch the application"];
    }
     UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Error"
                          message:@"Please check your network connection and relaunch the application"
                          delegate:self
                          cancelButtonTitle:@"Dismiss"
                          otherButtonTitles:nil, nil];
    [alert show];
    [activity stopAnimating];
    [activity setHidden:YES];
    [DejalBezelActivityView removeView];
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [activity stopAnimating];
    [activity setHidden:YES];
    [DejalBezelActivityView removeView];
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    @try {
        NSData* jsonData = [NSData dataWithData:responseData];
        JSONDecoder* decoder = [[JSONDecoder alloc]
                                initWithParseOptions:JKParseOptionNone];
        NSArray* json = [decoder objectWithData:jsonData];
        NSError *error=nil;
        jsonArray =  [NSJSONSerialization JSONObjectWithData:responseData //1
                                                     options:kNilOptions
                                                       error:&error];
        
        for(NSDictionary *userInfo in json)
        {
            NSLog(@"json response inhistory will %@",userInfo);
            [arrayData addObject:userInfo];
            
        }
        arrayTemp=[arrayData mutableCopy];
        NSString *key = @"AccountNo";
        // [arrayData removeAllObjects];
        // arrayData=[sortedArray mutableCopy];
        [self performSelector:@selector(relodtbl) withObject:nil afterDelay:0.02];
    }
    @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Sorry something went wrong" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [activity stopAnimating];
        [activity setHidden:YES];
        [DejalBezelActivityView removeView];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
    @finally {
        
    }
}
-(void)sortByAcountNumber
{
    NSString *key = @"AccountNo";
    NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:brandDescriptor,nil];
    NSArray *sortedArray = [arrayData sortedArrayUsingDescriptors:sortDescriptors];
    
    [arrayData removeAllObjects];
    arrayData=[sortedArray mutableCopy];
  [self performSelector:@selector(relodtbl) withObject:nil afterDelay:0.02];
}
-(NSMutableArray *)sortArrayBasedOndate:(NSMutableArray *)arraytoSort
{
    NSDateFormatter *fmtDate = [[NSDateFormatter alloc] init];
    [fmtDate setDateFormat:@"dd-MM-yyyy"];
    
    NSDateFormatter *fmtTime = [[NSDateFormatter alloc] init];
    [fmtTime setDateFormat:@"HH:mm"];
    
    NSComparator compareDates = ^(id string1, id string2)
    {
        NSDate *date1 = [fmtDate dateFromString:string1];
        NSDate *date2 = [fmtDate dateFromString:string2];
        
        return [date1 compare:date2];
    };
    
    NSSortDescriptor * sortDesc1 = [[NSSortDescriptor alloc] initWithKey:@"start_date" ascending:YES comparator:compareDates];
   
    [arraytoSort sortUsingDescriptors:@[sortDesc1]];
    
    return arraytoSort;
}
-(void)relodtbl
{
    if ([arrayData count]<1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No data !" message:@"No data found for saved day limit" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    }
    [tblView reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([arrayData count]>0) {
        return [arrayData count];
    }
    else
    {
    return 0;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        NSDictionary *dict = [arrayData objectAtIndex:indexPath.row];
        NSString *comment =[dict valueForKey:@"Comment"];
        CGFloat whidt =  461;
        UIFont *FONT = [UIFont systemFontOfSize:15];
        NSAttributedString *attributedText =[[NSAttributedString alloc]  initWithString:comment  attributes:@  { NSFontAttributeName: FONT }];
        CGRect rect = [attributedText boundingRectWithSize:(CGSize){whidt, MAXFLOAT}
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                   context:nil];
        CGSize size = rect.size;
        if (size.height<=21) {
            return 101;
        } else {
            return size.height +101;
        }
    }
    else
    {
    NSDictionary *dict = [arrayData objectAtIndex:indexPath.row];
    NSString *comment =[dict valueForKey:@"Comment"];
    CGFloat whidt =  179;
    UIFont *FONT = [UIFont systemFontOfSize:14];
    NSAttributedString *attributedText =[[NSAttributedString alloc]  initWithString:comment  attributes:@  { NSFontAttributeName: FONT }];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){whidt, MAXFLOAT}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    CGSize size = rect.size;
    if (size.height<=21) {
        return 92;
    } else {
        return size.height +92;
    }
  }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier;// = @"HistoryCell";
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        simpleTableIdentifier = @"HistoryCell_iPad";
    }
    else
    {
        simpleTableIdentifier = @"HistoryCell";
        
    }
    HistoryCell *cell = (HistoryCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
         NSArray *nib;// = [[NSBundle mainBundle] loadNibNamed:@"HistoryCell" owner:self options:nil];
         if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            
            nib = [[NSBundle mainBundle] loadNibNamed:@"HistoryCell_iPad" owner:self options:nil];
        }
        else{
            nib = [[NSBundle mainBundle] loadNibNamed:@"HistoryCell" owner:self options:nil];
         }
        
        cell = [nib objectAtIndex:0];
    }
    if(indexPath.row%2==0)
    {
      // cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gridgreen.png"]];
    }
    else{
      // cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gridwhite.png"]];
    }
    NSDictionary *dict = [arrayData objectAtIndex:indexPath.row];
    
    if ([[dict valueForKey:@"FName"] isEqualToString:@""]) {
        cell.lblName.text= @"Look in PestPac";
        
    }else{
    cell.lblName.text= [NSString stringWithFormat:@"%@ %@",[dict valueForKey:@"FName"],[dict valueForKey:@"LName"]];
    }
    cell.lblAcNo.text=[NSString stringWithFormat:@"Act No: %@",[dict valueForKey:@"AccountNo"]];
    cell.lblDate.text=[dict valueForKey:@"SendDateTime"];
    cell.lblstatus.text=[dict valueForKey:@"Status"];
    CGSize size;
    CGFloat hi;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        NSString *comment =[dict valueForKey:@"Comment"];
        CGFloat whidt =  461;
        UIFont *FONT = [UIFont systemFontOfSize:15];
        NSAttributedString *attributedText =[[NSAttributedString alloc]  initWithString:comment  attributes:@  { NSFontAttributeName: FONT }];
        CGRect rect = [attributedText boundingRectWithSize:(CGSize){whidt, MAXFLOAT}
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                   context:nil];
        size = rect.size;
        hi = size.height;
    }
    else
    {
    NSString *comment =[dict valueForKey:@"Comment"];
    CGFloat whidt =  179;
    UIFont *FONT = [UIFont systemFontOfSize:14];
    NSAttributedString *attributedText =[[NSAttributedString alloc]  initWithString:comment  attributes:@  { NSFontAttributeName: FONT }];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){whidt, MAXFLOAT}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
     size = rect.size;
    hi = size.height;
    }
    if (size.height<=21) {
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            cell.lblComment.frame=CGRectMake(232, 65, 461, 21);
        }
        else
        {
        cell.lblComment.frame=CGRectMake(122, 64, 179, 21);
        }
    } else {
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            cell.lblComment.frame=CGRectMake(232, 65, 461, hi);
        }
        else
        {
        cell.lblComment.frame=CGRectMake(122, 64, 179, hi);
        }
    }
    cell.lblComment.text=[dict valueForKey:@"Comment"];
    [cell setBackgroundColor:[UIColor clearColor]];
//    if (indexPath.row == [arrayData count] - 1)
//    {
//        [self launchReload];
//    }
    return cell;
}
-(IBAction)selectClicked:(id)sender {
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:@"NotificationMessageEvent" object:nil];
    
    CGFloat f;
    if(dropDown == nil) {
                   f=280;
       
        
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arrayDropDown :nil :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}
- (void)niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}
-(void)viewWillAppear:(BOOL)animated
{
     [self.navigationController setNavigationBarHidden: YES animated:NO];
  }

-(IBAction)gotoOutBox:(id)sender
{
    
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"FromHistory"];
    [defs synchronize];
    
   // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
    UIStoryboard *storyboard;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    }
    else
    {
        
        storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
    }
    OutboxViewController *myVC = (OutboxViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Outbox"];
    
    [self.navigationController pushViewController:myVC animated:YES];
}

-(IBAction)back:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:NO];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    //[self.navigationController popViewControllerAnimated:NO];

}
-(IBAction)sortList:(id)sender
{
    UIActionSheet *actionsheet = [[UIActionSheet alloc] initWithTitle:@"Sort Leads" delegate:nil cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"All Leads" otherButtonTitles:@"New Leads",@"Pending Leads", nil];
    actionsheet.delegate=self;
    [actionsheet showInView:self.view];
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        if ([arrayData count]>0) {
            [arrayData removeAllObjects];
        }
        arrayData= [arrayTemp mutableCopy];
        [tblView reloadData];
        
    }
   else if (buttonIndex==1)
   {
       [arrayData removeAllObjects];
       for (NSDictionary *dict in arrayTemp) {
           
          if ([[dict valueForKey:@"Status"] isEqualToString:@"New"]) {
              [arrayData addObject:dict];
           }
       }
       [tblView reloadData];
   }
   else if (buttonIndex==2)
   {
       [arrayData removeAllObjects];
       for (NSDictionary *dict in arrayTemp) {
           
           if ([[dict valueForKey:@"Status"] isEqualToString:@"Pending"]) {
               [arrayData addObject:dict];
           }
       }
       [tblView reloadData];
   }
}
-(void)triggerAction:(NSNotification *) notification
{
    if ([notification.object isKindOfClass:[NSString class]])
    {
        NSLog(@"dict on notification---%@",notification.object);
       // NSString *dict = notification.object;
        // arrayDropDown = [[NSMutableArray alloc] initWithObjects:@"All Leads",@"New Leads",@"Pending Leads",@"Open Leads",@"Not Interested",@"First Attempt",@"Second Attempt", nil];

        if ([notification.object isEqualToString:@"Pending Leads"]) {
            [arrayData removeAllObjects];
            for (NSDictionary *dict in arrayTemp) {
                
                if ([[dict valueForKey:@"Status"] isEqualToString:@"Pending"]) {
                    
                    [arrayData addObject:dict];
                }
            }
            [tblView reloadData];
        }
        else if ([notification.object isEqualToString:@"Total Leads"]){
            if ([arrayData count]>0) {
                [arrayData removeAllObjects];
            }
            arrayData= [arrayTemp mutableCopy];
            [tblView reloadData];
        
        }
        else if ([notification.object isEqualToString:@"Open Leads"]){
        //Open
            [arrayData removeAllObjects];
            for (NSDictionary *dict in arrayTemp) {
                
                if ([[dict valueForKey:@"Status"] isEqualToString:@"Open"]) {
                    
                    [arrayData addObject:dict];
                }
            }
            [tblView reloadData];
         }
        else if ([notification.object isEqualToString:@"Not Interested"]){
            [arrayData removeAllObjects];
            for (NSDictionary *dict in arrayTemp) {
                
                if ([[dict valueForKey:@"Status"] isEqualToString:@"Not Interested"]) {
                    
                    [arrayData addObject:dict];
                    
                }
            }
            [tblView reloadData];

        
        }
        else if ([notification.object isEqualToString:@"First Attempt"]){
            [arrayData removeAllObjects];
            for (NSDictionary *dict in arrayTemp) {
                
                if ([[dict valueForKey:@"Status"] isEqualToString:@"First Attempt"]) {
                    
                    [arrayData addObject:dict];
                    
                }
            }
            [tblView reloadData];
            
        }
        else if ([notification.object isEqualToString:@"Second Attempt"]){
            [arrayData removeAllObjects];
            for (NSDictionary *dict in arrayTemp) {
                
                if ([[dict valueForKey:@"Status"] isEqualToString:@"Second Attempt"]) {
                    
                    [arrayData addObject:dict];
                    
                }
            }
            [tblView reloadData];
            
        }
        else if ([notification.object isEqualToString:@"New Leads"]){
            [arrayData removeAllObjects];
            for (NSDictionary *dict in arrayTemp) {
                
                if ([[dict valueForKey:@"Status"] isEqualToString:@"New"]) {
                    
                    [arrayData addObject:dict];
                    
                }
            }
            [tblView reloadData];
            
        }
           }
}
@end
