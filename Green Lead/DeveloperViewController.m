//
//  DeveloperViewController.m
//  Green Lead
//
//  Created by Rakesh Jain on 18/09/13.
//  Copyright (c) 2013 Infocrats. All rights reserved.
//

#import "DeveloperViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "MainViewController.h"
#import "HelpViewController.h"
#import "AddleadViewController.h"
#import "NewcustomerViewController.h"
#import "SettingView.h"
#import "JSONKit.h"
#import "HistoryViewController.h"
#import "MyObjects.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import "OutboxViewController.h"

#define MAXLENGTH 100

@interface DeveloperViewController ()
{
    MyObjects *objClass;
    NSFetchRequest *request12;
    NSSortDescriptor *sortDescriptor;
    NSEntityDescription *entity;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
}
@end

@implementation DeveloperViewController
@synthesize managedObjectContext=_managedObjectContext;
@synthesize tablView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)dismissKeyboard {
    [txtZip resignFirstResponder];
}
-(void)viewWillDisappear:(BOOL)animated{
    [_helpBtnNew setHidden:YES];
}

-(void)viewDidLoad
{
//    _txtEmailId.text=@"";
//    _txtFirstName.text=@"";
//    _txtLastName.text=@"";
//    _txtPhoneNo.text=@"";
    
    RemoveIdv = [[NSMutableArray alloc] init];
    //hide extra separators
    [_helpBtnNew setHidden:NO];
    tablView.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
    BOOL isAD_New_Customer =[defsss boolForKey:@"AD_New_Customer"];

//        if (isAD_New_Customer) {
//            [_lblExistingCustomer setHidden:YES];
//            [segment setHidden:NO];
//            UIStoryboard *storyboard;
//            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//            {
//                storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
//            }
//            else
//            {
//                storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
//            }
//    
//            NewcustomerViewController* lgn = (NewcustomerViewController*)[storyboard instantiateViewControllerWithIdentifier:@"new"];
//            [self.navigationController  pushViewController:lgn animated:NO];
//        }

//    [_lblExistingCustomer.layer setCornerRadius:5.0f];
//    [_lblExistingCustomer.layer setBorderColor:(__bridge CGColorRef _Nullable)([UIColor colorWithRed:89/255 green:115/255 blue:88/255 alpha:1])];
//    [_lblExistingCustomer.layer setBorderWidth:0.8f];
//    [_lblExistingCustomer.layer setShadowColor:(__bridge CGColorRef _Nullable)([UIColor colorWithRed:89/255 green:115/255 blue:88/255 alpha:1])];
//    [_lblExistingCustomer.layer setShadowOpacity:0.3];
//    [_lblExistingCustomer.layer setShadowRadius:3.0];
//    [_lblExistingCustomer.layer setShadowOffset:CGSizeMake(2.0, 2.0)];

    cMgr=[CacheManager getInstance];
    if ([cMgr.strUsreName isEqualToString:@""]) {
        cMgr.strUsreName=cMgr.cacheName;
        NSLog(@"cache %@",cMgr.cacheName);
        NSLog(@"sr %@",cMgr.strUsreName);
    } else {
    }
     NSLog(@"cache %@",cMgr.cacheName);
    NSLog(@" UserName FROM CAche %@",cMgr.strUsreName);
    appDelegate =
    (id)[[UIApplication sharedApplication] delegate];
    
    context = [appDelegate managedObjectContext];
    request = [[NSFetchRequest alloc] init];
    
    entity = [NSEntityDescription entityForName:@"GetService" inManagedObjectContext:context];
    
    [request setEntity:entity];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sErvice_Id" ascending:NO];
    
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    
    [self.fetchedResultsController setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    
    [self.fetchedResultsController performFetch:&error];
    
    arrAllObj = [self.fetchedResultsController fetchedObjects];
    
//    //    //new Change just to test
//    NSArray *arr = [arrAllObj valueForKey:@"sErviceName"];
//    NSString *strMsg = [NSString stringWithFormat:@"%@",arr];
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Services" message:strMsg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil , nil];
//    [alert show];
//    //    //end new change just to test

    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
//        //    //new Change just to test
//        NSString *strErr = [NSString stringWithFormat:@"%@",error];
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:strErr message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil , nil];
//        [alert show];
//        //    //end new change just to test

    }
    
  //  [self setBadge11];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    [super viewDidLoad];
  
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(setBadge)
//                                                 name:@"Badge"
//                                               object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(setBadge11)
//                                                 name:@"Badge11"
//                                               object:nil];
//    
    objClass = [MyObjects shareManager];
    if ([objClass.ArrOutBox count]<1) {
         objClass.ArrOutBox= [[NSMutableArray alloc] init];
    }
   
    arrayForsendinga=[[NSMutableArray alloc]init];
    
    objectsMutablesw=[[NSMutableArray alloc]init];
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            [tablView setFrame:CGRectMake(tablView.frame.origin.x, tablView.frame.origin.y, tablView.frame.size.width, 130)];
            
        }
    }
    tabbarController.delegate=self;
    self.tabBarController.delegate=self;
    tablView.delegate=self;
    tablView.dataSource=self;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        
//        UIImage *bottomBarImage = [UIImage imageNamed:@"blank-FooterNew.png"];
//
//
//        homeTabImageView=[[UIImageView alloc]initWithFrame:CGRectMake(200, 1, 35, 35)];
//        homeTabImageView.image=[UIImage imageNamed:@"home.png"];
//        lbl=[[UILabel alloc]initWithFrame:CGRectMake(200,31,122,20)];
//
//        lbl.font=[UIFont systemFontOfSize:12.0];
//        lbl.textColor = [UIColor whiteColor];
//        lbl.font= [UIFont fontWithName:@"Arial-BoldMT" size:12];
//        lbl.text=@"Home";
//        lbl.backgroundColor=[UIColor clearColor];
//
//        settingViewImg=[[UIImageView alloc]initWithFrame:CGRectMake(310,1, 35, 35)];
//        settingViewImg.image=[UIImage imageNamed:@"setting_icon.png"];
//        lblSetting=[[UILabel alloc]initWithFrame:CGRectMake(306,31,122,20)];
//
//
//        lblSetting.textColor = [UIColor whiteColor];
//        lblSetting.font= [UIFont fontWithName:@"Arial-BoldMT" size:12];
//
//        lblSetting.text=@"Setting";
//        lblSetting.backgroundColor=[UIColor clearColor];
//
//        developerTabImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, UIScreen.mainScreen.bounds.size.width, 50)];
//        developerTabImageView.image=bottomBarImage;
//
//        bottomBarImage = [UIImage imageNamed:@"history_icon.png"];
//        outboxImageView = [[UIImageView alloc]initWithFrame:CGRectMake(420,3, 35, 35)];
//        outboxImageView.image=bottomBarImage;
//        UILabel   *lbl1=[[UILabel alloc]initWithFrame:CGRectMake(415,29,122,23)];
//        // lbl1.font=[UIFont systemFontOfSize:12.0];
//        lbl1.font= [UIFont fontWithName:@"Arial-BoldMT" size:12];
//        lbl1.textColor = [UIColor whiteColor];
//        lbl1.text=@"History";
//        lbl1.backgroundColor=[UIColor clearColor];
//
//        bottomBarImage = [UIImage imageNamed:@"logout_icon.png"];
//
//        logoutboxImageView=[[UIImageView alloc]initWithFrame:CGRectMake(520,1, 35, 35)];
//        logoutboxImageView.image=bottomBarImage;
//        UILabel   *lbl2=[[UILabel alloc]initWithFrame:CGRectMake(520,29,123,23)];
//        lbl2.textColor = [UIColor whiteColor];
//        lbl2.font= [UIFont fontWithName:@"Arial-BoldMT" size:12];
//        lbl2.text=@"Log out";
//        lbl2.backgroundColor=[UIColor clearColor];
//        UIButton *btnLogOut = [UIButton buttonWithType:UIButtonTypeCustom];
//        //btnLogOut =[[UIButton alloc]initWithFrame:CGRectMake(250,0, 70, 70)];
//
//        UIButton *btnHistory = [UIButton buttonWithType:UIButtonTypeCustom];
//        // btnHistory =[[UIButton alloc]initWithFrame:CGRectMake(175,0, 70,90)];
//
//        UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
//        //btnSetting =[[UIButton alloc]initWithFrame:CGRectMake(100,0, 70, 70)];
//
//        [btnHistory setImage:[UIImage imageNamed:@"new_login.png"] forState:UIControlStateNormal];
//        [btnHistory setBackgroundImage:[UIImage imageNamed:@"new_login.png"] forState:UIControlStateNormal];
//
//        [btnLogOut setImage:[UIImage imageNamed:@"new_login.png"] forState:UIControlStateNormal];
//         [btnLogOut setBackgroundImage:[UIImage imageNamed:@"new_login.png"] forState:UIControlStateNormal];
//
//        [btnSetting setImage:[UIImage imageNamed:@"new_login.png"] forState:UIControlStateNormal];
//         [btnSetting setBackgroundImage:[UIImage imageNamed:@"new_login.png"] forState:UIControlStateNormal];
//
//
//
//
//            btnLogOut =[[UIButton alloc]initWithFrame:CGRectMake(450,0, 130, 70)];
//            btnHistory =[[UIButton alloc]initWithFrame:CGRectMake(375,0, 100,90)];
//            btnSetting =[[UIButton alloc]initWithFrame:CGRectMake(280,0, 100, 70)];
//
//
//        [[self tabBarController].tabBar addSubview:developerTabImageView];
//        [[self tabBarController].tabBar addSubview:homeTabImageView];
//        [[self tabBarController].tabBar addSubview:lbl];
//
//        [[self tabBarController].tabBar addSubview:outboxImageView];
//
//        [[self tabBarController].tabBar addSubview:lbl1];
//
//        [[self tabBarController].tabBar addSubview:logoutboxImageView];
//        [[self tabBarController].tabBar addSubview:settingViewImg];
//        [[self tabBarController].tabBar addSubview:homeTabImageView];
//        [[self tabBarController].tabBar addSubview:lblSetting];
//        [[self tabBarController].tabBar addSubview:lbl2];
//        [btnLogOut addTarget:self action:@selector(logoutClicked) forControlEvents:UIControlEventTouchUpInside];
//
//        [[self tabBarController].tabBar addSubview:btnLogOut];
//
//        [btnSetting addTarget:self action:@selector(goTosetting) forControlEvents:UIControlEventTouchUpInside];
//        [[self tabBarController].tabBar addSubview:btnSetting];
//
//        [btnHistory addTarget:self action:@selector(gotoHistory) forControlEvents:UIControlEventTouchUpInside];
//        [[self tabBarController].tabBar addSubview:btnHistory];
      
        //MARK: Chnages by sourabh
        
        UIImage *bottomBarImage = [UIImage imageNamed:@"blank-FooterNew.png"];
        
        
        homeTabImageView=[[UIImageView alloc]init];
        homeTabImageView.image=[UIImage imageNamed:@"home.png"];
        lbl=[[UILabel alloc] init];
        
        lbl.font=[UIFont systemFontOfSize:12.0];
        lbl.textColor = [UIColor whiteColor];
        lbl.font= [UIFont fontWithName:@"Arial-BoldMT" size:16];
        lbl.text=@"Home";
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.backgroundColor=[UIColor clearColor];
        
        
        // Setting
        settingViewImg=[[UIImageView alloc]init];
        settingViewImg.image=[UIImage imageNamed:@"setting_icon.png"];
        lblSetting=[[UILabel alloc]init];
        
        
        lblSetting.textColor = [UIColor whiteColor];
        lblSetting.font= [UIFont fontWithName:@"Arial-BoldMT" size:16];
        
        lblSetting.text=@"Setting";
        lblSetting.backgroundColor=[UIColor clearColor];
        lblSetting.textAlignment = NSTextAlignmentCenter;
        
        developerTabImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, UIScreen.mainScreen.bounds.size.width, 70)];
        developerTabImageView.image=bottomBarImage;
        
        bottomBarImage = [UIImage imageNamed:@"history_icon.png"];
        outboxImageView = [[UIImageView alloc]init];
        outboxImageView.image=bottomBarImage;
        UILabel   *lbl1=[[UILabel alloc]init];
        // lbl1.font=[UIFont systemFontOfSize:12.0];
        lbl1.font= [UIFont fontWithName:@"Arial-BoldMT" size:16];
        lbl1.textColor = [UIColor whiteColor];
        lbl1.text=@"History";
        lbl1.backgroundColor=[UIColor clearColor];
        lbl1.textAlignment = NSTextAlignmentCenter;
        
        bottomBarImage = [UIImage imageNamed:@"logout_icon.png"];
        
        logoutboxImageView=[[UIImageView alloc]init];
        logoutboxImageView.image=bottomBarImage;
        UILabel   *lbl2=[[UILabel alloc]init];
        lbl2.textColor = [UIColor whiteColor];
        lbl2.font= [UIFont fontWithName:@"Arial-BoldMT" size:16];
        lbl2.text=@"Log out";
        lbl2.backgroundColor=[UIColor clearColor];
        lbl2.textAlignment = NSTextAlignmentCenter;
        
        UIButton *btnLogOut = [UIButton buttonWithType:UIButtonTypeCustom];
        //btnLogOut =[[UIButton alloc]initWithFrame:CGRectMake(250,0, 70, 70)];
        
        btnHistory  = [UIButton buttonWithType:UIButtonTypeCustom];
        // btnHistory =[[UIButton alloc]initWithFrame:CGRectMake(175,0, 70,90)];
        
        UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
        //btnSetting =[[UIButton alloc]initWithFrame:CGRectMake(100,0, 70, 70)];
        
        [btnLogOut setBackgroundColor:[UIColor redColor]];
        [btnHistory setBackgroundColor:[UIColor yellowColor]];
        [btnSetting setBackgroundColor:[UIColor blueColor]];
        btnLogOut =[[UIButton alloc]init];
        btnHistory =[[UIButton alloc]init];
        btnSetting =[[UIButton alloc]init];
        
//
        //MARK: Changes Sourabh
        
        //MARK: For Home  --------------------------------------------------------------------------------->>>>
        [[self tabBarController].tabBar addSubview:developerTabImageView];
        
        UIStackView *stackViewHome = [[UIStackView alloc] init];
        
        stackViewHome.axis = UILayoutConstraintAxisVertical;
        stackViewHome.distribution = UIStackViewDistributionEqualSpacing;
        stackViewHome.alignment = UIStackViewAlignmentCenter;
        stackViewHome.spacing = 0;
        
        
        [lbl.heightAnchor constraintEqualToConstant:20].active = true;
        [lbl.widthAnchor constraintEqualToConstant:122].active = true;
        
        [homeTabImageView.heightAnchor constraintEqualToConstant:25].active = true;
        [homeTabImageView.widthAnchor constraintEqualToConstant:25].active = true;

        
        [stackViewHome addArrangedSubview:homeTabImageView];
        [stackViewHome addArrangedSubview:lbl];
        
        stackViewHome.translatesAutoresizingMaskIntoConstraints = false;
        [self.view addSubview:stackViewHome];
        
        
        //Layout for Stack View
        [stackViewHome.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = true;
        [stackViewHome.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor].active = true;
      
        
       // [[self tabBarController].tabBar addSubview:stackViewHome];
        
        //MARK: For Setting --------------------------------------------------------------------------------->>>>
        
        UIStackView *stackViewSetting = [[UIStackView alloc] init];
        
        stackViewSetting.axis = UILayoutConstraintAxisVertical;
        stackViewSetting.distribution = UIStackViewDistributionEqualSpacing;
        stackViewSetting.alignment = UIStackViewAlignmentCenter;
        stackViewSetting.spacing = 0;
        
        
        [lblSetting.heightAnchor constraintEqualToConstant:20].active = true;
        [lblSetting.widthAnchor constraintEqualToConstant:250].active = true;
        
        [settingViewImg.heightAnchor constraintEqualToConstant:25].active = true;
        [settingViewImg.widthAnchor constraintEqualToConstant:25].active = true;

        
        [stackViewSetting addArrangedSubview:settingViewImg];
        [stackViewSetting addArrangedSubview:lblSetting];
        
        stackViewSetting.translatesAutoresizingMaskIntoConstraints = false;
        [self.view addSubview:stackViewSetting];
        
        
        //Layout for Stack View
        [stackViewSetting.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = true;
        [stackViewSetting.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor].active = true;
      
        
       // [[self tabBarController].tabBar addSubview:stackViewSetting];
        
        
        //MARK: For History --------------------------------------------------------------------------------->>>>
        
        UIStackView *stackViewHistory = [[UIStackView alloc] init];
        
        stackViewHistory.axis = UILayoutConstraintAxisVertical;
        stackViewHistory.distribution = UIStackViewDistributionEqualSpacing;
        stackViewHistory.alignment = UIStackViewAlignmentCenter;
        stackViewHistory.spacing = 0;
        
        
        [lbl1.heightAnchor constraintEqualToConstant:20].active = true;
        [lbl1.widthAnchor constraintEqualToConstant:500].active = true;
        
        [outboxImageView.heightAnchor constraintEqualToConstant:25].active = true;
        [outboxImageView.widthAnchor constraintEqualToConstant:25].active = true;

        [stackViewHistory addArrangedSubview:outboxImageView];
        [stackViewHistory addArrangedSubview:lbl1];
        
        stackViewHistory.translatesAutoresizingMaskIntoConstraints = false;
        [self.view addSubview:stackViewHistory];
        
        
        //Layout for Stack View
        [stackViewHistory.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = true;
        [stackViewHistory.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor].active = true;
      
        
       // [[self tabBarController].tabBar addSubview:stackViewHistory];
        
        //MARK: For LogOut --------------------------------------------------------------------------------->>>>
        
        UIStackView *stackViewLogOut = [[UIStackView alloc] init];
        
        stackViewLogOut.axis = UILayoutConstraintAxisVertical;
        stackViewLogOut.distribution = UIStackViewDistributionEqualSpacing;
        stackViewLogOut.alignment = UIStackViewAlignmentCenter;
        stackViewLogOut.spacing = 0;
        
        
        [lbl2.heightAnchor constraintEqualToConstant:20].active = true;
        [lbl2.widthAnchor constraintEqualToConstant:600].active = true;
        
        [logoutboxImageView.heightAnchor constraintEqualToConstant:25].active = true;
        [logoutboxImageView.widthAnchor constraintEqualToConstant:25].active = true;

        [stackViewLogOut addArrangedSubview:logoutboxImageView];
        [stackViewLogOut addArrangedSubview:lbl2];
        
        stackViewLogOut.translatesAutoresizingMaskIntoConstraints = false;
        [self.view addSubview:stackViewLogOut];
        
        
        //Layout for Stack View
        [stackViewLogOut.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = true;
        [stackViewLogOut.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor].active = true;
      
        
        //[[self tabBarController].tabBar addSubview:stackViewLogOut];
        
        
        //MARK: For Horizontal StackView --------------------------------------------------------------------------------->>>>
        
        UIStackView *stackView = [[UIStackView alloc]initWithFrame:CGRectMake(0,0, UIScreen.mainScreen.bounds.size.width, 50)];
        stackView.axis = UILayoutConstraintAxisHorizontal;
        stackView.alignment = UIStackViewAlignmentFill;
        stackView.distribution = UIStackViewDistributionFillEqually;
        stackViewLogOut.spacing = 5;
        
        [stackView addArrangedSubview:stackViewHome];
        [stackView addArrangedSubview:stackViewSetting];
        [stackView addArrangedSubview:stackViewHistory];
        [stackView addArrangedSubview:stackViewLogOut];
        
        [self.view addSubview:stackView];
        
        NSLog(@"dsjflskdjflkajsdf");
        NSLog(@"%f",[[UIScreen mainScreen] bounds].size.width) ;
        
//        if ([[UIScreen mainScreen] bounds].size.height <= 820){
//
//        }else{
//            stackView.center = developerTabImageView.center;
//        }
//
        
       // stackView.center = developerTabImageView.center;
        
        [[self tabBarController].tabBar addSubview:stackView];
        
//        [[self tabBarController].tabBar addSubview:lbl];
//
//        [[self tabBarController].tabBar addSubview:outboxImageView];
//
//        [[self tabBarController].tabBar addSubview:lbl1];
//
//        [[self tabBarController].tabBar addSubview:logoutboxImageView];
//        [[self tabBarController].tabBar addSubview:settingViewImg];
//        [[self tabBarController].tabBar addSubview:homeTabImageView];
//        [[self tabBarController].tabBar addSubview:lblSetting];
//        [[self tabBarController].tabBar addSubview:lbl2];
        
//        [btnLogOut addTarget:self action:@selector(logoutClicked) forControlEvents:UIControlEventTouchUpInside];
//
//        [[self tabBarController].tabBar addSubview:btnLogOut];
//
//        [btnSetting addTarget:self action:@selector(goTosetting) forControlEvents:UIControlEventTouchUpInside];
//        [[self tabBarController].tabBar addSubview:btnSetting];
//
//
//        [btnHistory addTarget:self action:@selector(gotoHistory) forControlEvents:UIControlEventTouchUpInside];
//        [[self tabBarController].tabBar addSubview:btnHistory];
            
        //[self setBadge];
        UITapGestureRecognizer *TapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(logoutClicked)];
        [stackViewLogOut addGestureRecognizer:TapRecognizer];
        
        UITapGestureRecognizer *TapRecognizerHistory = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoHistory)];
        [stackViewHistory addGestureRecognizer:TapRecognizerHistory];
        
        UITapGestureRecognizer *TapRecognizerSetting = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goTosetting)];
        [stackViewSetting addGestureRecognizer:TapRecognizerSetting];
        
        UITapGestureRecognizer *TapRecognizerHome = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToHome)];
        [stackViewHome addGestureRecognizer:TapRecognizerHome];
        

    }
    else
    {
        
        
        UIImage *bottomBarImage = [UIImage imageNamed:@"blank-FooterNew.png"];
        
        
        homeTabImageView=[[UIImageView alloc]init];
        homeTabImageView.image=[UIImage imageNamed:@"home.png"];
        lbl=[[UILabel alloc] init];
        
        lbl.font=[UIFont systemFontOfSize:12.0];
        lbl.textColor = [UIColor whiteColor];
        lbl.font= [UIFont fontWithName:@"Arial-BoldMT" size:12];
        lbl.text=@"Home";
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.backgroundColor=[UIColor clearColor];
        
        
        // Setting
        settingViewImg=[[UIImageView alloc]init];
        settingViewImg.image=[UIImage imageNamed:@"setting_icon.png"];
        lblSetting=[[UILabel alloc]init];
        
        
        lblSetting.textColor = [UIColor whiteColor];
        lblSetting.font= [UIFont fontWithName:@"Arial-BoldMT" size:12];
        
        lblSetting.text=@"Setting";
        lblSetting.backgroundColor=[UIColor clearColor];
        lblSetting.textAlignment = NSTextAlignmentCenter;
        
        developerTabImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, UIScreen.mainScreen.bounds.size.width, 85)];
        developerTabImageView.image=bottomBarImage;
        
        bottomBarImage = [UIImage imageNamed:@"history_icon.png"];
        outboxImageView = [[UIImageView alloc]init];
        outboxImageView.image=bottomBarImage;
        UILabel   *lbl1=[[UILabel alloc]init];
        // lbl1.font=[UIFont systemFontOfSize:12.0];
        lbl1.font= [UIFont fontWithName:@"Arial-BoldMT" size:12];
        lbl1.textColor = [UIColor whiteColor];
        lbl1.text=@"History";
        lbl1.backgroundColor=[UIColor clearColor];
        lbl1.textAlignment = NSTextAlignmentCenter;
        
        bottomBarImage = [UIImage imageNamed:@"logout_icon.png"];
        
        logoutboxImageView=[[UIImageView alloc]init];
        logoutboxImageView.image=bottomBarImage;
        UILabel   *lbl2=[[UILabel alloc]init];
        lbl2.textColor = [UIColor whiteColor];
        lbl2.font= [UIFont fontWithName:@"Arial-BoldMT" size:12];
        lbl2.text=@"Log out";
        lbl2.backgroundColor=[UIColor clearColor];
        lbl2.textAlignment = NSTextAlignmentCenter;
        
        UIButton *btnLogOut = [UIButton buttonWithType:UIButtonTypeCustom];
        //btnLogOut =[[UIButton alloc]initWithFrame:CGRectMake(250,0, 70, 70)];
        
        btnHistory  = [UIButton buttonWithType:UIButtonTypeCustom];
        // btnHistory =[[UIButton alloc]initWithFrame:CGRectMake(175,0, 70,90)];
        
        UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
        //btnSetting =[[UIButton alloc]initWithFrame:CGRectMake(100,0, 70, 70)];
        
        [btnLogOut setBackgroundColor:[UIColor redColor]];
        [btnHistory setBackgroundColor:[UIColor yellowColor]];
        [btnSetting setBackgroundColor:[UIColor blueColor]];
        btnLogOut =[[UIButton alloc] init];
        btnHistory =[[UIButton alloc] init];
        btnSetting =[[UIButton alloc] init];
        
//
        //MARK: Changes Sourabh
        
        //MARK: For Home  --------------------------------------------------------------------------------->>>>
        [[self tabBarController].tabBar addSubview:developerTabImageView];
        
        UIStackView *stackViewHome = [[UIStackView alloc] init];
        
        stackViewHome.axis = UILayoutConstraintAxisVertical;
        stackViewHome.distribution = UIStackViewDistributionEqualSpacing;
        stackViewHome.alignment = UIStackViewAlignmentCenter;
        stackViewHome.spacing = 0;
        
        
        [lbl.heightAnchor constraintEqualToConstant:20].active = true;
        [lbl.widthAnchor constraintEqualToConstant:122].active = true;
        
        [homeTabImageView.heightAnchor constraintEqualToConstant:25].active = true;
        [homeTabImageView.widthAnchor constraintEqualToConstant:25].active = true;

        
        [stackViewHome addArrangedSubview:homeTabImageView];
        [stackViewHome addArrangedSubview:lbl];
        
        stackViewHome.translatesAutoresizingMaskIntoConstraints = false;
        [self.view addSubview:stackViewHome];
        
        
        //Layout for Stack View
        [stackViewHome.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = true;
        [stackViewHome.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor].active = true;
      
        
       // [[self tabBarController].tabBar addSubview:stackViewHome];
        
        //MARK: For Setting --------------------------------------------------------------------------------->>>>
        
        UIStackView *stackViewSetting = [[UIStackView alloc] init];
        
        stackViewSetting.axis = UILayoutConstraintAxisVertical;
        stackViewSetting.distribution = UIStackViewDistributionEqualSpacing;
        stackViewSetting.alignment = UIStackViewAlignmentCenter;
        stackViewSetting.spacing = 0;
        
        
        [lblSetting.heightAnchor constraintEqualToConstant:20].active = true;
        [lblSetting.widthAnchor constraintEqualToConstant:250].active = true;
        
        [settingViewImg.heightAnchor constraintEqualToConstant:25].active = true;
        [settingViewImg.widthAnchor constraintEqualToConstant:25].active = true;

        
        [stackViewSetting addArrangedSubview:settingViewImg];
        [stackViewSetting addArrangedSubview:lblSetting];
        
        stackViewSetting.translatesAutoresizingMaskIntoConstraints = false;
        [self.view addSubview:stackViewSetting];
        
        
        //Layout for Stack View
        [stackViewSetting.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = true;
        [stackViewSetting.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor].active = true;
      
        
       // [[self tabBarController].tabBar addSubview:stackViewSetting];
        
        
        //MARK: For History --------------------------------------------------------------------------------->>>>
        
        UIStackView *stackViewHistory = [[UIStackView alloc] init];
        
        stackViewHistory.axis = UILayoutConstraintAxisVertical;
        stackViewHistory.distribution = UIStackViewDistributionEqualSpacing;
        stackViewHistory.alignment = UIStackViewAlignmentCenter;
        stackViewHistory.spacing = 0;
        
        
        [lbl1.heightAnchor constraintEqualToConstant:20].active = true;
        [lbl1.widthAnchor constraintEqualToConstant:500].active = true;
        
        [outboxImageView.heightAnchor constraintEqualToConstant:25].active = true;
        [outboxImageView.widthAnchor constraintEqualToConstant:25].active = true;

        [stackViewHistory addArrangedSubview:outboxImageView];
        [stackViewHistory addArrangedSubview:lbl1];
        
        stackViewHistory.translatesAutoresizingMaskIntoConstraints = false;
        [self.view addSubview:stackViewHistory];
        
        
        //Layout for Stack View
        [stackViewHistory.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = true;
        [stackViewHistory.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor].active = true;
      
        
       // [[self tabBarController].tabBar addSubview:stackViewHistory];
        
        //MARK: For LogOut --------------------------------------------------------------------------------->>>>
        
        UIStackView *stackViewLogOut = [[UIStackView alloc] init];
        
        stackViewLogOut.axis = UILayoutConstraintAxisVertical;
        stackViewLogOut.distribution = UIStackViewDistributionEqualSpacing;
        stackViewLogOut.alignment = UIStackViewAlignmentCenter;
        stackViewLogOut.spacing = 0;
        
        
        [lbl2.heightAnchor constraintEqualToConstant:20].active = true;
        [lbl2.widthAnchor constraintEqualToConstant:600].active = true;
        
        [logoutboxImageView.heightAnchor constraintEqualToConstant:25].active = true;
        [logoutboxImageView.widthAnchor constraintEqualToConstant:25].active = true;

        [stackViewLogOut addArrangedSubview:logoutboxImageView];
        [stackViewLogOut addArrangedSubview:lbl2];
        
        stackViewLogOut.translatesAutoresizingMaskIntoConstraints = false;
        [self.view addSubview:stackViewLogOut];
        
        
        //Layout for Stack View
        [stackViewLogOut.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = true;
        [stackViewLogOut.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor].active = true;
      
        
        //[[self tabBarController].tabBar addSubview:stackViewLogOut];
        
        
        //MARK: For Horizontal StackView --------------------------------------------------------------------------------->>>>
        
        UIStackView *stackView = [[UIStackView alloc]initWithFrame:CGRectMake(0,0, UIScreen.mainScreen.bounds.size.width, 50)];
        stackView.axis = UILayoutConstraintAxisHorizontal;
        stackView.alignment = UIStackViewAlignmentFill;
        stackView.distribution = UIStackViewDistributionFillEqually;
        stackViewLogOut.spacing = 5;
        
        [stackView addArrangedSubview:stackViewHome];
        [stackView addArrangedSubview:stackViewSetting];
        [stackView addArrangedSubview:stackViewHistory];
        [stackView addArrangedSubview:stackViewLogOut];
        
        [self.view addSubview:stackView];
        
        if ([[UIScreen mainScreen] bounds].size.height <= 736){
            self.heightImage.constant = 60;
        }else{
            self.heightImage.constant = 135;
            stackView.center = developerTabImageView.center;
        }
        
      //  stackView.center = developerTabImageView.center;
        [[self tabBarController].tabBar addSubview:stackView];
        
//        [[self tabBarController].tabBar addSubview:lbl];
//
//        [[self tabBarController].tabBar addSubview:outboxImageView];
//
//        [[self tabBarController].tabBar addSubview:lbl1];
//
//        [[self tabBarController].tabBar addSubview:logoutboxImageView];
//        [[self tabBarController].tabBar addSubview:settingViewImg];
//        [[self tabBarController].tabBar addSubview:homeTabImageView];
//        [[self tabBarController].tabBar addSubview:lblSetting];
//        [[self tabBarController].tabBar addSubview:lbl2];
        
//        [btnLogOut addTarget:self action:@selector(logoutClicked) forControlEvents:UIControlEventTouchUpInside];
//
//        [[self tabBarController].tabBar addSubview:btnLogOut];
//
//        [btnSetting addTarget:self action:@selector(goTosetting) forControlEvents:UIControlEventTouchUpInside];
//        [[self tabBarController].tabBar addSubview:btnSetting];
//
//
//        [btnHistory addTarget:self action:@selector(gotoHistory) forControlEvents:UIControlEventTouchUpInside];
//        [[self tabBarController].tabBar addSubview:btnHistory];
            
        //[self setBadge];
        UITapGestureRecognizer *TapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(logoutClicked)];
        [stackViewLogOut addGestureRecognizer:TapRecognizer];
        
        UITapGestureRecognizer *TapRecognizerHistory = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoHistory)];
        [stackViewHistory addGestureRecognizer:TapRecognizerHistory];
        
        UITapGestureRecognizer *TapRecognizerSetting = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goTosetting)];
        [stackViewSetting addGestureRecognizer:TapRecognizerSetting];
        
        UITapGestureRecognizer *TapRecognizerHome = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToHome)];
        [stackViewHome addGestureRecognizer:TapRecognizerHome];
        
    }
    
    CALayer *imageLayer = txtviewComment.layer;
    [imageLayer setCornerRadius:1];
    [imageLayer setBorderWidth:1];
    imageLayer.borderColor=[[UIColor lightGrayColor] CGColor];
    
    request = [[NSFetchRequest alloc] init];
    
    NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
    
    BOOL isZippCoce = [def boolForKey:@"AD_ZipCode"];
    
    if (isZippCoce) {
        lblZipTitle.hidden=NO;
        imgZipBackground.hidden=NO;
        txtZip.hidden=NO;
    }
    BOOL isAD_Customer_Detail = [def boolForKey:@"AD_Customer_Detail"];
    if (isAD_Customer_Detail) {
        _lblEmail.hidden=NO;
        _lblFirstName.hidden=NO;
        _lblLastName.hidden=NO;
        _lblPhone.hidden=NO;
        
        _imgEmail.hidden=NO;
        _imgfirst.hidden=NO;
        _imgLast.hidden=NO;
        _imgPhone.hidden=NO;
        
        _txtEmailId.hidden=NO;
        _txtFirstName.hidden=NO;
        _txtLastName.hidden=NO;
        _txtPhoneNo.hidden=NO;
    }
    [txtZip setKeyboardType:UIKeyboardTypePhonePad];
    // [def setObject:[arr objectAtIndex:1] forKey:@"url"];
    
   strU=[def stringForKey:@"url"];
    
   appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _managedObjectContext= [appDelegate managedObjectContext];

    
   // [self deleteDatabase];
    
    [self getService];
    
    NSString *strCompanyName=[NSString stringWithFormat:@"%@",[defsss valueForKey:@"Company_Name"]];
    
    _lblCompanyName.text=strCompanyName;
    
    isGoToOutBoxCall = NO;
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *isTerminated = [prefs stringForKey:@"isTerminated"];
    if ([isTerminated isEqual:@"YES"]) {
        [prefs setObject:@"NO" forKey:@"isTerminated"];
        [prefs synchronize];
        [self goToOutBox];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(goToOutBoxNotification:)
                                                 name:@"GoToOutBox"
                                               object:nil];
    

}

-(void)goToOutBoxNotification:(NSNotification *) notification
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"GoToOutBox" object:nil];
    isGoToOutBoxCall = NO;
    [self goToOutBox];
}

-(void)goToOutBox
{
    if (isGoToOutBoxCall == NO) {
        //Other Samples for NSUserDefaults
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *isLeadsInOutbox = [prefs stringForKey:@"isLeadsInOutbox"];
        if ([isLeadsInOutbox isEqual:@"YES"]) {
            NSLog(@"Redirect to Outbox");
            isGoToOutBoxCall = YES;
           // [self logoutButtonPressed];

            UIStoryboard *storyboard;
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
            }else {
                storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
            }
            
            OutboxViewController *myVC = (OutboxViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Outbox"];
            [self.navigationController pushViewController:myVC animated:YES];
        }
    }
}

- (void)logoutButtonPressed
{
     UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Logout"
                                 message:@"Are You Sure Want to Logout!"
                                 preferredStyle:UIAlertControllerStyleAlert];

    //Add Buttons
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];

    //Add your buttons to alert controller
    [alert addAction:noButton];

    [self presentViewController:alert animated:YES completion:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [_helpBtnNew setHidden:NO];
    // [DejalBezelActivityView activityViewForView:self.view];
    NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
    BOOL isAD_Existing_Customer =[defsss boolForKey:@"AD_Existing_Customer"];
    BOOL isAD_New_Customer =[defsss boolForKey:@"AD_New_Customer"];
    BOOL isAD_Both =[defsss boolForKey:@"AD_Both"];
    if (isAD_Existing_Customer) {
        [_lblExistingCustomer setHidden:NO];
        [segment setHidden:YES];
    }
    if (isAD_New_Customer) {
        
        //  [DejalActivityView removeView];
        [_lblExistingCustomer setHidden:YES];
        [segment setHidden:NO];
        UIStoryboard *storyboard;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
        }
        else
        {
            storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
        }
        
        NewcustomerViewController* lgn = (NewcustomerViewController*)[storyboard instantiateViewControllerWithIdentifier:@"new"];
        [self.navigationController  pushViewController:lgn animated:NO];
    }else
    {
        // [DejalActivityView removeView];
    }
    
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    BOOL isAD_Customer_Detail = [defs boolForKey:@"AD_Customer_Detail"];
    if (!isAD_Customer_Detail) {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenHeight = screenRect.size.height;
        CGFloat screenWidth = screenRect.size.width;
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            _addLeadBtnnn.frame = CGRectMake(287, 257, 195, 45);
            tablView.frame = CGRectMake(116,298, 537,646);
            //            if(screenWidth==1024)
            //            {
            //                tablView.frame = CGRectMake(106,298, 557,900);
            //            }
        }
        else
        {
            if ([UIScreen mainScreen].bounds.size.height==480)
            {
                _addLeadBtnnn.frame = CGRectMake(98, 180, 124, 30);
                tablView.frame = CGRectMake(12,240, 295,190);
            }
            else
            {
                _addLeadBtnnn.frame = CGRectMake(98, 180, 124, 30);
                tablView.frame = CGRectMake(12,240, 295,250);
            }
        }
    }
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"Badge11"
     object:self];
    
    NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
    [def setObject:@"" forKey:@"zipcode"];
    
    [def setObject:@"" forKey:@"acomment"];
    [def synchronize];
    [segment setSelectedSegmentIndex:0];
    //tex1.text=@"";
    [self loadJson];
    NSUserDefaults *de=[NSUserDefaults standardUserDefaults];
    strU=[de stringForKey:@"url"];
    
    NSMutableArray *arrayvaluesNolFilter=[[NSMutableArray alloc]init];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    arrayvaluesNolFilter= [userDefaults objectForKey:@"keySelectedValues"];
    
    //NSLog(@"key Selected Values %@",arrayvalus);
    if ([arrayvaluesNolFilter count]>0) {
        [activity setHidden:NO];
        [activity startAnimating];
        [tablView setHidden:NO];
    }
    else
    {
        arrayvaluesNolFilter= [[NSMutableArray alloc] init];
        [arrayvaluesNolFilter addObject:@"Total Leads"];
        [arrayvaluesNolFilter addObject:@"New Lead"];
        [arrayvaluesNolFilter addObject:@"Total Open"];
        [arrayvaluesNolFilter addObject:@"Not Reached"];
        [arrayvaluesNolFilter addObject:@"Not Interested"];
        [arrayvaluesNolFilter addObject:@"Scheduled"];
        //        }
    }
    NSMutableSet* existingNames = [NSMutableSet set];
    arrayvalus = [NSMutableArray array];
    for (id object in arrayvaluesNolFilter) {
        if (![existingNames containsObject:object]) {
            [existingNames addObject:object ];
            [arrayvalus addObject:object];
        }
    }
    
    //Updatedby Deepak
    //[self syncAllLeadIfAny];
    
    //Updated by navin or Back manage
    if ([[def valueForKey:@"backStatus"] isEqualToString:@"Addlead"]) {
        
    }else{
        txtZip.text=@"";
        _txtEmailId.text=@"";
        _txtFirstName.text=@"";
        _txtLastName.text=@"";
        _txtPhoneNo.text=@"";
        tex1.text=@"";
        
    }
    
    [def setValue:@"" forKey:@"backStatus"];
}

-(void)setBadge{
    _badgeView = [[M13BadgeView alloc] initWithFrame:CGRectMake(0, 0, 24.0, 24.0)];
    
    _badgeView.text = [NSString stringWithFormat:@"%d",objClass.LeadCountOutBox];
    NSLog(@"Badge Count %d",objClass.LeadCountOutBox);
    if (objClass.LeadCountOutBox>0) {
        [btnHistory addSubview:_badgeView];
    }
    else {
        for (UIView *view in [btnHistory subviews]) {
            [view removeFromSuperview];
        }
    }
    
}

-(void)goToHome{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(void)goTosetting
{
  //  NSLog(@"go to setting");
    if (objClass.isonSelectedServices==1) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning !!" message:@"Lead information has not been submitted" delegate:self cancelButtonTitle:@"Exit anyway" otherButtonTitles:@"Cancel", nil];
    [alert show];
    alert.tag=2;
    }
    else{
        [self deleteCustomerTable];
        [self deleteServiceDetailTable];
        [self deleteImagePathTable];
        [self deleteAudioPathTable];

        UIStoryboard *storyboard;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        }
        else
        {
            storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        }
        SettingView *myVC = (SettingView *)[storyboard instantiateViewControllerWithIdentifier:@"settings"];
        [self.navigationController pushViewController:myVC animated:NO];
    }
    
}

-(void)gotoHistory
{
 //  NSLog(@"go to history");
    if (objClass.isonSelectedServices==1) {
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning !!" message:@"Lead information has not been submitted" delegate:self cancelButtonTitle:@"Exit anyway" otherButtonTitles:@"Cancel", nil];
        alert.tag=4;
        [alert show];
    }
    else{
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
    {
        [self deleteCustomerTable];
        [self deleteServiceDetailTable];
        [self deleteImagePathTable];
        [self deleteAudioPathTable];

        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            
            HistoryViewController *hvc = [[HistoryViewController alloc] initWithNibName:@"HistoryViewController_iPhone3.5" bundle:nil];
            
            [self.navigationController pushViewController:hvc animated:NO];

        }else{
                HistoryViewController *hvc = [[HistoryViewController alloc] initWithNibName:@"HistoryViewController_iPhone5" bundle:nil];
    
                [self.navigationController pushViewController:hvc animated:NO];
        }
    }
    else
    {
        HistoryViewController *hvc = [[HistoryViewController alloc] initWithNibName:@"HistoryViewController_iPhad" bundle:nil];
        
        [self.navigationController pushViewController:hvc animated:NO];
    }
    }
    
}
-(void)deleteDatabase
{
    
    //GetService *getService= (GetService*)[NSEntityDescription insertNewObjectForEntityForName:@"GetService" inManagedObjectContext:_managedObjectContext];
    NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
    [allCars setEntity:[NSEntityDescription entityForName:@"GetService" inManagedObjectContext:context ]];
    [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * cars = [context  executeFetchRequest:allCars error:&error];
    
    //error handling goes here
    for (NSManagedObject * car in cars) {
        [context  deleteObject:car];
    }
    NSError *saveError = nil;
 //   [_managedObjectContext save:&saveError];
    [context save:&saveError];

}

-(void)getService
{
//    if ([arrAllObj count] == 0)
//    {
    Reachability *reachableForWiFy=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify = [reachableForWiFy currentReachabilityStatus];
    if (netStatusWify== NotReachable)
    {
        
    }
    else
    {
        [self deleteDatabase];
        NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
        NSString *va=[def valueForKey:@"url"];
        
        NSString   *strUk=[def stringForKey:@"key"];
        
        NSString   *strU1=[def stringForKey:@"name"];
        
        NSURL *url1 = [NSURL URLWithString:[NSString stringWithFormat:@"%@?key=getServices&Uname=%@&comKey=%@",[va stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[strU1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[strUk stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
        cMgr.serviceName=@"getServices";
        
        NSXMLParser *xmlParser = [[NSXMLParser alloc]initWithContentsOfURL:url1];
        XMLParser   *parser = [[XMLParser alloc] init];
        [xmlParser setDelegate:parser];
        
        BOOL success = [xmlParser parse];
        if (success) {
            // NSLog(@"SUCCESS:%d",[personalGoalarray count]);
            if ([personalGoalarray count]>0) {
            }
        }
    }
}

-(void)logoutClicked
{
    UIAlertView *alertvi=[[UIAlertView alloc]initWithTitle:@"Log out ?" message:@"Are you sure you want to log out?" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
    
    alertvi.tag=3;
    [alertvi show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==3){
        if (buttonIndex==0) {
            [self deleteDatabase];
            [self deleteCustomerTable];
            [self deleteServiceDetailTable];
            [self deleteImagePathTable];
            [self deleteAudioPathTable];
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Remember"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"name"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"pwd"];
            
            NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
            
            [def setObject:nil forKey:@"url"];
            [def setBool:NO forKey:@"AD_ZipCode"];
            [def setBool:NO forKey:@"AD_Commnet"];
            [def setObject:nil forKey:@"Company_Id"];
            [def setObject:nil forKey:@"Remember"];
            //Remember
            [def synchronize];

            //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
            
            UIStoryboard *storyboard;
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
            }
            else
            {
                
                storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
            }
            MainViewController* verificationBadgesViewController = (MainViewController*)[storyboard instantiateViewControllerWithIdentifier:@"log"];
            UINavigationController *nav1 = [[UINavigationController alloc] initWithRootViewController:verificationBadgesViewController];
            //Present the new navigation stack modally
            [nav1 setModalPresentationStyle:UIModalPresentationFullScreen];
            [self presentViewController:nav1  animated:NO completion:nil];
        }
    }
   else if (alertView.tag==2) {
        
        if (buttonIndex==0) {
            [self deleteCustomerTable];
            [self deleteServiceDetailTable];
            [self deleteImagePathTable];
            [self deleteAudioPathTable];

            UIStoryboard *storyboard;
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
            }
            else
            {
                storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
            }
            SettingView *myVC = (SettingView *)[storyboard instantiateViewControllerWithIdentifier:@"settings"];
            [self.navigationController pushViewController:myVC animated:NO];
        }
   }
   else if (alertView.tag==4){
       if (buttonIndex==0) {
           [self deleteCustomerTable];
           [self deleteServiceDetailTable];
           [self deleteImagePathTable];
           [self deleteAudioPathTable];

       if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
       {
           if ([UIScreen mainScreen].bounds.size.height==480)
           {
               
               HistoryViewController *hvc = [[HistoryViewController alloc] initWithNibName:@"HistoryViewController_iPhone3.5" bundle:nil];
               
               [self.navigationController pushViewController:hvc animated:NO];
               
           }else{
               HistoryViewController *hvc = [[HistoryViewController alloc] initWithNibName:@"HistoryViewController_iPhone5" bundle:nil];
               
               [self.navigationController pushViewController:hvc animated:NO];
           }
       }
       else
       {
           HistoryViewController *hvc = [[HistoryViewController alloc] initWithNibName:@"HistoryViewController_iPhad" bundle:nil];
           
           [self.navigationController pushViewController:hvc animated:NO];
       }
       }
   
   }
}

-(void)deleteDatabase1{
    NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
    [allCars setEntity:[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext ]];
    [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSError * error = nil;
    NSArray * cars = [_managedObjectContext    executeFetchRequest:allCars error:&error];
    //error handling goes here
    for (NSManagedObject * car in cars) {
        [_managedObjectContext  deleteObject:car];
    }
    NSError *saveError = nil;
[_managedObjectContext save:&saveError];
}
-(void)deleteDatabase2
{
    //Imgpath
   // ServiceDetailTable
    NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
    [allCars setEntity:[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext ]];
    [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * cars = [_managedObjectContext  executeFetchRequest:allCars error:&error];
    
    //error handling goes here
    for (NSManagedObject * car in cars) {
        [_managedObjectContext  deleteObject:car];
    }
    NSError *saveError = nil;
[_managedObjectContext save:&saveError];}


-(void)deleteDatabase3
{
    //Imgpath
    //ServiceDetailTable
    NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
    [allCars setEntity:[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext ]];
    [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * cars = [_managedObjectContext    executeFetchRequest:allCars error:&error];
    
    //error handling goes here
    for (NSManagedObject * car in cars) {
        [_managedObjectContext  deleteObject:car];
    }
    NSError *saveError = nil;
[_managedObjectContext save:&saveError];    // GetService
}

-(void)deleteDatabase4
{
    //Imgpath
    //    ServiceDetailTable
    NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
    [allCars setEntity:[NSEntityDescription entityForName:@"Imgpath" inManagedObjectContext:_managedObjectContext ]];
    [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * cars = [_managedObjectContext    executeFetchRequest:allCars error:&error];
    
    //error handling goes here
    for (NSManagedObject * car in cars) {
        [_managedObjectContext  deleteObject:car];
    }
    NSError *saveError = nil;
[_managedObjectContext save:&saveError];
    
    // GetService
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)loadAllDataForDrop
{
    entityDesc = [NSEntityDescription entityForName:@"Managelead" inManagedObjectContext:context];
    [request setEntity:entityDesc];
    //    NSPredicate *pred = [NSPredicate predicateWithFormat:@"((eUsername = %@)&&(ePassword=%@))", txtName.text,txtPassword.text];
    
    NSError *error;
    
    objects = [context executeFetchRequest:request error:&error];
    if ([objects count] == 0){
    } else {
    }
 }
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
     return YES;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{    UITouch *touch = [[event allTouches] anyObject];
    
    [super touchesBegan:touches withEvent:event];
    if (touch) {
    
        [tex1 resignFirstResponder];
        [_txtEmailId resignFirstResponder];
        [_txtFirstName resignFirstResponder];
        [_txtLastName  resignFirstResponder];
        [_txtPhoneNo resignFirstResponder];
        [txtZip resignFirstResponder];
          }
    for (UIView * txt in self.view.subviews){
        if ([txt isKindOfClass:[UITextView class]] && [txt isFirstResponder]) {
            [txt resignFirstResponder];
        }
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }

    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 100) ? NO : YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect =[self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =[self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =(MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    // NSLog(@"%f",denominator);
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =[[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}

-(IBAction)openHelp
{


}
-(IBAction)addLead
{
    NSUserDefaults *boolUserDefaults12 = [NSUserDefaults standardUserDefaults];
    [boolUserDefaults12 setValue:@"" forKey:@"strIndexNew"];
    [boolUserDefaults12 synchronize];

    [self deleteCustomerTable];
    [self deleteServiceDetailTable];
    [self deleteImagePathTable];
    [self deleteAudioPathTable];
   // NSLog(@"%@",tex1.text);
    cMgr.accountName=tex1.text;
    cMgr.isAutoid=NO;
    
    NSUserDefaults *boolUserDefaults = [NSUserDefaults standardUserDefaults];
    [boolUserDefaults setBool:NO forKey:@"isFromUpdate"];
    [boolUserDefaults setBool:NO forKey:@"isFromAddMoreViewNew"];

    
    [boolUserDefaults synchronize];
    
    static NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    if ([cMgr.accountName isEqualToString:@""]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert" message: @"Please Enter Account Number." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else if ([cMgr.accountName isEqualToString:@"0"]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert" message: @"Please Enter Account Number other then Zero." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else if ([_txtEmailId.text length]>0 && ![emailPredicate evaluateWithObject:_txtEmailId.text]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please enter a valid email address" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
         AppDelegate *appDelegatee = (AppDelegate *)[[UIApplication sharedApplication] delegate];
         _managedObjectContext= [appDelegatee managedObjectContext];
        
        
        CustomerTable   *cTable= (CustomerTable*)[NSEntityDescription insertNewObjectForEntityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext];
        
        //---------------------------------new Changes on 27 october
        
        if (_txtFirstName.text.length==0) {
            cTable.fName=@"";
        }
        else{
            cTable.fName=_txtFirstName.text;
        }

        if (_txtLastName.text.length==0) {
            cTable.lName=@"";
        }
        else{
            cTable.lName=_txtLastName.text;
        }

        if (_txtPhoneNo.text.length==0) {
            cTable.phNo=@"";
        }
        else{
            cTable.phNo=_txtPhoneNo.text;
        }
        
        cTable.username=cMgr.strUsreName;
        if (_txtEmailId.text.length==0) {
            cTable.emailId=@"";
        }
        else{
            cTable.emailId=_txtEmailId.text;
        }
        //---------------------------------new Changes on 27 october
        
        
        cTable.statusCustomer=@"old";
        cTable.accountNo=tex1.text;
        cTable.username=cMgr.strUsreName;
        
        NSDate *date1 = [NSDate date];
        //Create the dateformatter object
        NSDateFormatter* formatter1 = [[NSDateFormatter alloc] init] ;
        //Set the required date format
        [formatter1 setDateFormat:@"MM-dd-yyyy hh:mm:ss a"];
        //Get the string date
        NSString *dateSet1 = [formatter1 stringFromDate:date1];
        cTable.dateTimec=dateSet1;
        
        NSDate *date = [NSDate date];
        //Create the dateformatter object
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init] ;
        //Set the required date format
        [formatter setDateFormat:@"yyyy-MM-dd-HH-MM-SS"];
        //Get the string date
        NSString *dateSet = [formatter stringFromDate:date];
        //****RAndom number
        
         NSUInteger r = arc4random_uniform(1000000000);
        
        dateSet = [NSString stringWithFormat:@"%lu",(unsigned long)r];
        
       // NSInteger intRandom =   arc4random() %8;
        
        //******
        
        autoid=dateSet;
        cMgr.isAutoid=YES;
        cMgr.selectedService=autoid;
        
        cTable.autoId=cMgr.selectedService;
        
        
        if (txtZip.text.length>0) {
            
            NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
            
            [def setObject:txtZip.text forKey:@"zipcode"];
            [def synchronize];
            
        }
        else
        {
            NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
            [def setObject:@"" forKey:@"zipcode"];
            [def synchronize];
        }
            

        
        
        NSError *savingError = nil;
        
        if ([self.managedObjectContext save:&savingError]){
           // NSLog(@"Successfully saved the context.");
            [tex1 resignFirstResponder];
           // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
            
            UIStoryboard *storyboard;
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
            }
            else
            {
                
                storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
            }

            
            MainViewController* lgn = (MainViewController*)[storyboard instantiateViewControllerWithIdentifier:@"addLead"];
            [self.navigationController  pushViewController:lgn animated:YES];

}
        else {
           // NSLog(@"Failed to save the context. Error = %@", savingError);
        }
}

}

-(void)deleteCustomerTable
{
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSPredicate  *pred = [NSPredicate predicateWithFormat:@"(statuso= %@)",@"send"];
    [allData setPredicate:pred];
    NSError * error = nil;
    NSArray * Data = [_managedObjectContext executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [_managedObjectContext deleteObject:data];
    }
    NSError *saveError = nil;
    [_managedObjectContext save:&saveError];
}

-(void)deleteServiceDetailTable
{
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSPredicate  *pred = [NSPredicate predicateWithFormat:@"(statuso= %@)",@"send"];
    [allData setPredicate:pred];
    NSError * error = nil;
    NSArray * Data = [_managedObjectContext executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [_managedObjectContext deleteObject:data];
    }
    NSError *saveError = nil;
    [_managedObjectContext save:&saveError];
}

-(void)deleteImagePathTable
{
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"Imgpath" inManagedObjectContext:_managedObjectContext]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSPredicate  *pred = [NSPredicate predicateWithFormat:@"(statuso= %@)",@"send"];
    [allData setPredicate:pred];
    NSError * error = nil;
    NSArray * Data = [_managedObjectContext executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [_managedObjectContext deleteObject:data];
    }
    NSError *saveError = nil;
    [_managedObjectContext save:&saveError];
}

-(void)deleteAudioPathTable
{
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"Audiopath" inManagedObjectContext:_managedObjectContext]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
//    NSPredicate  *pred = [NSPredicate predicateWithFormat:@"(statuso= %@)",@"send"];
//    [allData setPredicate:pred];
    NSError * error = nil;
    NSArray * Data = [_managedObjectContext executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [_managedObjectContext deleteObject:data];
    }
    NSError *saveError = nil;
    [_managedObjectContext save:&saveError];
}

-(IBAction)newCustomer
{
    [self deleteCustomerTable];
    [self deleteServiceDetailTable];
    [self deleteImagePathTable];
    [self deleteAudioPathTable];

    if (segment.selectedSegmentIndex==1) {
        
        [segment setSelectedSegmentIndex:1];
        
       // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
        
        UIStoryboard *storyboard;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
        }
        else
        {
             storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
        }
        
        NewcustomerViewController* lgn = (NewcustomerViewController*)[storyboard instantiateViewControllerWithIdentifier:@"new"];
        [self.navigationController  pushViewController:lgn animated:NO];
    }
}

-(void)syncAllLeadIfAny {
    //*****Sync Lead if Any*****
    AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _managedObjectContext= [appdelegate managedObjectContext];
    
    NSEntityDescription *entityDesc13=[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext];
    
    NSFetchRequest *request25=[[NSFetchRequest alloc
                               ]init];
    [request25 setEntity:entityDesc13];
    
    NSPredicate    *pred11 = [NSPredicate predicateWithFormat:@"(username= %@) && (statuso= %@)",cMgr.strUsreName, @"true"];
    [request25 setPredicate:pred11];
    
    NSError *error=nil;
    NSArray  *objects123 = [_managedObjectContext executeFetchRequest:request25  error:&error];
    
    
    objectsMutablesw=[NSMutableArray arrayWithArray:objects123];
    
    if ([objectsMutablesw count]==0) {
        cMgr.issSending=YES;
    }else {
        cMgr.issSending=NO;
    }
    
    if (!cMgr.issSending) {
        Reachability *reachableForWiFy=[Reachability reachabilityForInternetConnection];
        NetworkStatus netStatusWify = [reachableForWiFy currentReachabilityStatus];
        if (netStatusWify== NotReachable)
        {
            //Internet not working
        }else {
            [self postdatattoserver];
            cMgr.issSending=YES;
        }
        
    }
    
    
    //*****Sync Image Path if Any*****
    NSEntityDescription  *entityDesc4=[NSEntityDescription entityForName:@"Imgpath" inManagedObjectContext:_managedObjectContext];
    
    NSFetchRequest *request2=[[NSFetchRequest alloc
                              ]init];
    [request2 setEntity:entityDesc4];
    
    NSArray *objects2 = [_managedObjectContext executeFetchRequest:request2 error:&error];
    NSMutableArray  *objectsMutables1=[NSMutableArray arrayWithArray:objects2];
    if ([objectsMutables1 count]==0) {
        
    }
    else
    {
        if (!cMgr.isImagesending) {
            Reachability *reachableForWiFy=[Reachability reachabilityForInternetConnection];
            NetworkStatus netStatusWify = [reachableForWiFy currentReachabilityStatus];
            if (netStatusWify== NotReachable)
            {
                //Internet not working
            }else {
                [self sendimageToserver];
            }
        }else{
        }
    }
    
    //*****Sync Audio if Any*****
    NSEntityDescription   *entityDesc2=[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext];
    
    request=[[NSFetchRequest alloc
             ]init];
    [request setEntity:entityDesc2];
    
    NSPredicate    *pred111 = [NSPredicate predicateWithFormat:@"(username= %@)",cMgr.strUsreName];
    [request setPredicate:pred111];
    
    NSArray    *objects12 = [_managedObjectContext executeFetchRequest:request error:&error];
    arrayForsendinga=[NSMutableArray arrayWithArray:objects12];
    
    
    for (int i=0; i<[arrayForsendinga count]; i++)
    {
        audioPathData= (Audiopath*)[NSEntityDescription insertNewObjectForEntityForName:@"Audiopath" inManagedObjectContext:_managedObjectContext];
        
        audioPathData.audionameDatabase=[[arrayForsendinga objectAtIndex:i]audioName];
        audioPathData.audiopathDatabase=[[arrayForsendinga objectAtIndex:i]audioPath];
        audioPathData.username=cMgr.strUsreName;
        NSError *savingError = nil;
        if ([self.managedObjectContext save:&savingError]){
            // NSLog(@"Successfully saved the context.");
        }
    }
    
    [self allaudio];
    [self deleteDatabasesend];
    [self deletedatabasevalueforsending];
}

-(void)allaudio
{
    NSEntityDescription   *entityDesc2=[NSEntityDescription entityForName:@"Audiopath" inManagedObjectContext:_managedObjectContext];
    request=[[NSFetchRequest alloc]init];
    // [request setEntity:entityDesc];
    [request setEntity:entityDesc2];
    NSPredicate    *pred1 = [NSPredicate predicateWithFormat:@"(username= %@)",cMgr.strUsreName];
    [request setPredicate:pred1];

    NSError *error=nil;
    NSArray    *objects12 = [_managedObjectContext executeFetchRequest:request error:&error];
    arrayForsendinga=[NSMutableArray arrayWithArray:objects12];
    
    if ([arrayForsendinga count]==0) {
        
    }
    
    
else
{
    if (!cMgr.issendaudio) {
        [self sendAudio];
        cMgr.issendaudio=YES;
    }
}
}

-(void)deleteDatabasesend
{
    
    AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _managedObjectContext= [appdelegate managedObjectContext];
    
    NSEntityDescription    *entityDesc1=[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext];
    
    NSFetchRequest  *request123=[[NSFetchRequest alloc
                                 ]init];
   
    // [request setEntity:entityDesc];
    
    [request123 setEntity:entityDesc1];
    
    NSError *error;
    
    //    objects = [_managedObjectContext executeFetchRequest:request error:&error];
    //
    
    //
    NSPredicate  *pred = [NSPredicate predicateWithFormat:@"(statuso= %@)",@"send"];
    [request123 setPredicate:pred];
    NSArray    *objects12 = [_managedObjectContext executeFetchRequest:request123 error:&error];
    
    for (NSManagedObject * car in objects12) {
       
                                [_managedObjectContext  deleteObject:car];
                                NSError *saveError = nil;
                                [_managedObjectContext save:&saveError];
                         }
    
}


-(void)deletedatabasevalueforsending
{
    NSEntityDescription *entityDesc13=[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext];
    
    NSFetchRequest *request25=[[NSFetchRequest alloc
                                ]init];
     // [request setEntity:entityDesc];
    
    [request25 setEntity:entityDesc13];
    
    NSPredicate    *pred111 = [NSPredicate predicateWithFormat:@"(username= %@) && (statuso= %@)",cMgr.strUsreName, @"send"];
    [request25 setPredicate:pred111];

    NSError *error;
    NSArray  *objects123 = [_managedObjectContext executeFetchRequest:request25  error:&error];

    for (NSManagedObject * car in objects123) {
        
       // [_managedObjectContext  deleteObject:car];
      //  NSError *saveError = nil;
      //  [_managedObjectContext save:&saveError];
        
        
    }
}

-(void)postdatattoserver
{
    AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _managedObjectContext= [appdelegate managedObjectContext];
    
    NSEntityDescription    *entityDesc1=[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext];
    
    NSFetchRequest  *request122=[[NSFetchRequest alloc
                                 ]init];
 
    [request122 setEntity:entityDesc1];
    
    NSError *error;

    NSPredicate  *pred = [NSPredicate predicateWithFormat:@"(statuso= %@)",@"true"];
    [request122 setPredicate:pred];
    NSArray    *objects12 = [_managedObjectContext executeFetchRequest:request122 error:&error];
    NSMutableArray  *objectsMutables=[NSMutableArray arrayWithArray:objects12];
    
    
    
    if ([objectsMutables count]==0) {
        cMgr.issSending=NO;
    }
        
    for (int i=0 ;i<[objectsMutables count]; i++) {
        
        NSString *post;
        NSString *strK=[[NSUserDefaults standardUserDefaults]valueForKey:@"key"];
        
        post = [NSString stringWithFormat:@"&AutoID=%@&SendDateTime=%@&uname=%@&comKey=%@&key=insertMaster",[[objectsMutables objectAtIndex:i]autoId],[[objectsMutables objectAtIndex:i]dateTimec],cMgr.cacheName,strK];
        // NSLog(@"%@",post);
        // NSLog(@"%@",[[objectsMutables objectAtIndex:i]dateTimec]);
        NSString  *post1;
        if ([[[objectsMutables objectAtIndex:i]statusCustomer] isEqualToString:@"new"])
            
        {
            NSEntityDescription   *entityDesc2=[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext];
            
            request=[[NSFetchRequest alloc
                     ]init];
            //
            
            
            
            // [request setEntity:entityDesc];
            
            [request setEntity:entityDesc2];
            
            NSError *error;
            
            //    objects = [_managedObjectContext executeFetchRequest:request error:&error];
            //
            
            
            pred = [NSPredicate predicateWithFormat:@"(autoId = %@)",[[objectsMutables objectAtIndex:i]autoId]];
            [request setPredicate:pred];
            NSArray  *objects2 = [_managedObjectContext executeFetchRequest:request error:&error];
            
            
            
            if ([objects2 count]>0) {
                post1=[NSString stringWithFormat:@"&FName=%@&LName=%@&Phno=%@&Email=%@&status=new",[[objects2 objectAtIndex:0]fName],[[objects2 objectAtIndex:0]lName],[[objects2 objectAtIndex:0]phNo],[[objects2 objectAtIndex:0]emailId]];
                post=[post stringByAppendingString:post1];
                
            }
            
            
            
            
            
        }
        
        else
        {
            
            post1=[NSString stringWithFormat:@"&AccountNo=%@&status=old",[[objectsMutables objectAtIndex:i]accountNo]];
            post=[post stringByAppendingString:post1];
            
            
        }
        
        
        
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        //    You need to send the actual length of your data. Calculate the length of the post string.
        
        
        //    3. Create a Urlrequest with all the properties like HTTP method, http header field with length of the post string.
        //    Create URLRequest object and initialize it.
        
        NSMutableURLRequest *request34 = [[NSMutableURLRequest alloc] init];
        
        //    Set the Url for which your going to send the data to that request.
        
        
        //  NSLog(@"%@",strU);
        
        
        
        strU =[strU stringByAppendingString:@"?"];
        [request34 setURL:[NSURL URLWithString:strU]];
        
        //    Now, set HTTP method (POST or GET).
        //    Write this lines as it is in your code.
        
        [request34 setHTTPMethod:@"POST"];
        
        //    Set HTTP header field with length of the post data.
        
        
        //    Also set the Encoded value for HTTP header Field.
        
        //  [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];
        
        //    Set the HTTPBody of the urlrequest with postData.
        
        [request34 setHTTPBody:postData];
        
        
        
        //    4. Now, create URLConnection object. Initialize it with the URLRequest.
        //   dispatch_async(dispatch_get_main_queue(), ^{
        //update UI here
        //
        NSURLConnection *conn = [[NSURLConnection alloc]initWithRequest:request34 delegate:self];
        
        //            NSHTTPURLResponse  *response = nil;
        //            [NSURLConnection sendSynchronousRequest:request1
        //                                  returningResponse:&response
        //                                              error:nil];
        
        
        
        
        
        
        if (conn) {
            
            
            
            
            
            //  NSLog(@"poc");
            
            
            
            
            NSFetchRequest * allCars3 = [[NSFetchRequest alloc] init];
            [allCars3 setEntity:[NSEntityDescription entityForName:@"CustomerTable" inManagedObjectContext:_managedObjectContext ]];
            [allCars3 setIncludesPropertyValues:NO]; //only fetch the managedObjectID
            NSPredicate *p1=[NSPredicate predicateWithFormat:@"autoId=%@",[[objectsMutables objectAtIndex:i]autoId]];
            [allCars3 setPredicate:p1];
            NSError * error1 = nil;
            NSArray * cars1 = [_managedObjectContext    executeFetchRequest:allCars3 error:&error1];
            
            
            
            
            NSManagedObject *match=nil;
            
            if([cars1 count]==0)
            {
                
                
            }
            else
            {
                for (int i=0; i<[cars1 count] ; i++)
                {
                    
                    match=[cars1 objectAtIndex:i];
                    
                    [match setValue:@"send" forKey:@"statuso"];
                    
                    [_managedObjectContext save:&error];
                    
                }
            }
            
            
        }
        
        else
        {
            
        }
        
    }
    
    
    NSEntityDescription *entityDesc13=[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext];
    
    NSFetchRequest *request25=[[NSFetchRequest alloc
                               ]init];
   
    [request25 setEntity:entityDesc13];
    
    NSPredicate    *pred111 = [NSPredicate predicateWithFormat:@"(username= %@)",cMgr.strUsreName];
    [request25 setPredicate:pred111];
    
//    NSPredicate  *pred1 = [NSPredicate predicateWithFormat:@"(statuso= %@)",@"true"];
//    [request setPredicate:pred1];
    NSArray  *objects123 = [_managedObjectContext executeFetchRequest:request25  error:&error];
        
    objectsMutablesw=[NSMutableArray arrayWithArray:objects123];
    for (int i = 0; i<[objectsMutablesw count]; i++)
        
    {
        imagNamelocal=[[objectsMutablesw objectAtIndex:i]imgName];
        
        
        if ([imagNamelocal isEqualToString:@""]) {
            imagNamelocal=@"";
        }
        
        else if ([imagNamelocal isEqualToString:@"null"]) {
            imagNamelocal=@"";
        }
        
        else if (imagNamelocal==nil) {
            imagNamelocal=@"";
        }
        // NSLog(@"%@",[[objectsMutablesw objectAtIndex:i]imgName]);
        localPath=[[objectsMutablesw objectAtIndex:i]savedImagePath];
        
        audioNamelocal=[[objectsMutablesw objectAtIndex:i]audioName];
        
        if ([audioNamelocal isEqualToString:@""]) {
            audioNamelocal=@"";
        }
        
        else if ([audioNamelocal isEqualToString:@"null"]) {
            audioNamelocal=@"";
        }
        
        else if (audioNamelocal==nil) {
            audioNamelocal=@"";
        }
        localaudioPath=[[objectsMutablesw objectAtIndex:i]audioPath];
        [RemoveIdv insertObject:[[objectsMutablesw objectAtIndex:i]autoId] atIndex:i];
        NSString *postn;
        
        postn = [NSString stringWithFormat:@"&autoid=%@&serviceid=%@&servicename=%@&images=%@&audio=%@&comment=%@&urgency=%@&acno=%@&key=detailinsert",[[objectsMutablesw objectAtIndex:i]autoId],[[objectsMutablesw objectAtIndex:i]serviceId],[[objectsMutablesw objectAtIndex:i]serviceName], imagNamelocal,audioNamelocal,[[objectsMutablesw objectAtIndex:i]comment],[[objectsMutablesw objectAtIndex:i]urgencyLevel],[[objectsMutablesw objectAtIndex:i]accountNo]];
        
        NSData *postData1 = [postn dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *request2 = [[NSMutableURLRequest alloc] init];
        [request2 setURL:[NSURL URLWithString:strU]];
        [request2 setHTTPMethod:@"POST"];
        [request2 setHTTPBody:postData1];
       
        NSURLConnection *conn1 = [[NSURLConnection alloc]initWithRequest:request2 delegate:self];
        
        if(conn1)
        {
            
            NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
            [allCars setEntity:[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext ]];
            [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
            NSPredicate *p=[NSPredicate predicateWithFormat:@"iD=%@", [[objectsMutablesw objectAtIndex:i]iD],cMgr.strUsreName];
            [allCars setPredicate:p];
            NSError * error = nil;
            NSArray * cars = [_managedObjectContext    executeFetchRequest:allCars error:&error];
       
            NSManagedObject *match=nil;
            
            if([cars count]==0)
            {
                
                
            }
            else
            {
                for (int i=0; i<[cars count] ; i++)
                {
                    match=[cars objectAtIndex:i];
                    [match setValue:@"send" forKey:@"statuso"];
                    [_managedObjectContext save:&error];
                    
                }
            }
        }
        else
        {
            //  NSLog(@"Connection could not be made”);
        }
        
        //    It returns the initialized url connection and begins to load the data for the url request. You can check that whether you URL connection is done properly or not using just if/else statement as below.
        cMgr.issSending=NO;
        //     [self sendAudio];
    }
  
    cMgr.issSending=NO;
}
-(void)sendAudio
{

    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        

        for (int i=0; i<[arrayForsendinga count]; i++) {
      
    
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSData *audioData;
    
    
    audioData = [NSData dataWithContentsOfFile:[[arrayForsendinga objectAtIndex:i]audiopathDatabase]];
    
   // NSLog(@"%@",[[arrayForsendinga objectAtIndex:i]audiopathDatabase]);
   // NSLog(@"%@",localaudioPath);
   // NSLog(@"Data:%@.......",audioData);
    
    // setting up the URL to post to
    
    NSString *st=[strU stringByReplacingOccurrencesOfString:@"ServiceHandler.ashx" withString:@"AudioHandler.ashx"];
    //http://myanteater.com/AudioHandler.ashx
  //  NSLog(@"%@",st);
    NSString *urlString = [NSString stringWithFormat:@"%@",st];
    
    // setting up the request object now
    NSMutableURLRequest *request14 = [[NSMutableURLRequest alloc] init];
    [request14 setURL:[NSURL URLWithString:urlString]];
    [request14 setHTTPMethod:@"POST"];
    
    /*
     add some header info now
     we always need a boundary when we post a file
     also we need to set the content type
     
     You might want to generate a random boundary.. this is just the same
     as my output from wireshark on a valid html post
     */
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request14 addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    /*
     now lets create the body of the post
     */
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",[[arrayForsendinga objectAtIndex:i]audionameDatabase]] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:audioData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request14 setHTTPBody:body];
    
    // now lets make the connection to the web
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request14 returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
   // NSLog(@"%@",returnString);
            
            
            
            NSFetchRequest * allCars3 = [[NSFetchRequest alloc] init];
            [allCars3 setEntity:[NSEntityDescription entityForName:@"Audiopath" inManagedObjectContext:_managedObjectContext ]];
            [allCars3 setIncludesPropertyValues:NO]; //only fetch the managedObjectID
            NSPredicate *p1=[NSPredicate predicateWithFormat:@"audionameDatabase=%@",[[arrayForsendinga objectAtIndex:i]audionameDatabase]];
            [allCars3 setPredicate:p1];
            NSError * error1 = nil;
            NSArray * cars1 = [_managedObjectContext    executeFetchRequest:allCars3 error:&error1];
            //
            //        //error handling goes here
            for (NSManagedObject * car in cars1) {
                //
                //
                [_managedObjectContext  deleteObject:car];
                NSError *saveError = nil;
                [_managedObjectContext save:&saveError];
                            
            }
            
            
            cMgr.issendaudio=NO;

            
        }

    });
    

}
-(void)sendimageToserver
{

    
    if (!cMgr.isImagesending)
    {
        
        
        cMgr.isImagesending=YES;
       NSEntityDescription  *entityDesc4=[NSEntityDescription entityForName:@"Imgpath" inManagedObjectContext:_managedObjectContext];
    
    NSFetchRequest *request2=[[NSFetchRequest alloc
                               ]init];
    //
    
    
    
    // [request setEntity:entityDesc];
    
    [request2 setEntity:entityDesc4];
    
    
    
    //    objects = [_managedObjectContext executeFetchRequest:request error:&error];
    //
    
    //
    NSError *error=nil;
    NSArray    *objects2 = [_managedObjectContext executeFetchRequest:request2 error:&error];
    
    
    NSMutableArray  *objectsMutables1=[NSMutableArray arrayWithArray:objects2];
    
   // NSLog(@"%d",[objectsMutables1 count]);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    
 
    
    for (int i=0; i<[objectsMutables1 count]; i++)
    {
        
        
        
        
        NSData *imageData;
        
        UIImage *_img = [UIImage imageWithContentsOfFile:[[objectsMutables1 objectAtIndex:i]pathImg]];
        
        //NSLog(@"%@",[[objectsMutables1 objectAtIndex:i]pathImg]);
        removalId=[[objectsMutables1 objectAtIndex:i]imgNameimg];
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,1.0,1)];
        imgView.image = _img;
        imageData =  UIImageJPEGRepresentation(imgView.image, 0.00000000000000000000000000001f);
        // NSLog(@"%@",imageData);
        // [self.view addSubview:imgView];
        
        // setting up the request object now
        //  	// setting up the URL to post to
        //http://myanteater.com/ImageUploader.ashx
        NSString *st=[strU stringByReplacingOccurrencesOfString:@"ServiceHandler.ashx" withString:@"ImageUploader.ashx"];
        NSString *urlString = [NSString stringWithFormat:@"%@",st];
        
        
        NSMutableURLRequest *request13 = [[NSMutableURLRequest alloc] init];
        [request13 setURL:[NSURL URLWithString:urlString]];
        [request13 setHTTPMethod:@"POST"];
        
        /*
         add some header info now
         we always need a boundary when we post a file
         also we need to set the content type
         
         You might want to generate a random boundary.. this is just the same
         as my output from wireshark on a valid html post
         */
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request13 addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        /*
         now lets create the body of the post
         */
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSString *combine =[[objectsMutables1 objectAtIndex:i]imgNameimg];
        
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@.png\"\r\n",combine] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:imageData]];
        // NSLog(@"%@",imageData);
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        // setting the body of the post to the reqeust
        [request13 setHTTPBody:body];
        
        // now lets make the connection to the web
        
        
        //  dispatch_async(dispatch_get_main_queue(), ^{
        //update UI here
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request13 returningResponse:nil error:nil];
        
        
        
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        
        chkforResponse=returnString;
       // NSLog(@"%@",returnString);
        //  });
        
        if ([ chkforResponse isEqualToString:@"OK"] ) {
            
            
            //
            //            combine =[combine stringByAppendingString:@".png"];
            //            NSString *comma=@",";
            //            combine=[combine stringByAppendingString:comma];
            //            NSString *anthr=;
            //            anthr=[anthr stringByAppendingString:@".png"];
            //            NSString *yhi=[combine stringByAppendingString:anthr];
            //            NSLog(@"%@",yhi);
            NSData *imgd4;
            
            UIImage *img1 = [UIImage imageWithContentsOfFile:[[objectsMutables1 objectAtIndex:i]pathImg1]];
            
          //  NSLog(@"%@",[[objectsMutables1 objectAtIndex:i]pathImg1]);
            removalId=[[objectsMutables1 objectAtIndex:i]imgNameimg];
            UIImageView *imgView1 =  [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,1.0,1.0)];
            imgView1.image = img1;
            
            
            imgd4 =  UIImageJPEGRepresentation(imgView1.image, 0.00000000000000000000000000001f);
           // NSLog(@"%@",imgd4);
            // [self.view addSubview:imgView];
            
            
            //  	// setting up the URL to post to
            
            
            // setting up the request object now
            
            NSString *st=[strU stringByReplacingOccurrencesOfString:@"ServiceHandler.ashx" withString:@"ImageUploader.ashx"];
            NSString *urlString = [NSString stringWithFormat:@"%@",st];
            
            NSMutableURLRequest *request14 = [[NSMutableURLRequest alloc] init];
            [request14 setURL:[NSURL URLWithString:urlString]];
            [request14 setHTTPMethod:@"POST"];
            
            /*
             add some header info now
             we always need a boundary when we post a file
             also we need to set the content type
             
             You might want to generate a random boundary.. this is just the same
             as my output from wireshark on a valid html post
             */
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            [request14 addValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            /*
             now lets create the body of the post
             */
            NSMutableData *body = [NSMutableData data];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            NSString *combine =[[objectsMutables1 objectAtIndex:i]imgNameimg1];
            
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@.png\"\r\n",combine] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:imgd4]];
           // NSLog(@"%@",imgd4);
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            // setting the body of the post to the reqeust
            [request14 setHTTPBody:body];
            
            // now lets make the connection to the web
            
            
            
            //   dispatch_async(dispatch_get_main_queue(), ^{
            //update UI here
            
            NSData *returnData = [NSURLConnection sendSynchronousRequest:request14 returningResponse:nil error:nil];
            NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
            
            
            
            
           // NSLog(@"%@",returnString);
            //   });
            NSFetchRequest * allCars3 = [[NSFetchRequest alloc] init];
            [allCars3 setEntity:[NSEntityDescription entityForName:@"Imgpath" inManagedObjectContext:_managedObjectContext ]];
            [allCars3 setIncludesPropertyValues:NO]; //only fetch the managedObjectID
            NSPredicate *p1=[NSPredicate predicateWithFormat:@"imgNameimg =%@",removalId];
            [allCars3 setPredicate:p1];
            NSError * error1 = nil;
            NSArray * cars1 = [_managedObjectContext    executeFetchRequest:allCars3 error:&error1];
            //
            //        //error handling goes here
            for (NSManagedObject * car in cars1) {
                //
                //
                [_managedObjectContext  deleteObject:car];
                NSError *saveError = nil;
                [_managedObjectContext save:&saveError];
                
                
                
            }
            cMgr.isImagesending=NO;
        }
        
    }
    
        
    });
    
    }

}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController1{
  
    
    
    
    viewController1=[tabBarController.viewControllers objectAtIndex:1];

    
    if (viewController1 == [tabBarController.viewControllers objectAtIndex:0])
    {
        
        
        
    }
    else if (viewController1 == [tabBarController.viewControllers objectAtIndex:1])
    {
       
    }
    else if (viewController1 == [tabBarController.viewControllers objectAtIndex:2])
    {
        
        
    }
    
    
    
}


#pragma mark-Nsuser defaults
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSString *charlieSendString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //
    
    cMgr.versionCache=charlieSendString;
   // NSLog(@"%@",charlieSendString);
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection

{
    
    
}
-(void)loadJson
{
    NSUserDefaults *de=[NSUserDefaults standardUserDefaults];
    NSString *cKey=[de stringForKey:@"key"];

       // NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://myleadnow.com/ServiceHandler.ashx?key=getleadcount&Uname=%@&cKey=%@",[cMgr.cacheName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[cKey stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?key=getleadcount&Uname=%@&cKey=%@",KLOGINTOAPP,[cMgr.cacheName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[cKey stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];

    
    NSURLRequest* requestdf = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:requestdf
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse * response,
                                               NSData * data,
                                               NSError * error) {
                               
                               NSDictionary    *dictQuestion = [[NSDictionary alloc] initWithDictionary:[XMLReader dictionaryForXMLData:data error:nil]];
                              NSDictionary *dictDetails=  [[dictQuestion valueForKey:@"NewDataSet"] valueForKey:@"Table1"];
                               
                               if (dictDetails) {
                                   
                                  // NSLog(@"total lead %@",[[dictDetails valueForKey:@"TotalLead"] valueForKey:@"text"]);
                                   NSString *Scheduled =[[dictDetails valueForKey:@"Scheduled"] valueForKey:@"text"];
                                   NSString *NotInterested=[[dictDetails valueForKey:@"NotInterested"] valueForKey:@"text"];
                                   NSString *totalLead =[[dictDetails valueForKey:@"TotalLead"] valueForKey:@"text"];
                                   NSString *TotalNew=[[dictDetails valueForKey:@"TotalNew"] valueForKey:@"text"];
                                   NSString *TotalOpen =[[dictDetails valueForKey:@"TotalOpen"] valueForKey:@"text"];
                                   NSString *TotalComplete=[[dictDetails valueForKey:@"TotalComplete"] valueForKey:@"text"];
                                   NSString *FirstAttempt =[[dictDetails valueForKey:@"FirstAttempt"] valueForKey:@"text"];
                                   NSString *SecondAttemp=[[dictDetails valueForKey:@"SecondAttemp"] valueForKey:@"text"];
                                   NSString *NeaverReach=[[dictDetails valueForKey:@"NeaverReach"] valueForKey:@"text"];
                                   NSString *Sold=[[dictDetails valueForKey:@"Sold"] valueForKey:@"text"];
                                   NSString *Completeproposed=[[dictDetails valueForKey:@"Completeproposed"] valueForKey:@"text"];
                                   NSString *Invalid=[[dictDetails valueForKey:@"Invalid"] valueForKey:@"text"];

                  
                                   
               dictRsponseFromUrl = [NSDictionary dictionaryWithObjectsAndKeys:Scheduled,@"Scheduled",NotInterested,@"Not Interested",totalLead,@"Total Leads",TotalNew,@"New Lead",TotalOpen,@"Total Open",TotalComplete,@"Total Complete",FirstAttempt,@"First Attempt",SecondAttemp,@"Second Attempt",NeaverReach,@"Not Reached",Sold,@"Sold",Completeproposed,@"Complete Proposed",Invalid,@"Invalid", nil];
                                   
                                  // NSLog(@"final dict will %d",[dictRsponseFromUrl count]);
                                   [activity stopAnimating];
                                   [activity setHidden:YES];
                                   
                                   [tablView reloadData];
                                   

                               }
                           }];
    
     }
#pragma mark tableview delegate mathods


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([arrayvalus count]<1) {
        return 1;
    }else {
        return [arrayvalus count];
       
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell) {
        cell=nil;
        [cell removeFromSuperview];
    }
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    if ([arrayvalus count]>0) {
    
   
        NSString *stringValue = [arrayvalus objectAtIndex:indexPath.row];
        
        for (NSString *key in [dictRsponseFromUrl allKeys]) {
            if ([key isEqualToString:stringValue]) {
                cell.textLabel.text= key;
                NSString *strOld =[dictRsponseFromUrl valueForKey:key];
                NSString *trimmedString = [strOld stringByReplacingOccurrencesOfString:@"\n  " withString:@""];
                cell.detailTextLabel.text= trimmedString;
            }
         }
    }
    [cell setBackgroundColor:[UIColor clearColor]];
    return cell;
}
-(void)setBadge11{
    
    cMgr=[CacheManager getInstance];
    AppDelegate *appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _managedObjectContext= [appDelegate1 managedObjectContext];
    
    NSEntityDescription    *entityDesc111=[NSEntityDescription entityForName:@"ServiceDetailTable" inManagedObjectContext:_managedObjectContext];
    
    NSFetchRequest  *request1211=[[NSFetchRequest alloc
                                   ]init];
    [request1211 setEntity:entityDesc111];
    
    NSPredicate    *pred1 = [NSPredicate predicateWithFormat:@"(username= %@)",cMgr.strUsreName];
    NSLog(@"UserName == %@",cMgr.strUsreName);
    [request1211 setPredicate:pred1];

    
    NSError *error;
    
    NSArray    *objects121 = [_managedObjectContext executeFetchRequest:request1211 error:&error];
    objectsMutable = [NSMutableArray  arrayWithArray:objects121];
    _badgeView = [[M13BadgeView alloc] initWithFrame:CGRectMake(0, 0, 24.0, 24.0)];
    
    _badgeView.text = [NSString stringWithFormat:@"%lu",(unsigned long)[objectsMutable count]];
    if ([objectsMutable count]>0) {
        [btnHistory addSubview:_badgeView];
    }
    else {
        for (UIView *view in [btnHistory subviews]) {
            [view removeFromSuperview];
        }
    }
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
  //  [textField setKeyboardType:UIKeyboardTypePhonePad];
    return YES;
}

- (IBAction)VersionInfoAction:(id)sender {
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    NSString *strDate =[defs valueForKey:@"DateAppInstalled"];
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    NSString *strAppVersions =@"App Version: 3.0.5";
    NSString *strdates = [NSString stringWithFormat:@"Installed Date: %@",strDate];
    NSString *strIOSversions =[NSString stringWithFormat:@"current iOS Version %@",currSysVer];
    NSString *strInfo = [NSString stringWithFormat:@"**This App supports iOS version greater then 8.0**"];

    NSString *strVersionDetals = [NSString stringWithFormat:@"%@ \n %@ \n %@ \n %@",strAppVersions,strdates,strIOSversions,strInfo];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Version Details" message:strVersionDetals delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}
- (IBAction)goToiPadHelp:(id)sender {
    UIStoryboard *storyboard;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    }
    HelpViewController* lgn = (HelpViewController*)[storyboard instantiateViewControllerWithIdentifier:@"HelpViewControlleriPadExisting"];
    [self.navigationController presentViewController:lgn animated:NO completion:nil];
}
@end
