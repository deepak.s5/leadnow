//
//  TabbbarViewController.m
//  Green Lead
//
//  Created by Rakesh Jain on 19/09/13.
//  Copyright (c) 2013 Infocrats. All rights reserved.
//

#import "TabbbarViewController.h"

@interface TabbbarViewController ()

@end

@implementation TabbbarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{	
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
