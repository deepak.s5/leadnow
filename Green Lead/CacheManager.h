//
//  CacheManager.h
//  CycleApplication
//
//  Created by mac on 07/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface CacheManager : NSObject {
    
    
    NSString *accountName;
    
    BOOL isCheck;
    BOOL isUpdate;
    int cacheIndex;
    NSData *cacheData;
    NSString *cacheGoalName,*cacheWhtGoal,*cacheCurrentDate,*cacheGoalimp;
    
    NSString *userName,*serviceName;
    NSString *rferalId;
    NSString *selectedService;
    
    BOOL chkPost;
    BOOL chkFirst;
    BOOL chkloadData;
    
    BOOL chkUpdateclick,chkcellupdateclick,clientUpdate;
    NSIndexPath *chkIndexRight;
    
    BOOL comFromcli;
    BOOL comFromref;
    BOOL comfromgoal;
    BOOL comfromReferral;
    BOOL comfromUP;
    BOOL isUpdated;
    BOOL isAutoid;
    BOOL isFromCustomer;
    BOOL isFromaddLead;
    BOOL isYesimg;
    NSString *cacheName;
    NSString *strCompanykey;
    NSString *strUsreName;
    NSString *strPassword;
    
    NSString *cacheAudioname,*cacheaudioPath;
    
    
   }
extern NSMutableArray *pImg;
extern NSMutableArray *pImg1;
extern NSMutableArray *personalGoalarray;
extern NSMutableArray *getUserdetail;
extern NSMutableArray *getReferal;
extern NSMutableArray *getLead;
extern NSMutableArray *getnots;
extern NSMutableArray  *knotpre;
extern NSMutableArray *getclient;
extern NSMutableArray *getmanageactivity;
extern NSMutableArray *getResult;
extern NSMutableArray *getOverdue;
extern NSMutableArray *getLeadname;


extern NSMutableArray *objectsMutable;

extern NSMutableArray *getUpcomingtask;
extern  int p;


@property BOOL issSending;

@property BOOL isImagesending;
@property BOOL isFromaddLead;
@property BOOL isYesimg;
@property (nonatomic,retain)
 NSString *cacheAudioname,*cacheaudioPath;

@property (nonatomic,retain)
NSString *cacheName;
@property (nonatomic,retain)
NSString *accountName;
@property (nonatomic,retain)NSString *accountLName;
@property(nonatomic,retain)NSString *strCompanykey;
@property(nonatomic,retain) NSString *strUsreName;
@property(nonatomic,retain) NSString *strPassword;
@property(nonatomic,retain) NSString *selectedService;
@property BOOL isFromCustomer;
@property BOOL chkcellupdateclick;
@property int cacheIndex;
@property(nonatomic,retain) NSString *cacheGoalName,*cacheWhtGoal,*cacheCurrentDate,*cacheGoalimp,*userName,*serviceName,*rferalId,*versionCache;
@property(nonatomic,retain)NSString *cacheEmpid;

@property BOOL isCheck;
@property BOOL isUpdate;
@property BOOL clientUpdate;
@property   BOOL chkUpdateclick;
@property  BOOL comFromcli;
@property   BOOL comFromref;
@property BOOL comfromgoal,chkPost;
@property BOOL comfromReferral;
@property(nonatomic,retain) NSData *cacheData;
@property NSIndexPath *chkIndexRight;
@property BOOL chkloadData;
@property BOOL chkFirst,ntClient;
@property BOOL comfromUP;
@property  BOOL isUpdated;
@property  BOOL isAutoid;
@property BOOL issendaudio;
+(CacheManager*)getInstance;
-(void)resetInstance;
@end
