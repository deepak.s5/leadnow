//
//  IncidenceTableViewCell.h
//  CitizenCOPCorp
//
//  Created by Rakesh Jain on 01/05/15.
//  Copyright (c) 2015 Rajpal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IncidenceTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *ServiceType;
@property (strong, nonatomic) IBOutlet UILabel *Description;
@property (strong, nonatomic) IBOutlet UILabel *LevelOfUrgency;

@end
