//
//  POPDSampleViewController.h
//  popdowntable
//
//  Created by Alex Di Mango on 15/09/2013.
//  Copyright (c) 2013 Alex Di Mango. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMLParser.h"
#import "AppDelegate.h"

@interface SettingView : UIViewController
{
    CacheManager *cMgr;
    NSManagedObjectContext *context;
    AppDelegate *appDelegate;
}
- (IBAction)RefreshAction:(id)sender;
- (IBAction)aboutAction:(id)sender;
-(IBAction)back:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hieght;



@end
