//
//  Imgpath.h
//  Lead Now
//
//  Created by Rakesh Jain on 10/06/15.
//  Copyright (c) 2015 Infocrats. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Imgpath : NSManagedObject

@property (nonatomic, retain) NSString * autoidimg;
@property (nonatomic, retain) NSString * iD;
@property (nonatomic, retain) NSString * imgNameimg;
@property (nonatomic, retain) NSString * imgNameimg1;
@property (nonatomic, retain) NSString * imgNameimg2;
@property (nonatomic, retain) NSString * imgNameimg3;
@property (nonatomic, retain) NSString * pathImg;
@property (nonatomic, retain) NSString * pathImg1;
@property (nonatomic, retain) NSString * pathImg2;
@property (nonatomic, retain) NSString * pathImg3;
@property (nonatomic, retain) NSString * statuso;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * userkey;

@end
