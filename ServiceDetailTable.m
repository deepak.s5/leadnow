//
//  ServiceDetailTable.m
//  Lead Now
//
//  Created by Rakesh Jain on 10/06/15.
//  Copyright (c) 2015 Infocrats. All rights reserved.
//

#import "ServiceDetailTable.h"


@implementation ServiceDetailTable

@dynamic accountNo;
@dynamic audioName;
@dynamic audioPath;
@dynamic autoId;
@dynamic cName;
@dynamic comment;
@dynamic dateTime;
@dynamic iD;
@dynamic imgName;
@dynamic savedImagePath;
@dynamic sEnddetail;
@dynamic sendForCustomer;
@dynamic serviceId;
@dynamic serviceName;
@dynamic status1;
@dynamic statuso;
@dynamic urgencyLevel;
@dynamic username;
@dynamic userkey;

@end
