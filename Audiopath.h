//
//  Audiopath.h
//  Lead Now
//
//  Created by Rakesh Jain on 10/06/15.
//  Copyright (c) 2015 Infocrats. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Audiopath : NSManagedObject

@property (nonatomic, retain) NSString * audionameDatabase;
@property (nonatomic, retain) NSString * audiopathDatabase;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * userkey;

@end
