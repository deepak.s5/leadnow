//
//  ServiceDetailTable.h
//  Lead Now
//
//  Created by Rakesh Jain on 10/06/15.
//  Copyright (c) 2015 Infocrats. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ServiceDetailTable : NSManagedObject

@property (nonatomic, retain) NSString * accountNo;
@property (nonatomic, retain) NSString * audioName;
@property (nonatomic, retain) NSString * audioPath;
@property (nonatomic, retain) NSString * autoId;
@property (nonatomic, retain) NSString * cName;
@property (nonatomic, retain) NSString * comment;
@property (nonatomic, retain) NSString * dateTime;
@property (nonatomic, retain) NSString * iD;
@property (nonatomic, retain) NSString * imgName;
@property (nonatomic, retain) NSString * savedImagePath;
@property (nonatomic, retain) NSString * sEnddetail;
@property (nonatomic, retain) NSString * sendForCustomer;
@property (nonatomic, retain) NSString * serviceId;
@property (nonatomic, retain) NSString * serviceName;
@property (nonatomic, retain) NSString * status1;
@property (nonatomic, retain) NSString * statuso;
@property (nonatomic, retain) NSString * urgencyLevel;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * userkey;

@end
